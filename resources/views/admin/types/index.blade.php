@extends('admin.layout.admin')

@push('styles')

<link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">

@endpush

@section('title','types')


@section('content')



<div class="kt-portlet kt-portlet--mobile js-wrapper">

	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
				types table
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">

					&nbsp;
					@can('type_create')
					<a href="#" id="addNewBtn"  class="btn btn-brand btn-elevate btn-icon-sm">
						<i class="la la-plus"></i>
						New Type
					</a>
					@endcan
				</div>
			</div>
		</div>
	</div>

	<div class="kt-portlet__body">
		<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer js-region-table">
			<div class="row">
				<div class="col-sm-12">
					<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="types_table" role="grid" aria-describedby="kt_table_1_info" style="width: 1221px;">
						<thead>
							<tr role="row">
								<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">id</th>
								<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">name</th>
							
								<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">Description</th>
								
								<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">categories</th>
								
								<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 97.5px;" aria-label="Actions">Actions</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade show" id="type_modal"  role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; display: none;" aria-modal="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Type</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<form enctype="multipart/form-data" class="kt-form kt-form--label-right js-new-type-form" data-url="{{route('types.store')}}"  novalidate="novalidate">
						<div class="kt-portlet__body">
							<div class="form-group row validate {{$errors->has('name') ? 'is-invalid' : ''}}">
								<label class="col-form-label col-lg-3 col-sm-12">Name *</label>
								<div class="col-lg-9 col-md-9 col-sm-12">
									<input type="text"  class="form-control" name="name" placeholder="Entername">
								</div>
							</div>							

							<div class="form-group row validate">
								<label class="col-form-label col-lg-3 col-sm-12">description</label>
								<div class="col-lg-9 col-md-9 col-sm-12">
									<textarea class="form-control" name="description"></textarea>
								</div>
							</div>							
							<div class="form-group row validate">
								<label class="col-form-label col-lg-3 col-sm-12">category</label>
								<div class="col-lg-9 col-md-9 col-sm-12">
									<select name="category_id[]" multiple class="form-control select2_category" style="width: 100%">
									</select>
								</div>
							</div>
						</div>
						<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<div class="row">
									<div class="col-lg-9 ml-lg-auto">
										<button type="submit" id="saveType" class="btn btn-brand">save</button>
										<button type="reset" class="btn btn-secondary">reset</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>	
</div>



@endsection
@push('scripts')


<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
<script src="{{asset('js/dashboard/types.js')}}"></script>
<script>
	
	var can = {
		edit: false,
		delete: false
	}

	@can('type_edit')
		can.edit = true
	@endcan
	
	@can('type_delete')
		can.delete = true
	@endcan
	
</script>
<script>



    $(document).ready(function(){

            var $wrapper = $('.js-wrapper');
            var repLogApp = new types($wrapper);
    });
</script>
@endpush
