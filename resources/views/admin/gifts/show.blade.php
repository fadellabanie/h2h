@extends('admin.layout.admin')

@push('styles')

<link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">

@endpush

@section('title','gift')


@section('content')

<br>

@include('admin.gifts.main')



<div class="row js-wrapper">
	<div class="col-xl-12">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">Gift Information
						<small>update Gift informaiton</small>
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<form action="{{route('gifts.update',$gift->id)}}" method="post">
					{{csrf_field()}}
					{{ method_field('PATCH') }}

					<div class="kt-form kt-form--label-right">
						<div class="kt-form__body">
							<div class="kt-section kt-section--first">
								<div class="kt-section__body">
									<div class="form-group row validate {{$errors->has('name') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">*Name</label>
										<div class="col-lg-9 col-xl-6">
											  <input name="name" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" type="text" value="{{$gift->name}}">
											@error('name')
												<div id="name-error" class="error invalid-feedback">{{$message}}</div>
											@enderror
										</div>
									</div>

									<div class="form-group row validate {{$errors->has('description') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">Description</label>
										<div class="col-lg-9 col-xl-6">
											<textarea name="description" class="form-control {{$errors->has('description') ? 'is-invalid' : ''}}">{{$gift->description}}</textarea>
											@error('description')
												<div id="description-error" class="error invalid-feedback">{{$message}}</div>
											@enderror
										</div>
									</div>



                                    <div class="form-group row code validate {{$errors->has('code') ? 'is-invalid' : ''}}">
                                        <label class="col-xl-3 col-lg-3 col-form-label">*Code</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input value="{{$gift->code}}" type="text" name="code" class="form-control {{$errors->has('code') ? 'is-invalid' : ''}}">
                                            @error('code')
                                            <div id="code-error" class="error invalid-feedback">{{$message}}</div>
                                            @enderror
                                        </div>
                                    </div>

									<div class="form-group row validate {{$errors->has('from') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">*From</label>
										<div class="col-lg-9 col-xl-6">
											  <input name="from" class="form-control datepicker {{$errors->has('from') ? 'is-invalid' : ''}}" type="text" value="{{$gift->from}}">
											  @error('from')
													<div id="from-error" class="error invalid-feedback">{{$message}}</div>
											  @enderror
										</div>
									</div>

									<div class="form-group row validate {{$errors->has('to') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">*To</label>
										<div class="col-lg-9 col-xl-6">
											  <input name="to" class="form-control to {{$errors->has('to') ? 'is-invalid' : ''}}" type="text" value="{{$gift->to}}">

											  @error('to')
													<div id="to-error" class="error invalid-feedback">{{$message}}</div>
											  @enderror
										</div>
									</div>

									<div class="form-group row validate {{$errors->has('points') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">*points</label>
										<div class="col-lg-9 col-xl-6">
											  <input name="points" class="form-control {{$errors->has('points') ? 'is-invalid' : ''}}" type="text" value="{{$gift->points}}">
											@error('points')
												<div id="points-error" class="error invalid-feedback">{{$message}}</div>
											@enderror
										</div>
									</div>
									@can('gift_edit')
										<button class="btn btn-primary" type="submit">save</button>
									@endcan
								</div>
							</div>
						</div>
					</div>

				</form>

			</div>
		</div>
	</div>
</div>
@endsection
@push('scripts')


<script>

    $(document).ready(function(){

        $('#type').on('change',function (e) {

            id = $(e.currentTarget).val();
            if (id == 2) {
                $('.code').show();
            }else{
                $('.code').hide();
            }
        });
		var today = new Date();

    	$('.datepicker').datepicker({
			format: 'yyyy-mm-dd'
		});

    	$('.to').datepicker({
			format: 'yyyy-mm-dd',
			startDate: today
		});

        $('.select2').select2({
            placeholder: 'Choose Type'
        });

    });

</script>
@endpush
