@extends('admin.layout.admin')

@push('styles')

@endpush

@section('title','account')

@section('content')
<br>
<div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app js-account">
	<button class="kt-app__aside-close" id="kt_user_profile_aside_close">
		<i class="la la-close"></i>
	</button>
	@include('admin.drivers.profile')
	<div class="kt-grid__item kt-grid__item--fluid kt-app__content">
		<div class="row">
			<div class="col-xl-12">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">Driver Information 
								<small>update driver informaiton</small>
							</h3>
						</div>
					</div>
					<form class="kt-form kt-form--label-right js-update-driver-form" method="post" action="{{route('drivers.update',$driver->id)}}" enctype="multipart/form-data">
						{{csrf_field()}}
						<div class="kt-portlet__body">
							<div class="kt-section kt-section--first">
								<div class="kt-section__body">
									<div class="row">
										<label class="col-xl-3"></label>
										<div class="col-lg-9 col-xl-6">
											<h3 class="kt-section__title kt-section__title-sm">Driver Info:</h3>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
										<div class="col-lg-9 col-xl-6">
											<div class="kt-avatar kt-avatar--outline" id="kt_user_avatar">
												<div class="kt-avatar__holder" 
												style="background-image: url({{$driver->image_url ? asset($driver->image_url) : 'https://via.placeholder.com/300.png/09f/fffC/'}})">
												</div>
												<label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
													<i class="fa fa-pen"></i>
													<input type="file" name="driver_image" accept=".png, .jpg, .jpeg">
												</label>
												<span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
													<i class="fa fa-times"></i>
												</span>
											</div>
										</div>
									</div>														
									<div class="form-group row validate {{$errors->has('date_of_birth') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">date of birth</label>
										<div class="col-lg-9 col-xl-6">
											<input class="form-control {{$errors->has('date_of_birth') ? 'is-invalid' : ''}}" type="text" id="date_select" name="date_of_birth" value="{{$driver->date_of_birth}}"   placeholder="select date">
											@error('date_of_birth')
											    <div id="date_of_birth-error" class="error invalid-feedback">{{$message}}</div>
											@enderror
										</div>
									</div>								
									<div class="form-group row validate {{$errors->has('license') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">license</label>
										<div class="col-lg-9 col-xl-6">
											<input class="form-control {{$errors->has('license') ? 'is-invalid' : ''}}" type="text" name="license" value="{{$driver->license}}">
											@error('license')
											    <div id="license-error" class="error invalid-feedback">{{$message}}</div>
											@enderror
										</div>
									</div>
								

									<div class="form-group row validate">
										<label class="col-xl-3 col-lg-3 col-form-label">*City</label>
										<div class="col-lg-9 col-xl-6">
											<select data-city-id="{{$driver->regions[0]->city_id}}" class="form-control kt-select2 select2-hidden-accessible select_city" id="kt_select2_5" name="city_id" style="width: 100%">
												{{-- <option selected value="{{$driver->region[0]->city_id}}">{{$driver->region[0]->city->name}}</option> --}}
											</select>
										</div>
									</div>									
									<div class="form-group row validate {{$errors->has('region_id') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">*Region</label>
										<div class="col-lg-9 col-xl-6">
											<select class="form-control {{$errors->has('region_id') ? 'is-invalid' : ''}} select_region" id="" name="region_id[]" multiple style="width: 100%">
												@foreach($driver->regions as $region)
													<option selected value="{{$region->id}}">{{$region->name}}</option>
												@endforeach
												{{-- <option selected value="{{$driver->region->id}}">{{$driver->region->name}}</option> --}}
											</select>
											@error('region_id')
											    <div id="region_id-error" class="error invalid-feedback">{{$message}}</div>
											@enderror
										</div>
									</div>

									<div class="form-group row validate {{$errors->has('file') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">File</label>
										<div class="col-lg-9 col-xl-6">
											<input type="file" name="file" class="form-control">
											@error('file')
											    <div id="file-error" class="error invalid-feedback">{{$message}}</div>
											@enderror
										</div>
									</div>

									@if($driver->file_url != '')
										<div class="form-group row validate {{$errors->has('file') ? 'is-invalid' : ''}}">
											<label class="col-xl-3 col-lg-3 col-form-label">File</label>
											<div class="col-lg-9 col-xl-6">
												<a href="{{route('drivers.download',$driver->id)}}" class="btn btn-primary">Download File</a>
											</div>
										</div>
									@endif

								</div>
							</div>
						</div>
						<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<div class="row">
									<div class="col-lg-3 col-xl-3">
									</div>
									<div class="col-lg-9 col-xl-9">
										<button id="updateDriver" type="submit" class="btn btn-primary">Submit</button>&nbsp;
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@push('scripts')
	<script src="{{asset('assets/js/pages/custom/user/profile.js')}}"></script>
	<script src="{{asset('js/dashboard/account.js')}}"></script>
	<script src="{{asset('js/dashboard/bootstrap-timepicker.js')}}" type="text/javascript"></script>

	<script>
		$(document).ready(function(){
			var $wrapper = $('.js-account');
            var repLogApp = new account($wrapper);
		});
	</script>
@endpush
