<div class="">
	<!--Begin:: App Aside-->
	<div class="kt-grid__item kt-app__toggle kt-app__aside" id="kt_user_profile_aside">
		<div class="kt-portlet kt-portlet--height-fluid-">
			<div class="kt-portlet__head  kt-portlet__head--noborder">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title"></h3>
				</div>
				<!-- small nav -->
			<!-- 	<div class="kt-portlet__head-toolbar">
				<a href="#" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="false">
					<i class="flaticon-more-1"></i>
				</a>
				<div class="dropdown-menu dropdown-menu-right dropdown-menu-fit dropdown-menu-md" style="">
					<ul class="kt-nav">
						<li class="kt-nav__head">
							Export Options
							<span data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Click to learn more...">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand kt-svg-icon--md1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<rect x="0" y="0" width="24" height="24"></rect>
										<circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"></circle>
										<rect fill="#000000" x="11" y="10" width="2" height="7" rx="1"></rect>
										<rect fill="#000000" x="11" y="7" width="2" height="2" rx="1"></rect>
									</g>
								</svg> 
							</span>
						</li>
						<li class="kt-nav__separator"></li>
						<li class="kt-nav__item">
							<a href="#" class="kt-nav__link">
								<i class="kt-nav__link-icon flaticon2-drop"></i>
								<span class="kt-nav__link-text">Activity</span>
							</a>
						</li>
						<li class="kt-nav__item">
							<a href="#" class="kt-nav__link">
								<i class="kt-nav__link-icon flaticon2-calendar-8"></i>
								<span class="kt-nav__link-text">FAQ</span>
							</a>
						</li>
						<li class="kt-nav__item">
							<a href="#" class="kt-nav__link">
								<i class="kt-nav__link-icon flaticon2-telegram-logo"></i>
								<span class="kt-nav__link-text">Settings</span>
							</a>
						</li>
					</ul>
				</div>
			</div> -->
			</div>
			<div class="kt-portlet__body kt-portlet__body--fit-y">
				<div class="kt-widget kt-widget--user-profile-1">
					<div class="kt-widget__head">
						<div class="kt-widget__media">
							<img src="{{$driver->image_url == '' ? 'https://via.placeholder.com/300.png/09f/fffC/O' : asset($driver->image_url)}}" alt="image">
						</div>
						<div class="kt-widget__content">
							<div class="kt-widget__section">
								<a href="#" class="kt-widget__username">
									{{$driver->user->name}}
									@if($driver->user->email_verified_at != null)
										<i class="flaticon2-correct kt-font-success"></i>
									@endif
								</a>
								<span class="kt-widget__subtitle">
									{{$driver->user->group->name}}
								</span>
							</div>
							<div class="kt-widget__action">
								@if($driver->driver_status_id == 1)
									<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">Busy</span>
								@else
									<span class="kt-badge kt-badge--info kt-badge--inline kt-badge--pill">Available</span>
								@endif
							</div>
						</div>
					</div>
					<div class="kt-widget__body">
						<div class="kt-widget__content">
							<div class="kt-widget__info">
								<span class="kt-widget__label">Email:</span>
								<a href="#" class="kt-widget__data">{{$driver->user->email}}</a>
							</div>
							<div class="kt-widget__info">
								<span class="kt-widget__label">Phone:</span>
								<p class="kt-widget__data">{{$driver->user->phone}}</p>
							</div>
							<div class="kt-widget__info">
								<span class="kt-widget__label">Date of birth:</span>
								<span class="kt-widget__data">{{$driver->date_of_birth}}</span>
							</div>
						</div>
						<div class="kt-widget__items">
							<a href="{{route('drivers.overview',request('id'))}}" class="kt-widget__item kt-widget__item--{{Route::is('drivers.overview') ? 'active': ''}}">
								<span class="kt-widget__section">
									<span class="kt-widget__icon">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24"></polygon>
												<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
												<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3">
												</path>
											</g>
										</svg> 
									</span>
									<span class="kt-widget__desc">
										Profile Overview
									</span>
								</span>
							</a>

							@can('driver_edit')
							<a href="{{route('drivers.personal',request('id'))}}" class="kt-widget__item kt-widget__item--{{Route::is('drivers.personal') ? 'active': ''}}">
								<span class="kt-widget__section">
									<span class="kt-widget__icon">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24"></polygon>
												<path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
												<path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero"></path>
											</g>
										</svg> 
									</span>
									<span class="kt-widget__desc">
										Personal Information
									</span>
								</span>
							</a>
							@endcan
							@can('driver_edit')
							<a href="{{route('drivers.account',request('id'))}}" class="kt-widget__item ">
								<span class="kt-widget__section">
									<span class="kt-widget__icon">
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<rect x="0" y="0" width="24" height="24"></rect>
												<path d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z" fill="#000000" opacity="0.3"></path>
												<path d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z" fill="#000000"></path>
											</g>
										</svg> 
									</span>
									<span class="kt-widget__desc">
										Account Information
									</span>

								</span>
							</a>
							@endcan

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>