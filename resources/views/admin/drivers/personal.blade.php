@extends('admin.layout.admin')

@push('styles')

@endpush

@section('title','personal')

@section('content')
<br>
<div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app js-personal">
	<button class="kt-app__aside-close" id="kt_user_profile_aside_close">
		<i class="la la-close"></i>
	</button>
	@include('admin.drivers.profile')
	<div class="kt-grid__item kt-grid__item--fluid kt-app__content">
		<div class="row">
			<div class="col-xl-12">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">Personal Information 
								<small>update driver personal informaiton</small>
							</h3>
						</div>
					</div>
					<form class="kt-form kt-form--label-right js-update-driver-form" method="post" action="{{route('managers.update',$driver->user->id)}}">
						{{csrf_field()}}
						{{ method_field('PATCH') }}
						<div class="kt-portlet__body">
							<div class="kt-section kt-section--first">
								<div class="kt-section__body">
									<div class="row">
										<label class="col-xl-3"></label>
										<div class="col-lg-9 col-xl-6">
											<h3 class="kt-section__title kt-section__title-sm">Baisc Info:</h3>
										</div>
									</div>
									<div class="form-group row validate {{$errors->has('name') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">*Name</label>
										<div class="col-lg-9 col-xl-6">
											<input type="text" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" name="name" value="{{$driver->user->name}}">
											@error('name')
											    <div id="name-error" class="error invalid-feedback">{{$message}}</div>
											@enderror
										</div>
									</div>					
									<div class="form-group row validate {{$errors->has('email') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">*Email</label>
										<div class="col-lg-9 col-xl-6">
											<input class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" type="text" name="email" value="{{$driver->user->email}}">

											@error('email')
											    <div id="name-error" class="error invalid-feedback">{{$message}}</div>
											@enderror
										</div>
									</div>								
									<div class="form-group row validate {{$errors->has('address') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">Address</label>
										<div class="col-lg-9 col-xl-6">
											<input class="form-control" type="text" name="address" value="{{$driver->user->address}}">
											@error('address')
											    <div id="address-error" class="error invalid-feedback">{{$message}}</div>
											@enderror

										</div>
									</div>
									<div class="form-group row validate {{$errors->has('phone') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">Phone</label>
										<div class="col-lg-9 col-xl-6">
											<input class="form-control" type="text" name="phone" value="{{$driver->user->phone}}">

											@error('phone')
											    <div id="phone-error" class="error invalid-feedback">{{$message}}</div>
											@enderror
										</div>
									</div>	
									<div class="kt-separator kt-separator--border-dashed kt-separator--portlet-fit kt-separator--space-lg"></div>
									<div class="row">
										<label class="col-xl-3"></label>
											<div class="col-lg-9 col-xl-6">
												<h3 class="kt-section__title kt-section__title-sm">Security:</h3>
											</div>
									</div>
									<div class="form-group row validate {{$errors->has('password') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">password</label>
										<div class="col-lg-9 col-xl-6">
											<input class="form-control" type="password" name="password">
											@error('password')
											    <div id="password-error" class="error invalid-feedback">{{$message}}</div>
											@enderror
										</div>
									</div>								
									<div class="form-group row validate {{$errors->has('password_confirmation') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">password confirmation</label>
										<div class="col-lg-9 col-xl-6">
											<input class="form-control" type="password" name="password_confirmation">
											@error('password_confirmation')
											    <div id="password_confirmation-error" class="error invalid-feedback">{{$message}}</div>
											@enderror
										</div>
									</div>		
								</div>
							</div>
						</div>
						<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<div class="row">
									<div class="col-lg-3 col-xl-3">
									</div>
									<div class="col-lg-9 col-xl-9">
										<button id="updateDriver" type="submit" class="btn btn-primary">Submit</button>&nbsp;
										<button id="toggleActivation" data-id="{{$driver->user->id}}" type="button" class="btn {{$driver->user->isActive  ==  1 ? 'btn-danger' : 'btn-success'}}">
											
											{{$driver->user->isActive  ==  1 ? 'deactivate' : 'activate'}}
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('scripts')
	<script>var success = {!! json_encode(Session::get('success'), JSON_HEX_TAG) !!};</script>
	<script src="{{asset('assets/js/pages/custom/user/profile.js')}}"></script>
	<script src="{{asset('js/dashboard/personal.js')}}"></script>
	<script>
		$(document).ready(function(){
			var $wrapper = $('.js-personal');
            var repLogApp = new personal($wrapper);
		});
	</script>
@endpush
