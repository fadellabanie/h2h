@extends('admin.layout.admin')

@push('styles')



@endpush

@section('title','overview')


@section('content')
<br>
<div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">
	<button class="kt-app__aside-close" id="kt_user_profile_aside_close">
		<i class="la la-close"></i>
	</button>
	@include('admin.drivers.profile')
	
	<div class="kt-grid__item kt-grid__item--fluid kt-app__content">
<!-- 		<div class="row">
			<div class="col-xl-6">
				<div class="kt-portlet kt-portlet--height-fluid">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								Requests Statistics
							</h3>
						</div>
					</div>
					<div class="kt-portlet__body kt-portlet__body--fluid">
						<div class="kt-widget12">
							<div class="kt-widget12__content">
								<div class="kt-widget12__item">
									<div class="kt-widget12__info">
										<span class="kt-widget12__desc">Annual Taxes EMS</span>
											<span class="kt-widget12__value">$400,000</span>
									</div>
									<div class="kt-widget12__info">
										<span class="kt-widget12__desc">Finance Review Date</span>
										<span class="kt-widget12__value">July 24,2019</span>
									</div>
								</div>
								<div class="kt-widget12__item">
									<div class="kt-widget12__info">
										<span class="kt-widget12__desc">Avarage Revenue</span>
										<span class="kt-widget12__value">$60M</span>
									</div>
									<div class="kt-widget12__info">
										<span class="kt-widget12__desc">Revenue Margin</span>
										<div class="kt-widget12__progress">
											<div class="progress kt-progress--sm">
												<div class="progress-bar kt-bg-brand" role="progressbar" style="width: 40%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<span class="kt-widget12__stat">
													40%
												</span>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-widget12__chart" style="height:250px;">
									<div class="chartjs-size-monitor">
										<div class="chartjs-size-monitor-expand">
											<div class="">
			
											</div>
										</div>
										<div class="chartjs-size-monitor-shrink">
											<div class="">
												
											</div>
										</div>
									</div>
									<canvas id="kt_chart_order_statistics" style="display: block; width: 392px; height: 250px;" width="392" height="250" class="chartjs-render-monitor">
										
									</canvas>
								</div>
							</div>
						</div>
				</div>
			</div>
			<div class="col-xl-6">
				<div class="kt-portlet kt-portlet--height-fluid">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								Notifications
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<ul class="nav nav-pills nav-pills-sm nav-pills-label nav-pills-bold" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" data-toggle="tab" href="#kt_widget6_tab1_content" role="tab">
										Latest
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#kt_widget6_tab2_content" role="tab">
										Week
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#kt_widget6_tab3_content" role="tab">
										Month
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="kt-portlet__body">
						<div class="tab-content">
							<div class="tab-pane active" id="kt_widget6_tab1_content"aria-expanded="true">
								<div class="kt-notification">
									<a href="#" class="kt-notification__item">
										<div class="kt-notification__item-icon">
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
											    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											        <path d="M17,12 L18.5,12 C19.3284271,12 20,12.6715729 20,13.5 C20,14.3284271 19.3284271,15 18.5,15 L5.5,15 C4.67157288,15 4,14.3284271 4,13.5 C4,12.6715729 4.67157288,12 5.5,12 L7,12 L7.5582739,6.97553494 C7.80974924,4.71225688 9.72279394,3 12,3 C14.2772061,3 16.1902508,4.71225688 16.4417261,6.97553494 L17,12 Z" fill="#000000"/>
											        <rect fill="#000000" opacity="0.3" x="10" y="16" width="4" height="4" rx="2"/>
											    </g>
											</svg>
										</div>
										<div class="kt-notification__item-details">
											<div class="kt-notification__item-title">
												New order has been received.
											</div>
											<div class="kt-notification__item-time">
												2 hrs ago
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> -->
		<div class="row">
			<div class="kt-portlet kt-portlet--mobile js-wrapper">
				<div class="kt-portlet__head kt-portlet__head--lg">
					<div class="kt-portlet__head-label">
						<span class="kt-portlet__head-icon">
							<i class="kt-font-brand flaticon2-line-chart"></i>
						</span>
						<h3 class="kt-portlet__head-title">
							Orders
						</h3>
					</div>
				</div>

				<div class="kt-portlet__body">
					<form action="" class="kt-form kt-form--fit kt-margin-b-20">
							<div class="row kt-margin-b-20">
								<div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
									<label>OrderNo:</label>
									<input type="text" name="orderNo" class="form-control kt-input" data-col-index="0">
								</div>
								<div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
									<label>Customer:</label>
									<select name="customer_id"  class="form-control kt-input customerSelect"></select>
								</div>
								<div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
									<label>Status:</label>
									<select name="status_id"  class="form-control kt-input status_select">
										<option selected></option>
										<option value="1">Accepted</option>
										<option value="2">PickedUp</option>
										<option value="3">Dropped</option>
										<option value="4">Out Of Delivery</option>
										<option value="5">Delivered</option>
									</select>
								</div>
							</div>
							<div class="kt-separator kt-separator--md kt-separator--dashed"></div>
							<div class="row">
								<div class="col-lg-12">
									<button 
								class="btn btn-primary btn-brand--icon" id="kt_search">
										<span>
											<i class="la la-search"></i>
											<span>Search</span>
										</span>
									</button>
									&nbsp;&nbsp;
									<button class="btn btn-secondary btn-secondary--icon" id="kt_reset">
										<span>
											<i class="la la-close"></i>
											<span>Reset</span>
										</span>
									</button>
								</div>
							</div>
						</form>
					<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer js-region-table">
						<div class="row">
							<div class="col-sm-12">
								<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="delivery_table" role="grid" aria-describedby="kt_table_1_info" style="width: 1221px;">
									<thead>
										<tr role="row">
											<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">orderNo</th>
											<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">Customer</th>

											<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">Status</th>

											<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">Date</th>
											
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@push('scripts')
	
	
	<script src="{{asset('assets/js/pages/dashboard.js')}}"></script>
	<script src="{{asset('assets/js/pages/custom/user/profile.js')}}"></script>
	<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

	<script>

		var driver_id = {!! json_encode($id, JSON_HEX_TAG) !!};

		$('#delivery_table').DataTable({
			responsive: true,
			processing: true,
			serverSide: true,
			ajax: {
				url:'/dashboard/orders/delivery/'+driver_id,
				data: function (d) {
                	d.orderNo = $('input[name=orderNo]').val();
                	d.customer_id =  $(".customerSelect option:selected").val();
                	d.status_id = $(".status_select option:selected").val();
                	
            	}
			},
			columns: [
				{data: 'order.uuId', name: 'order.uuId'},
				{data: 'order.customer.user.name', name: 'order.customer.user.name'},
				{data: 'status_id',render:function(data, type, full, meta){
					if (full.accepted_at != "") {
						return `<span class="kt-badge  kt-badge--info kt-badge--inline kt-badge--pill">Accepted</span>`;
					}
					if (full.picked_at != "") {
						return `<span class="kt-badge  kt-badge--primary kt-badge--inline kt-badge--pill">PickedUp</span>`;
					}
					if (full.dropped_at != "") {
						return `<span class="kt-badge  kt-badge--light kt-badge--inline kt-badge--pill">DroppedOff</span>`;
					}
					if (full.outDelivery_at != "") {
						return `<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">Out of Delivery</span>`;
					}						
					if (full.delivered_at != "") {
						return `<span class="kt-badge  kt-badge--dark kt-badge--inline kt-badge--pill">Delivered</span>`;
					}
					
					return 're';
				}},
				{data: 'created_at', name: 'orders.created_at'},
				],
				columnDefs: [
					{
						targets: -1,
						title: 'Date',
						orderable: false,
						render: function(data, type, full, meta) {
							
							if (full.accepted_at != "") {
								return full.accepted_at;
							}							

							if (full.dropped_at != "") {
								return full.dropped_at;
							}							

							if (full.outDelivery_at != "") {
								return full.outDelivery_at;
							}							

							if (full.delivered_at != "") {
								return full.delivered_at;
							}


						}
					}				
				],
		});


		$('#kt_search').on('click',function(e){
			e.preventDefault();
			$('#delivery_table').DataTable().ajax.reload();
		});
		$('.status_select').select2({
			placeholder: 'Select Status',
			allowClear: true
		});

		$('.customerSelect').select2({
	        placeholder: "Choose Customer...",
	        minimumInputLength: 2,
	        ajax: {
	            url: '/dashboard/customers/find',
	            dataType: 'json',
	            data: function (params) {
	                return {
	                    q: $.trim(params.term)
	                };
	            },
	            processResults: function (data) {
	                return {
	                    results: data
	                };
	            },
	            cache: true
	        }
	    }).empty();
	</script>
@endpush
