@extends('admin.layout.admin')

@push('styles')

<link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">

@endpush

@section('title','drivers')


@section('content')



<div class="kt-portlet kt-portlet--mobile js-wrapper">

	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
				drivers table
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">

					&nbsp;
					@can('driver_create')
					<a href="#" id="addNewDriver"  class="btn btn-brand btn-elevate btn-icon-sm">
						<i class="la la-plus"></i>
						Add New Driver
					</a>
					@endcan

				</div>
			</div>
		</div>
	</div>

	<div class="kt-portlet__body">
		<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer js-region-table">
			<div class="row">
				<div class="col-sm-12">
					<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="drivers_table" role="grid" aria-describedby="kt_table_1_info" style="width: 1221px;">
						<thead>
							<tr role="row">
								<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">id</th>
								<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">name</th>

								<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">email</th>

								<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">status</th>
								
								<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 97.5px;" aria-label="Actions">Actions</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>


	<!-- begain modal -->
	<div class="modal fade show" id="newDriverModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" style="display: hidden; padding-right: 17px;">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">New Driver</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<form class="kt-form kt-form js-new-driver-form" enctype="multipart/form-data" data-url="{{route('drivers.store')}}" autocomplete="off">
						<div class="kt-portlet__body">
							<div class="kt-section">
								<div class="kt-section__title">
									Baisc Info
								</div>
								<div class="kt-section__content">
									<div class="row">
										<div class="col-sm-3">
											<div class="form-group validate {{$errors->has('name') ? 'is-invalid' : ''}}">
												<label class="form-control-label">*Name:</label>
												<input type="text" name="name" class="form-control">
												@error('name')
											    	<div id="name-error" class="error invalid-feedback">{{$message}}</div>
												@enderror
											</div>
										</div>										
										<div class="col-sm-3">
											<div class="form-group validate {{$errors->has('email') ? 'is-invalid' : ''}}">
												<label class="form-control-label">*Email:</label>
												<input type="text" name="email" class="form-control">
												@error('name')
											    	<div id="email-error" class="error invalid-feedback">{{$message}}</div>
												@enderror
											</div>
										</div>										
										<div class="col-sm-3">
											<div class="form-group validate {{$errors->has('password') ? 'is-invalid' : ''}}">
												<label class="form-control-label">*password:</label>
												<input type="password" name="password" class="form-control">
												@error('password')
											    	<div id="password-error" class="error invalid-feedback">{{$message}}</div>
												@enderror
											</div>
										</div>										
										<div class="col-sm-3">
											<div class="form-group validate">
												<label class="form-control-label">address:</label>
												<input type="text" name="address" class="form-control">
											</div>
										</div>										
										<div class="col-sm-3">
											<div class="form-group validate  {{$errors->has('phone') ? 'is-invalid' : ''}}">
												<label class="form-control-label">phone:</label>
												<input type="text" name="phone" class="form-control">
												@error('phone')
											    	<div id="phone-error" class="error invalid-feedback">{{$message}}</div>
												@enderror
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="kt-separator kt-separator--border-dashed kt-separator--space-x"></div>
							<div class="kt-section">
								<div class="kt-section__title">
									Driver Info
								</div>
								<div class="kt-section__content">
									<div class="row">
										<div class="col-sm-3">
											<div class="form-group validate">
												<label class="form-control-label">*city:</label>
												<select name="city_id" class="form-control select_city">
												</select>
											</div>
										</div>										
										<div class="col-sm-3">
											<div class="form-group validate {{$errors->has('region_id') ? 'is-invalid' : ''}}">
												<label class="form-control-label">*region:</label>
												<select name="region_id[]" multiple="multiple" class="form-control kt-select2 select_region" disabled>
												</select>
											</div>
										</div>										
										<div class="col-sm-3">
											<div class="form-group validate">
												<label class="form-control-label">license:</label>
												<input type="text" name="license" class="form-control">
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group validate">
												<label class="form-control-label">date of birth:</label>
												<input type="date" name="date_of_birth" class="form-control">
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group validate">
												<label class="form-control-label">driver file</label>
												<input type="file" name="file" class="form-control">
											</div>
										</div>
									</div>
								</div>
							</div>

							<!-- Time Availability -->
<!-- 							<div class="kt-section">
								<div class="kt-section__title">
									Time Availability 
								</div>
								<div class="kt-section__content">
									<div class="row">
									<div class="col-sm-2">
											<div class="form-group validate">
													<label class="form-control-label">Time From</label>
													<input class="form-control" name="time_from" id="kt_timepicker_21" readonly placeholder="Select time" type="text" />
											</div>
										</div>	
										<div class="col-sm-2">
											<div class="form-group validate">
													<label class="form-control-label">Time to</label>
													<input class="form-control" name="time_to" id="kt_timepicker_2" readonly placeholder="Select time" type="text" />
											</div>
										</div>										
										<div class="form-group col-sm-8">
											<label>Work Days</label>
											<div class="kt-checkbox-inline">
												<label class="kt-checkbox">
													<input name="day[]" value="Saturday" type="checkbox"> Saturday
													<span></span>
												</label>
												<label class="kt-checkbox">
													<input name="day[]" value="Sunday" type="checkbox"> Sunday
													<span></span>
												</label>
												<label class="kt-checkbox">
													<input name="day[]" value="Monday" type="checkbox"> Monday
													<span></span>
												</label>
												<label class="kt-checkbox">
													<input name="day[]" value="Tuesday" type="checkbox"> Tuesday
													<span></span>
												</label>
												<label class="kt-checkbox">
													<input name="day[]" value="Wednesday" type="checkbox"> Wednesday
													<span></span>
												</label>
												<label class="kt-checkbox">
													<input name="day[]" value="Thursday" type="checkbox"> Thursday
													<span></span>
												</label>
												<label class="kt-checkbox">
													<input name="day[]" value="Friday" type="checkbox"> Friday
													<span></span>
												</label>
											</div>
											<span class="form-text text-muted">Select the available day</span>
										</div>
									</div>
								</div>
							</div>
 -->
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							<button type="reset" class="btn btn-secondary">reset</button>
							<button type="submit" id="new" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- end modal -->
</div>



@endsection
@push('scripts')


<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
<script src="{{asset('js/dashboard/drivers.js')}}"></script>
<script src="{{asset('js/dashboard/bootstrap-timepicker.js')}}" type="text/javascript"></script>
<script>

	var can = {
		view: false,
		delete: false
	};

	@can('driver_view')
		can.view = true
	@endcan	

	@can('drive_delete')
		can.delete = true
	@endcan

</script>
<script>
	$(document).ready(function(){

		var $wrapper = $('.js-wrapper');


		var repLogApp = new drivers($wrapper);
	});
</script>
@endpush
