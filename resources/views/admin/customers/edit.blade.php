@extends('admin.layout.admin')

@push('styles')

<link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">

@endpush

@section('title','Customers')


@section('content')

<br>



<div class="row js-wrapper">
	<div class="col-xl-12">
		<div class="kt-portlet">

			<div class="kt-portlet__body">

				<form class="kt-form kt-form js-new-driver-form" action="{{route('customers.update', $customer->id)}}" method="POST">

					@csrf

					<div class="kt-portlet__body">
						<div class="kt-section">
							<div class="kt-section__title">
								Baisc Info
							</div>
							<div class="kt-section__content">
								<div class="row">
									<div class="col-sm-3">
										<div class="form-group validate {{$errors->has('name') ? 'is-invalid' : ''}}">
											<label class="form-control-label">*Name:</label>
											<input type="text" name="name" class="form-control" value="{{$customer->user->name}}">
											@error('name')
												<div id="name-error" class="error invalid-feedback">{{$message}}</div>
											@enderror
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group validate {{$errors->has('email') ? 'is-invalid' : ''}}">
											<label class="form-control-label">*Email:</label>
											<input type="text" name="email" class="form-control" value="{{$customer->user->email}}">
											@error('name')
												<div id="email-error" class="error invalid-feedback">{{$message}}</div>
											@enderror
										</div>
									</div>
									
									<div class="col-sm-3">
										<div class="form-group validate">
											<label class="form-control-label">address:</label>
											<input type="text" name="address" class="form-control" value="{{$customer->user->address}}">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group">
											<label class="form-control-label">phone:</label>
											<input type="text" name="phone" class="form-control" value="{{$customer->user->phone}}">
											
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
					<div class="modal-footer">
						<a href="{{route('customers.index')}}" class="btn btn-danger">back</a>
						<button type="submit" id="new" class="btn btn-primary">update</button>
					</div>
				</form>


			</div>
		</div>
	</div>
</div>


@endsection
@push('scripts')


<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
{{-- <script src="{{asset('js/dashboard/customer.js')}}"></script> --}}
<script src="{{asset('js/dashboard/bootstrap-timepicker.js')}}" type="text/javascript"></script>
<script>
	var can = {
		view: false
	}

	@can('customer_view')
		can.view = true
	@endcan
</script>
<script>



	$(document).ready(function(){

		var $wrapper = $('.js-wrapper');
		var repLogApp = new drivers($wrapper);
	});
</script>
@endpush
