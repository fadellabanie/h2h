@extends('admin.layout.admin')

@push('styles')

<link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">

@endpush

@section('title','Customers')
@section('content')

<br>

<div class="js-wrapper">

<div class="row">
	<div class="kt-portlet">
		<div class="kt-portlet__body  kt-portlet__body--fit">
			 <div class="row row-no-padding row-col-separator-lg">
			 	<div class="col-md-12 col-lg-6 col-xl-3">
			 		<div class="kt-widget24">
			 			<div class="kt-widget24__details">
							<div class="kt-widget24__info">
								<h4 class="kt-widget24__title">
									Total Orders
								</h4>
								<span class="kt-widget24__desc">
									All Customer Orders
								</span>
							</div>
							<span class="kt-widget24__stats kt-font-brand">
								{{$ordersCount}}
							</span>
						</div>
			 		</div>
			 	</div>

			 	<div class="col-md-12 col-lg-6 col-xl-3">
			 		<div class="kt-widget24">
			 			<div class="kt-widget24__details">
							<div class="kt-widget24__info">
								<h4 class="kt-widget24__title">
									Total Profit
								</h4>
								<span class="kt-widget24__desc">
									All Customer Profit
								</span>
							</div>
							<span class="kt-widget24__stats kt-font-brand">
								{{$total_profit}}
							</span>
						</div>
			 		</div>
			 	</div>

                 <div class="col-md-12 col-lg-6 col-xl-3">
			 		<div class="kt-widget24">
			 			<div class="kt-widget24__details">
							<div class="kt-widget24__info">
								<h4 class="kt-widget24__title">
									Total Points
								</h4>
								<span class="kt-widget24__desc">
                                    Customer Points
								</span>
							</div>
							<span class="kt-widget24__stats kt-font-brand">
								{{floatval($customer->points)}}
							</span>
						</div>
			 		</div>
			 	</div>

			 	<div class="col-md-12 col-lg-6 col-xl-3">
			 		<div class="kt-widget24">
			 			<div class="kt-widget24__details">
							<div class="kt-widget24__info">
								<h4 class="kt-widget24__title">
									Wallet
								</h4>
								<span class="kt-widget24__desc">
									All Customer Wallet
								</span>
							</div>
							<span class="kt-widget24__stats kt-font-brand">
								{{$customer->wallet}}
							</span>
						</div>
			 		</div>
			 	</div>

			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-xl-12">
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__body">
				<div class="kt-widget kt-widget--user-profile-3">
					<div class="kt-widget__top">
						<div class="kt-widget__media kt-hidden-">
								<img src="{{$customer->image == null ? 'https://via.placeholder.com/300.png/09f/fffC/' : asset('uploads/'.$customer->image)}}" alt="image">
						</div>
						<div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">

						</div>

						<div class="kt-widget__content">
							<div class="kt-widget__head">
								<a href="#" class="kt-widget__username">
									{{$customer->user->name}}

								</a>
								<div class="kt-widget__action">
									<button id="toggleActivation" data-id="{{$customer->user->id}}" type="button" class="btn {{$customer->user->isActive  ==  1 ? 'btn-danger' : 'btn-success'}}">

											{{$customer->user->isActive  ==  1 ? 'deactivate' : 'activate'}}
										</button>
								</div>
							</div>
						</div>
					</div>

					<div class="kt-widget__bottom">
						<div class="kt-widget__item">
							<div class="kt-widget__icon">
								<i class="flaticon-profile-1"></i>
							</div>
							<div class="kt-widget__details">
								<span class="kt-widget__title">Name</span>
								<span class="kt-widget__value">{{$customer->user->name}}</span>
							</div>
						</div>
						<div class="kt-widget__item">
							<div class="kt-widget__icon">
								<i class="flaticon2-email"></i>
							</div>
							<div class="kt-widget__details">
								<span class="kt-widget__title">Email</span>
								<span class="kt-widget__value">{{$customer->user->email}}</span>
							</div>
						</div>
						<div class="kt-widget__item">
							<div class="kt-widget__icon">
								<i class="flaticon2-phone"></i>
							</div>
							<div class="kt-widget__details">
								<span class="kt-widget__title">Phone</span>
								<span class="kt-widget__value">{{$customer->user->phone}}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xl-12">
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__body">
				<div class="kt-widget__top">
					<div class="kt-widget__content">
						<div class="kt-widget__head">
							<h3>Customer Addresses</h3>
						</div>
					</div>
				</div>

				<div class="kt-widget__bottom">
					<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer ">
						<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="customerAddressesTable" role="grid" aria-describedby="kt_table_1_info" style="width: 1221px;">
							<thead>
								<tr role="row">
									<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">id</th>
									<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">Building No</th>

									<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">Floor No</th>

									<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">Flat No</th>

									<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">street</th>

									<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">city</th>

									<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">region</th>

									<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">type</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">

	<div class="col-xl-12">
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__body">
				<div class="kt-widget__top">
					<div class="kt-widget__content">
						<div class="kt-widget__head">
							<h3>Customer Orders</h3>
						</div>


					</div>
				</div>

				<div class="kt-widget__bottom">
					<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer js-region-table">
						<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="customerOrdersTable" role="grid" aria-describedby="kt_table_1_info" style="width: 1221px;">
							<thead>
								<tr role="row">
									<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">orderNo</th>

									<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">Status</th>

									<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">Total</th>

									<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">Subtotal</th>

									<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">Actions</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>



<div class="row">
	<div class="col-xl-12">
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__body">
				<div class="kt-widget__top">
					<div class="kt-widget__content">
						<div class="kt-widget__head">
							<h3>Customer Invoices</h3>
						</div>
					</div>
				</div>
				<div class="kt-widget__bottom">
					<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer js-region-table">
						<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="customerOrdersInvoices" role="grid" aria-describedby="kt_table_1_info" style="width: 1221px;">
							<thead>
								<tr role="row">
									<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">invoiceNo</th>

									<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">Status</th>


									<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">Tax</th>

									<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">Delivery Cost</th>


									<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">Subtotal</th>

									<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">Total</th>

									<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">Actions</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xl-12">
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__body">
				<div class="kt-widget__top">
					<div class="kt-widget__content">
						<div class="kt-widget__head">
							<h3>Customer Points</h3>
						</div>
					</div>
				</div>
				<div class="kt-widget__bottom">
					<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer js-region-table">
						<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="customerPoints" role="grid" aria-describedby="kt_table_1_info" style="width: 1221px;">
							<thead>
								<tr role="row">
									<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">Id</th>



									<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">Points</th>


									<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">Expire Date</th>

									<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">Expire Days</th>

									<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">Actions</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</div>
@endsection
@push('scripts')

<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
<script src="{{asset('js/dashboard/customerProfile.js')}}"></script>

<script>

	$(document).ready(function(){

		var $wrapper = $('.js-wrapper');
		var customer_id = {!! json_encode($customer->id, JSON_HEX_TAG) !!};
		var repLogApp = new customerProfile($wrapper,customer_id);
	});

</script>

@endpush
