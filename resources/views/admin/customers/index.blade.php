@extends('admin.layout.admin')

@push('styles')

<link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">

@endpush

@section('title','Customers')


@section('content')



<div class="kt-portlet kt-portlet--mobile js-wrapper">

	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
				customers table
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">

					&nbsp;
					@can('customer_create')
					<a href="#" id="addNewDriver"  class="btn btn-brand btn-elevate btn-icon-sm">
						<i class="la la-plus"></i>
						Add New Customer
					</a>
					@endcan
				</div>
			</div>
		</div>
	</div>

	<div class="kt-portlet__body">
		<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer js-region-table">
			<div class="row">
				<div class="col-sm-12">
					<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="drivers_table" role="grid" aria-describedby="kt_table_1_info" style="width: 1221px;">
						<thead>
							<tr role="row">
								<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">id</th>
								<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">name</th>

								<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">email</th>

								<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">status</th>

								<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 97.5px;" aria-label="Actions">Actions</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>


	<!-- begain modal -->
	<div class="modal fade show" id="newDriverModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" style="display: hidden; padding-right: 17px;">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">New Customer</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<form class="kt-form kt-form js-new-driver-form" enctype="multipart/form-data" data-url="{{route('customers.store')}}">
						<div class="kt-portlet__body">
							<div class="kt-section">
								<div class="kt-section__title">
									Baisc Info
								</div>
								<div class="kt-section__content">
									<div class="row">
										<div class="col-sm-3">
											<div class="form-group validate {{$errors->has('name') ? 'is-invalid' : ''}}">
												<label class="form-control-label">*Name:</label>
												<input type="text" name="name" class="form-control">
												@error('name')
											    	<div id="name-error" class="error invalid-feedback">{{$message}}</div>
												@enderror
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group validate {{$errors->has('email') ? 'is-invalid' : ''}}">
												<label class="form-control-label">*Email:</label>
												<input type="text" name="email" class="form-control">
												@error('name')
											    	<div id="email-error" class="error invalid-feedback">{{$message}}</div>
												@enderror
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group validate {{$errors->has('password') ? 'is-invalid' : ''}}">
												<label class="form-control-label">*password:</label>
												<input type="password" name="password" class="form-control">
												@error('password')
											    	<div id="password-error" class="error invalid-feedback">{{$message}}</div>
												@enderror
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group validate">
												<label class="form-control-label">address:</label>
												<input type="text" name="address" class="form-control">
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<label class="form-control-label">phone:</label>
												<input type="text" name="phone" class="form-control">
												
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							<button type="reset" class="btn btn-secondary">reset</button>
							<button type="submit" id="new" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- end modal -->
</div>



@endsection
@push('scripts')


<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
<script src="{{asset('js/dashboard/customer.js')}}"></script>
<script src="{{asset('js/dashboard/bootstrap-timepicker.js')}}" type="text/javascript"></script>
<script>
	var can = {
		view: false
	}

	@can('customer_view')
		can.view = true
	@endcan
</script>
<script>



	$(document).ready(function(){

		var $wrapper = $('.js-wrapper');
		var repLogApp = new drivers($wrapper);
	});
</script>
@endpush
