@extends('admin.layout.admin')

@push('styles')

    <link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">

@endpush

@section('title','Driver Reports')


@section('content')

    <br>
    <div class="kt-portlet kt-portlet--mobile js-wrapper">

        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
                <h3 class="kt-portlet__head-title">
                    Drivers Reports table
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">

                    </div>
                </div>
            </div>
        </div>

        <div class="kt-portlet__body">

            <form action="" class="kt-form kt-form--fit kt-margin-b-20">
                <div class="row kt-margin-b-20">
                    <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                        <label>Date:</label>
                        <div class="input-daterange input-group" id="kt_datepicker">
                            <input type="text" class="form-control kt-input datepicker" name="from" placeholder="From" data-col-index="5">
                            <div class="input-group-append">
						<span class="input-group-text"><i class="la la-ellipsis-h"></i>
						</span>
                            </div>
                            <input type="text" class="form-control kt-input datepicker" name="to" placeholder="To" data-col-index="5">
                        </div>
                    </div>

                    <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                        <label>Driver:</label>
                        <select name="driver_id"  class="form-control kt-input driver_select">
                            <option selected></option>
                            @foreach($drivers as $driver)
                                <option value="{{$driver->id}}">{{$driver->user->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                        <label>City:</label>
                        <select name="city_id"  class="form-control kt-input city_select">
                            <option selected></option>
                            @foreach($cities as $city)
                                <option value="{{$city->id}}">{{$city->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="kt-separator kt-separator--md kt-separator--dashed"></div>
                <div class="row">
                    <div class="col-lg-12">
                        <button
                            class="btn btn-primary btn-brand--icon" id="kt_search">
						<span>
							<i class="la la-search"></i>
							<span>Search</span>
						</span>
                        </button>
                        &nbsp;&nbsp;
                        <button class="btn btn-secondary btn-secondary--icon" id="kt_reset">
						<span>
							<i class="la la-close"></i>
							<span>Reset</span>
						</span>
                        </button>
                    </div>
                </div>
            </form>
            <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer js-cat-table">

                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="driver_report_table" role="grid" aria-describedby="kt_table_1_info" style="width: 1221px;">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">City</th>

                                <th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="Title: activate to sort column ascending">OrderNo</th>

                                <th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="Title: activate to sort column ascending">Driver</th>

                                <th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="Title: activate to sort column ascending">Delivery Cost</th>
                            </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th colspan="3" style="text-align:right">Total:</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@push('scripts')


    <script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

    <script>

        $(document).ready(function(){

            $(document).on('click','#kt_search',function(e){
                e.preventDefault();
                $('#driver_report_table').DataTable().ajax.reload();
            });

            $(document).on('click','#kt_reset',function(e){
                e.preventDefault();
                $('.kt-input').each(function() {
                    $(this).val('');
                    $('#driver_report_table').DataTable().column($(this).data('col-index')).search('', false, false);
                });

                $('#driver_report_table').DataTable().ajax.reload();

                $('.driver_select').val('').trigger('change');
                $('.city_select').val('').trigger('change');

            });


            $('.driver_select').select2({
                placeholder: 'Select Driver',
                allowClear: true
            });

            $('.city_select').select2({
                placeholder: 'Select City',
                allowClear: true
            });

            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd',
                orientation: "bottom"
            });

            $('#driver_report_table').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                ajax: {
                    url:'/dashboard/reports/drivers',
                    data: function (d) {
                        d.from = $('input[name=from]').val();
                        d.to = $('input[name=to]').val();
                        d.driver_id = $(".driver_select option:selected").val();
                        d.city_id = $(".city_select option:selected").val();
                    }
                },
                columns: [
                    {data: 'city', name: 'city'},
                    {data: 'orderNo', name: 'orderNo'},
                    {data: 'user', name: 'user'},
                    {data: 'delivery_cost', name: 'invoices.delivery_cost'},
                ],
                footerCallback: function(row, data, start, end, display ) {

                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column( 3 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column( 3, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Update footer
                    $( api.column( 3 ).footer() ).html(
                        '$'+pageTotal +' ( $'+ total +' total)'
                    );
                },
            });
            // var repLogApp = new facilityReport($wrapper);
        });
    </script>
@endpush
