@extends('admin.layout.admin')

@push('styles')
<link rel="stylesheet"  href="{{asset('assets/css/pages/invoices/invoice-2.css')}}">

@endpush

@section('title','Invoices')


@section('content')
<br>

<div class="kt-portlet">
	<div class="kt-portlet__body kt-portlet__body--fit">
		<div class="kt-invoice-2">
			<!-- invoice head start -->
			<div class="kt-invoice__head">
				<div class="kt-invoice__container">
					<div class="kt-invoice__brand">
						<h1 class="kt-invoice__title" style="margin-top: 55px">{{$invoice->invoiceno}}</h1>
						<div href="#" class="kt-invoice__logo">
							@if(isset($setting) && $setting->logo != null)
							<a href="#"><img width="150" src="{{asset('uploads/'.$setting->logo)}}">
							</a>
							@endif
							@if(isset($setting) && $setting->address != null)
							<span class="kt-invoice__desc">
								<span>$setting->address</span>
							</span>
							@endif
						</div>
					</div>

					<div class="kt-invoice__items">
						<div class="kt-invoice__item">
							<span class="kt-invoice__subtitle">DATE</span>
							<span class="kt-invoice__text">{{$invoice->created_at->diffForHumans()}}</span>
						</div>
						<div class="kt-invoice__item">
							<span class="kt-invoice__subtitle">ORDER NO.</span>
							<span class="kt-invoice__text">{{$invoice->order->uuId}}</span>
						</div>
						<div class="kt-invoice__item">
							<span class="kt-invoice__subtitle">STATUS</span>
							<span class="kt-invoice__text">
								@if($invoice->invoice_status_id == 1)
									<span class="kt-badge  kt-badge--info kt-badge--inline kt-badge--pill">issued</span>
								@endif
								@if($invoice->invoice_status_id == 2)
									<span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill">cancelled</span>
								@endif
								@if($invoice->invoice_status_id == 3)
									<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">completed</span>
								@endif
							</span>
						</div>
						{{-- <div class="kt-invoice__item">
							<span class="kt-invoice__subtitle">INVOICE TO.</span>
							<span class="kt-invoice__text">
								{{$invoice->order->address->address}}
							</span>
						</div> --}}
						<div class="kt-invoice__item">
							<span class="kt-invoice__subtitle">CUSTOMER</span>
							<span class="kt-invoice__text">
								{{$invoice->order->customer->user->name}}
							</span>
						</div>
					</div>
				</div>
			</div>
			<!-- invoice head end -->

			<!-- invoice body start -->
			<div class="kt-invoice__body">
				<div class="kt-invoice__container">
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>TYPE</th>
									<th>SERVICE</th>
									<th>AMOUNT</th>
								</tr>
							</thead>
							<tbody>
								@foreach($invoice->order->items as $item)
									<tr>
										<td>{{$item->type->name}}</td>
										<td>{{$item->service->name}}</td>
										<td class="kt-font-danger kt-font-lg">{{$item->total_amount}}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- invoice body end -->

			<!-- invoice footer start -->
			<div class="kt-invoice__footer">
				<div class="kt-invoice__container">
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
                                    <th>Facility</th>
                                    <th>Facility Amount</th>
                                    <th>Urgent Amount</th>
                                    <th>DELIVERY COST</th>
									<th>TAX</th>
									<th>SUBTOTAL</th>
									<th>TOTALAMOUNT</th>
								</tr>
							</thead>
							<tbody>
								<tr>
                                    <td>{{optional($invoice->order->facility)->name}}</td>
                                    <td>{{optional($invoice->order)->facility_amount}}  {{$invoice->order->isPercentage == 1 ? '%' : ''}}</td>
                                    <td>{{$invoice->order->is_urgent == 1 ? 'X2' : ''}}</td>
									<td>{{$invoice->delivery_cost}}</td>
									<td>{{$invoice->tax}}</td>
									<td>{{$invoice->subtotal}}</td>
									<td class="kt-font-danger kt-font-lg">{{$invoice->total}}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- invoice footer end -->

			<!-- invoice action start -->
			<div class="kt-invoice__actions">
				<div class="kt-invoice__container">
					<a type="button" class="btn btn-label-brand btn-bold" href="{{route('invoices.download',$invoice->id)}}">Download Invoice</a>
					<button type="button" class="btn btn-brand btn-bold" onclick="window.print();">Print Invoice</button>
				</div>
			</div>
			<!-- invoice action end -->
		</div>
	</div>
</div>
@endsection
@push('scripts')

@endpush
