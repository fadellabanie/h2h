@extends('admin.layout.admin')

@push('styles')

<link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">

@endpush

@section('title','Invoices')


@section('content')


<br>
<div class="kt-portlet kt-portlet--mobile js-wrapper">

	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
				Invoices table
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
					
				</div>
			</div>
		</div>
	</div>

	<div class="kt-portlet__body">

		<form action="" class="kt-form kt-form--fit kt-margin-b-20">
			<div class="row kt-margin-b-20">
				<div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
					<label>InvoiceNo:</label>
					<input type="text" name="invoiceno" class="form-control kt-input" data-col-index="0">
				</div>

				<div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
					<label>OrderNo:</label>
					<input type="text" name="orderNo" class="form-control kt-input" data-col-index="0">
				</div>

				<div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
					<label>Customer:</label>
					<select name="customer_id"  class="form-control kt-input customerSelect"></select>
				</div>
			</div>

			<div class="row kt-margin-b-20">
				<div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
					<label>Ship Date:</label>
					<div class="input-daterange input-group" id="kt_datepicker">
						<input type="text" class="form-control kt-input datepicker" name="from" placeholder="From" data-col-index="5">
					<div class="input-group-append">
						<span class="input-group-text"><i class="la la-ellipsis-h"></i>
						</span>
					</div>
						<input type="text" class="form-control kt-input datepicker" name="to" placeholder="To" data-col-index="5">
					</div>
				</div>

				<div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
					<label>Status:</label>
					<select name="status_id"  class="form-control kt-input status_select">
						<option selected></option>
						<option value="1">Issued</option>
						<option value="2">Cancelled</option>
						<option value="3">Completed</option>
					</select>
				</div>
			</div>
			<div class="kt-separator kt-separator--md kt-separator--dashed"></div>
			<div class="row">
				<div class="col-lg-12">
					<button 
				class="btn btn-primary btn-brand--icon" id="kt_search">
						<span>
							<i class="la la-search"></i>
							<span>Search</span>
						</span>
					</button>
					&nbsp;&nbsp;
					<button class="btn btn-secondary btn-secondary--icon" id="kt_reset">
						<span>
							<i class="la la-close"></i>
							<span>Reset</span>
						</span>
					</button>
				</div>
			</div>
		</form>
		<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer js-cat-table">

			<div class="row">
				<div class="col-sm-12">
					<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="invoices_table" role="grid" aria-describedby="kt_table_1_info" style="width: 1221px;">
						<thead>
							<tr role="row">
								<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">Id</th>

								<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">invoiceNo</th>

								<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">orderNo</th>

								<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="Title: activate to sort column ascending">customer</th>

								

								<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="Title: activate to sort column ascending">status</th>

								<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="Title: activate to sort column ascending">issued_at</th>

								<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 97.5px;" aria-label="Actions">Actions</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>



@endsection
@push('scripts')


<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
<script src="{{asset('js/dashboard/invoices.js')}}"></script>
<script>
	var can = {
		view:false,
	}

	@can('invoice_view')
		can.view = true
	@endcan
</script>
<script>


    $(document).ready(function(){
            var $wrapper = $('.js-wrapper');
            var repLogApp = new invoices($wrapper);
    });
</script>
@endpush
