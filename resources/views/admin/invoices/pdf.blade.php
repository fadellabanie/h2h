<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta charset="UTF-8">
	<title>Invoice</title>
	<style>
		.attendance-table table{
		  width: 100%;
		  border-collapse: collapse;
		  border: 1px solid #000;
		}

		.blank-cell{

		  min-width: 50px;


		}

		.attendance-cell{

		  padding: 8px;


		}

		.attendance-table table th.attendance-cell, .attendance-table table td.attendance-cell {
		    border: 1px solid #000;
		}
	</style>
</head>
<body>
	
	<h3>{{$invoice->invoiceno}}</h3>
	

	<div class="attendance-table">
		
		<table class="table-bordered">
			<thead>
				<tr>
					<th class="attendance-cell">TYPE</th>
					<th class="attendance-cell">COLOR</th>
					<th class="attendance-cell">SIZE</th>
					<th class="attendance-cell">BRAND</th>
					<th class="attendance-cell">SERVICE</th>
					<th class="attendance-cell">FACILITY</th>
					<th class="attendance-cell">AMOUNT</th>
					<th class="attendance-cell">TOTAL AMOUNT</th>
				</tr>
			</thead>
			<tbody>
				@foreach($invoice->order->items as $item)
					<tr>
						<td class="attendance-cell">{{$item->type->name}}</td>
						<td class="attendance-cell">{{optional($item->color)->name}}</td>
						<td class="attendance-cell">{{optional($item->size)->name}}</td>
						<td class="attendance-cell">{{optional($item)->brand}}</td>
						<td class="attendance-cell">{{$item->service->name}}</td>
						<td class="attendance-cell">{{isset($item->facilities[0]) ? $item->facilities[0]['name'] : ''}}</td>
						<td class="attendance-cell">{{$item->amount}}</td>
						<td class="attendance-cell">{{$item->total_amount}}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		
		<br>

		<table class="table-bordered">
			<thead>
				<tr>
					<th class="attendance-cell">Tax</th>
					<th class="attendance-cell">Delivery Cost</th>
					<th class="attendance-cell">Subtotal</th>
					<th class="attendance-cell">Total</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="attendance-cell">{{$invoice->tax}}</td>
					<td class="attendance-cell">{{$invoice->delivery_cost}}</td>
					<td class="attendance-cell">{{$invoice->subtotal}}</td>
					<td class="attendance-cell">{{$invoice->total}}</td>
				</tr>			
			</tbody>
		</table>
	</div>

</body>
</html>