<div class="row">
	<div class="col-xl-12">

		<!--begin:: Widgets/Applications/User/Profile3-->
		<div class="kt-portlet kt-portlet--height-fluid">
			<div class="kt-portlet__body">
				<div class="kt-widget kt-widget--user-profile-3">
					<div class="kt-widget__top">
						<div class="kt-widget__content">
							<div class="kt-widget__head">
								<a href="#" class="kt-widget__username">
									{{$promoCode->name}}
								</a>
								<div class="kt-widget__action">
									
									@if($promoCode->status == 1)
										<a href="{{route('promoCodes.toggle',$promoCode->id)}}" class="btn btn-label-danger btn-sm btn-upper">
											Deactive
										</a>
									@else
										<a class="btn btn-label-success btn-sm btn-upper" href="{{route('promoCodes.toggle',$promoCode->id)}}">Activate</a>
										
									@endif
									

								</div>
							</div>
							
							<div class="kt-widget__info">
								<div class="kt-widget__desc">
									{{$promoCode->description}}
								</div>
								<div class="kt-widget__progress">
									<div class="kt-widget__text">
										Code
									</div>
									<span class="kt-badge kt-badge--danger kt-badge--inline">
										{{$promoCode->code}}				
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="kt-widget__bottom">
						<div class="kt-widget__item">
							<div class="kt-widget__icon">
								<i class="flaticon-event-calendar-symbol"></i>
							</div>
							<div class="kt-widget__details">
								<span class="kt-widget__title">From</span>
								<span class="kt-widget__value">
									{{$promoCode->from}}
								</span>
							</div>
						</div>
						<div class="kt-widget__item">
							<div class="kt-widget__icon">
								<i class="flaticon-event-calendar-symbol"></i>
							</div>
							<div class="kt-widget__details">
								<span class="kt-widget__title">To</span>
								<span class="kt-widget__value">
									{{$promoCode->to}}
								</span>
							</div>
						</div>
						
						<div class="kt-widget__item">
							<div class="kt-widget__icon">
								<i class="flaticon-pie-chart"></i>
							</div>
							<div class="kt-widget__details">
								<span class="kt-widget__title">Amount</span>
								<span class="kt-widget__value">
									<span>
										@if($promoCode->isPercentage == 1)
											%
										@else
											$
										@endif
									</span>{{$promoCode->amount}}</span>
							</div>
						</div>
						<div class="kt-widget__item">
							<div class="kt-widget__icon">
								@if($promoCode->status == 1)
									<span class="kt-badge kt-badge--unified-success kt-badge--lg kt-badge--bold">
										<i class="flaticon2-check-mark"></i>
									</span>
								@else
									<span class="kt-badge kt-badge--unified-danger kt-badge--lg kt-badge--bold">
										<i class="flaticon2-cancel-music"></i>
									</span>
								@endif
							</div>
							<div class="kt-widget__details">
								<span class="kt-widget__title">Status</span>
								<span class="kt-widget__value">
									@if($promoCode->status == 1)
										Active
									@else
										Not Active
									@endif
								</span>
							</div>
						</div>
						<div class="kt-widget__item">
							<div class="kt-widget__icon">
								<i class="flaticon-network"></i>
							</div>
							<div class="kt-widget__details">
								<span class="kt-widget__title">Customers</span>
								<a href="{{route('promoCodes.customers',$promoCode->id)}}" class="kt-widget__value kt-font-brand">View</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>