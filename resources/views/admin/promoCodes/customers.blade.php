@extends('admin.layout.admin')

@push('styles')

<link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">

@endpush

@section('title','promoCodes')


@section('content')

<br>

@include('admin.promoCodes.main')

<div class="row js-wrapper">
	<div class="col-xl-12">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">Customers 
						<small>Customers whose use this code</small>
					</h3>
				</div>
			</div>
			
			<div class="kt-portlet__body">
				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer js-promoCustomer-table">
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="js-promoCustomer-table" role="grid" aria-describedby="kt_table_1_info" style="width: 1221px;">
								<thead>
									<tr role="row">
										

										<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="gift: activate to sort column ascending">Name</th>

										<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="point: activate to sort column ascending">Email</th>

										<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 97.5px;" aria-label="Actions">Actions</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('scripts')

<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script src="{{asset('js/dashboard/promoCustomers.js')}}"></script>

<script>


    $(document).ready(function(){

    	var $wrapper = $('.js-promoCustomer-table');
    	var id = {!! json_encode($promoCode->id, JSON_HEX_TAG) !!};
        var repLogApp = new promoCustomer($wrapper,id);

    });


</script>

@endpush
