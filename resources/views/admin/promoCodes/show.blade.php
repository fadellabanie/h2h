@extends('admin.layout.admin')

@push('styles')

<link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">

@endpush

@section('title','promoCodes')


@section('content')

<br>

@include('admin.promoCodes.main')

<div class="row js-wrapper">
	<div class="col-xl-12">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">Code Information 
						<small>update Code informaiton</small>
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				
				<form action="{{route('promoCodes.update',$promoCode->id)}}" method="post">
					{{csrf_field()}}
					{{ method_field('PATCH') }}
					<div class="kt-form kt-form--label-right">
						<div class="kt-form__body">
							<div class="kt-section kt-section--first">
								<div class="kt-section__body">

									<div class="form-group row validate {{$errors->has('name') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">*Name</label>
										<div class="col-lg-9 col-xl-6">
											  <input name="name" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" type="text" value="{{$promoCode->name}}">
										</div>
									</div>

									<div class="form-group row validate {{$errors->has('code') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">*Code</label>
										<div class="col-lg-9 col-xl-6">
											  <input name="code" class="form-control {{$errors->has('code') ? 'is-invalid' : ''}}" type="text" value="{{$promoCode->code}}">

											  @error('code')
												<div id="code-error" class="error invalid-feedback">{{$message}}</div>
											@enderror
										</div>
									</div>

									<div class="form-group row validate {{$errors->has('name') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">Description</label>
										<div class="col-lg-9 col-xl-6">
											<textarea name="description" class="form-control {{$errors->has('description') ? 'is-invalid' : ''}}">{{$promoCode->description}}</textarea>
										</div>
									</div>
									
									<div class="form-group row validate {{$errors->has('amount') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">*Amount</label>
										<div class="col-lg-9 col-xl-6">
											  <input name="amount" class="form-control {{$errors->has('amount') ? 'is-invalid' : ''}}" type="text" value="{{$promoCode->amount}}">
										</div>
									</div>

									<div class="form-group row validate {{$errors->has('from') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">*From</label>
										<div class="col-lg-9 col-xl-6">
											  <input name="from" class="form-control datepicker {{$errors->has('from') ? 'is-invalid' : ''}}" type="text" value="{{$promoCode->from}}">
										</div>
									</div>

									<div class="form-group row validate {{$errors->has('to') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">*To</label>
										<div class="col-lg-9 col-xl-6">
											  <input name="to" class="form-control to {{$errors->has('to') ? 'is-invalid' : ''}}" type="text" value="{{$promoCode->to}}">
										</div>
									</div>

									<div class="form-group row validate">
										<label class="col-xl-3 col-lg-3 col-form-label">isPercentage</label>
										<div class="col-lg-9 col-xl-6">
											<span class="kt-switch kt-switch--icon">
												<label>
													<input 
													type="checkbox" 
													name="isPercentage"
													value="{{$promoCode->isPercentage}}" 
													{{$promoCode->isPercentage == 1 ? 'checked' : ''}}
													id="isPercentage">
													<span></span>
												</label>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<div class="row">
									<div class="col-lg-3 col-xl-3"></div>
									<div class="col-lg-9 col-xl-9">
										@can('promocode_edit')
										<button id="" type="submit" class="btn btn-primary">Submit</button>
										@endcan
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@push('scripts')


<script>


    $(document).ready(function(){

		var today = new Date();
		
    	$('.datepicker').datepicker({
			format: 'yyyy-mm-dd'
		});

    	$('.to').datepicker({
			format: 'yyyy-mm-dd',
			// startDate: today 
		});

        $('#isPercentage').on('click',function(e){
        	var elm = $(e.currentTarget);
			if (elm.val() == 0) {
				elm.val(1);
			}else{
				elm.val(0);
			}
        });
    });


</script>

@endpush
