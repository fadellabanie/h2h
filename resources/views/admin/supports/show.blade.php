@extends('admin.layout.admin')

@push('styles')

    <link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">

@endpush

@section('title','Ticket')
@section('content')
<br>
<div class="row">
    <div class="col-xl-12">
        <!--begin:: Widgets/Applications/User/Profile3-->
        <div class="kt-portlet kt-portlet--height-fluid">
            <div class="kt-portlet__body">
                <div class="kt-widget kt-widget--user-profile-3">
                    <div class="kt-widget__top">
                        <div class="kt-widget__content">
                            <div class="kt-widget__head">
                                <a href="#" class="kt-widget__username">
                                    {{$support->title}}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="kt-widget__bottom">
                        <div class="kt-widget__item">
                            <div class="kt-widget__icon">
                                <i class="flaticon-event-calendar-symbol"></i>
                            </div>
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Date</span>
                                <span class="kt-widget__value">
									{{$support->created_at->diffForHumans()}}
								</span>
                            </div>
                        </div>
                        <div class="kt-widget__item">
                            <div class="kt-widget__icon">
                                <i class="flaticon-profile-1"></i>
                            </div>
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Customer Name</span>
                                <span class="kt-widget__value">
									{{$support->customer->user->name}}
								</span>
                            </div>
                        </div>
                        <div class="kt-widget__item">
                            <div class="kt-widget__icon">
                                <i class="flaticon2-email"></i>
                            </div>
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Email</span>
                                <span class="kt-widget__value">{{$support->customer->user->email}}</span>
                            </div>
                        </div>
                        <div class="kt-widget__item">
                            <div class="kt-widget__icon">
                                <i class="flaticon2-phone"></i>
                            </div>
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Phone</span>
                                <span class="kt-widget__value">{{$support->customer->user->phone}}</span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="kt-portlet kt-portlet--height-fluid">
            <div class="kt-portlet__body">
                <div class="kt-widget__top">
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <h3>Description</h3><br>
                        </div>
                    </div>
                </div>
                <div class="kt-widget__bottom">
                    {{$support->description}}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="kt-portlet kt-portlet--height-fluid">
            <div class="kt-portlet__body">
                <div class="kt-widget__top">
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <h3>Reply By Mail</h3><br>
                        </div>
                    </div>
                </div>
                <div class="kt-widget__bottom">
                    <form action="{{route('support.reply',$support->id)}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group validate {{$errors->has('name') ? 'is-invalid' : ''}}">
                            <label class="form-control-label">*Reply:</label>
                            <textarea name="reply" id="" cols="30" rows="10" class="form-control">{{$support->reply}}</textarea>
                        </div>
                        @if($support->reply == null)
                            <button class="btn btn-success" type="submit">Send</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
