@extends('admin.layout.admin')

@push('styles')

    <link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">

@endpush

@section('title','support tickets')


@section('content')

    <div class="kt-portlet kt-portlet--mobile js-wrapper">

        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
                <h3 class="kt-portlet__head-title">
                    support table
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-portlet__body">
            <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="support_table" role="grid" aria-describedby="kt_table_1_info" style="width: 1221px;">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">id</th>
                                <th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">title</th>
                                <th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">customer</th>
                                <th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">date</th>

                                <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 97.5px;" aria-label="Actions">Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@push('scripts')


    <script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
    <script src="{{asset('js/dashboard/support.js')}}"></script>

    <script>



        $(document).ready(function(){

            var $wrapper = $('.js-wrapper');
            var repLogApp = new supports($wrapper);
        });
    </script>
@endpush
