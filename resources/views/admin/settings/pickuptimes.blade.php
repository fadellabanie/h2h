<div class="row">
	<div class="col-sm-12">
		<a href="#" id="newpickup" class="btn btn-brand btn-elevate btn-icon-sm" style="float: right">
			<i class="la la-plus"></i>
			New PickupTime
		</a>
		<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="pickupTable" role="grid" aria-describedby="kt_table_1_info" style="width: 100%;">
			<thead>
				<tr role="row">
					<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="gift: activate to sort column ascending">Id</th>

					<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="gift: activate to sort column ascending">From</th>

					<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="point: activate to sort column ascending">To</th>

					<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 97.5px;" aria-label="Actions">Actions</th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<div class="modal fade show" id="newpickupModal"  role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; display: none;" aria-modal="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">pickupTime</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<form enctype="multipart/form-data" class="kt-form kt-form--label-right js-new-pickup-form" data-url="{{route('timepicker.store')}}"  novalidate="novalidate">
						<div class="kt-portlet__body">
							<div class="form-group row validate">
								<label class="col-form-label col-lg-3 col-sm-12">* From</label>
								<div class="col-lg-9 col-md-9 col-sm-12">
									<input type="text"  class="form-control timepicker" name="from" >
								</div>
							</div>							

							<div class="form-group row validate">
								<label class="col-form-label col-lg-3 col-sm-12">* To</label>
								<div class="col-lg-9 col-md-9 col-sm-12">
									<input type="text"  class="form-control timepicker" name="to">
								</div>
							</div>
						</div>
						<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<div class="row">
									<div class="col-lg-9 ml-lg-auto">
										<button type="submit" id="savePickTime" class="btn btn-brand">save</button>
										<button type="reset" class="btn btn-secondary">reset</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>	
