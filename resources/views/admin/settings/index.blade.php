@extends('admin.layout.admin')

@push('styles')


<link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">


@endpush

@section('title','settings')

@section('content')

<br>

<div class="row js-wrapper" data-sticky-container>
	<div class="col-3 col-sm-2">
		<div class="kt-portlet sticky" data-sticky="true" data-margin-top="100px" data-sticky-for="1023" data-sticky-class="kt-sticky">
			<div class="kt-portlet__body kt-portlet__body--fit" id="tabs">
				<ul class="kt-nav kt-nav--bold kt-nav--md-space kt-nav--v3 kt-margin-t-20 kt-margin-b-20 nav nav-tabs" id="myTab" role="tablist">

				  <li class="kt-nav__item active tab">
				    <a class="kt-nav__link" id="setting-tab" data-toggle="tab" href="#setting" role="tab" aria-controls="setting" aria-selected="true">
				    	<span class="kt-nav__link-icon"><i class="flaticon-settings"></i></span>
						<span class="kt-nav__link-text">Settings</span>
				    </a>
				  </li>

				  <li class="kt-nav__item tab">
				    <a class="kt-nav__link" id="pickup-tab" data-toggle="tab" href="#pickup" role="tab" aria-controls="pickup" aria-selected="false">
				    	<span class="kt-nav__link-icon"><i class="flaticon-time"></i></span>
						<span class="kt-nav__link-text">Pickup Times</span>
				    </a>
				  </li>

				  <li class="kt-nav__item tab">
				    <a class="kt-nav__link" id="pages-tab" data-toggle="tab" href="#pages" role="tab" aria-controls="pages" aria-selected="false">
				    	<span class="kt-nav__link-icon"><i class="flaticon-clipboard"></i></span>
						<span class="kt-nav__link-text">Mobile Pages</span>
				    </a>
				  </li>
				</ul>
			</div>
		</div>
	</div>

	<div class="col-lg-10 col-xl-10">
		<div class="kt-portlet">
			<div class="kt-portlet__body">
				<div class="tab-content">
				  	<div class="tab-pane active" id="setting" role="tabpanel" aria-labelledby="setting-tab">
				  		<form method="POST" action="{{route('settings.save')}}" enctype="multipart/form-data">
				  			{{csrf_field()}}
				  			<div class="form-group row">
								<label class="col-xl-3 col-lg-3 col-form-label">Logo</label>
								<div class="col-lg-9 col-xl-6">
									<div class="kt-avatar kt-avatar--outline" id="kt_user_avatar_1">
										<div class="kt-avatar__holder" style="background-image: url({{optional($setting)->logo ? asset('uploads/'.optional($setting)->logo) : 'https://via.placeholder.com/300.png/09f/fffC/'}})">
										</div>
										<label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
										<i class="fa fa-pen"></i>
										<input type="file" name="logo" value="" accept=".png, .jpg, .jpeg">
										</label>
										<span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
											<i class="fa fa-times"></i>
										</span>
									</div>
									<span class="form-text text-muted">Allowed file types: png, jpg, jpeg.</span>
								</div>
							</div>

							<div class="form-group row validate {{$errors->has('fixed_point') ? 'is-invalid' : ''}}">
								<label class="col-xl-3 col-lg-3 col-form-label">*Fixed Points For Ten QTR</label>
								<div class="col-lg-9 col-xl-6">
									<input type="text" class="form-control {{$errors->has('fixed_point') ? 'is-invalid' : ''}}" name="fixed_point" value="{{optional($setting)->fixed_point}}">
									@error('fixed_point')
										<div id="fixed_point-error" class="error invalid-feedback">{{$message}}</div>
									@enderror
								</div>
							</div>

							<div class="form-group row validate {{$errors->has('expire_days') ? 'is-invalid' : ''}}">
								<label class="col-xl-3 col-lg-3 col-form-label">expire_days</label>
								<div class="col-lg-9 col-xl-6">
									<input type="text" class="form-control {{$errors->has('expire_days') ? 'is-invalid' : ''}}" name="expire_days" value="{{optional($setting)->expire_days}}">
									@error('expire_days')
										<div id="expire_days-error" class="error invalid-feedback">{{$message}}</div>
									@enderror
								</div>
							</div>

							<div class="form-group row validate {{$errors->has('tax') ? 'is-invalid' : ''}}">
								<label class="col-xl-3 col-lg-3 col-form-label">Tax</label>
								<div class="col-lg-9 col-xl-6">
									<input type="text" class="form-control {{$errors->has('tax') ? 'is-invalid' : ''}}" name="tax" value="{{optional($setting)->tax}}">
									@error('tax')
										<div id="tax-error" class="error invalid-feedback">{{$message}}</div>
									@enderror
								</div>
							</div>

							<div class="form-group row validate {{$errors->has('address') ? 'is-invalid' : ''}}">
								<label class="col-xl-3 col-lg-3 col-form-label">Address</label>
								<div class="col-lg-9 col-xl-6">
									<input type="text" class="form-control {{$errors->has('address') ? 'is-invalid' : ''}}" name="address" value="{{optional($setting)->address}}">
									@error('address')
										<div id="tax-error" class="error invalid-feedback">{{$message}}</div>
									@enderror
								</div>
							</div>
							<div class="col-lg-9 col-xl-9">

								<button class="btn btn-primary" type="submit">Save</button>
							</div>
				  		</form>
					</div>
				  	<div class="tab-pane" id="pickup" role="tabpanel" aria-labelledby="pickup-tab">
				  		@include('admin.settings.pickuptimes')
				  	</div>

				  	<div class="tab-pane" id="pages" role="tabpanel" aria-labelledby="pages-tab">
				  		<form method="POST" action="{{route('settings.storePage')}}" enctype="multipart/form-data">
				  			{{csrf_field()}}
				  			<div class="form-group row validate {{$errors->has('about') ? 'is-invalid' : ''}}">
								<label class="col-xl-3 col-lg-3 col-form-label">About</label>
								<div class="col-lg-9 col-xl-6">
									<textarea rows="6" cols="12" name="about" class="form-control {{$errors->has('about') ? 'is-invalid' : ''}}">{{optional($setting)->about}}</textarea>

									@error('about')
										<div id="tax-error" class="error invalid-feedback">{{$message}}</div>
									@enderror
								</div>
							</div>

							<div class="form-group row validate {{$errors->has('terms') ? 'is-invalid' : ''}}">
								<label class="col-xl-3 col-lg-3 col-form-label">Terms & Conditions</label>
								<div class="col-lg-9 col-xl-6">
									<textarea rows="6" cols="12" name="terms" class="form-control {{$errors->has('terms') ? 'is-invalid' : ''}}">{{optional($setting)->terms}}</textarea>

									@error('terms')
										<div id="tax-error" class="error invalid-feedback">{{$message}}</div>
									@enderror
								</div>
							</div>
							<button class="btn btn-primary" type="submit">Save</button>
				  		</form>
				  	</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@push('scripts')
	<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

	<script src="{{asset('assets/js/pages/components/extended/sticky-panels.js')}}" type="text/javascript"></script>
	<script src="{{asset('js/dashboard/settings.js')}}"></script>

	<script>


    $(document).ready(function(){
        var $wrapper = $('.js-wrapper');
        var repLogApp = new settings($wrapper);
    });

	</script>
@endpush
