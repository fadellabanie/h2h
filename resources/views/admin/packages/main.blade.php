<div class="row">
    <div class="col-xl-12">

        <!--begin:: Widgets/Applications/User/Profile3-->
        <div class="kt-portlet kt-portlet--height-fluid">
            <div class="kt-portlet__body">
                <div class="kt-widget kt-widget--user-profile-3">
                    <div class="kt-widget__top">
                        <div class="kt-widget__content">
                            <div class="kt-widget__head">
                                <a href="{{route('packages.show',$package->id)}}" class="kt-widget__username">
                                    {{$package->name}}
                                </a>
                                <div class="kt-widget__action">

                                    @if($package->isActive == 1)
                                        <a href="{{route('packages.toggle',$package->id)}}" class="btn btn-label-danger btn-sm btn-upper">
                                            Deactive
                                        </a>
                                    @else
                                        <a class="btn btn-label-success btn-sm btn-upper" href="{{route('packages.toggle',$package->id)}}">Activate</a>

                                    @endif


                                </div>
                            </div>

                            <div class="kt-widget__info">
                                <div class="kt-widget__desc">
                                    {{$package->description}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-widget__bottom">
                        <div class="kt-widget__item">
                            <div class="kt-widget__icon">
                                <i class="flaticon-pie-chart"></i>
                            </div>
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Cost</span>
                                <span class="kt-widget__value">
                                </span>{{$package->cost}}</span>
                            </div>
                        </div>
                        <div class="kt-widget__item">
                            <div class="kt-widget__icon">
                                @if($package->isActive == 1)
                                    <span class="kt-badge kt-badge--unified-success kt-badge--lg kt-badge--bold">
                                        <i class="flaticon2-check-mark"></i>
                                    </span>
                                @else
                                    <span class="kt-badge kt-badge--unified-danger kt-badge--lg kt-badge--bold">
                                        <i class="flaticon2-cancel-music"></i>
                                    </span>
                                @endif
                            </div>
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Status</span>
                                <span class="kt-widget__value">
                                    @if($package->isActive == 1)
                                        Active
                                    @else
                                        Not Active
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div class="kt-widget__item">
                            <div class="kt-widget__icon">
                                <i class="flaticon-network"></i>
                            </div>
                            <div class="kt-widget__details">
                                <span class="kt-widget__title">Customers</span>
                                <a href="{{route('packages.customers',$package->id)}}" class="kt-widget__value kt-font-brand">View</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
