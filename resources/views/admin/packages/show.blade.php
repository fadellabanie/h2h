@extends('admin.layout.admin')

@push('styles')

<link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">

@endpush

@section('title','package')


@section('content')

<br>

@include('admin.packages.main')

<div class="row js-wrapper">
	<div class="col-xl-12">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">Package Information
						<small>update Package informaiton</small>
					</h3>
				</div>
			</div>
			<div class="kt-portlet__body">
				<form action="{{route('packages.update',$package->id)}}" method="post">
					{{csrf_field()}}
					{{ method_field('PATCH') }}
					<div class="kt-form kt-form--label-right">
						<div class="kt-form__body">
							<div class="kt-section kt-section--first">
								<div class="kt-section__body">
									<div class="form-group row validate {{$errors->has('name') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">*Name</label>
										<div class="col-lg-9 col-xl-6">
											  <input name="name" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" type="text" value="{{$package->name}}">
										</div>
									</div>

									<div class="form-group row validate {{$errors->has('name') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">Description</label>
										<div class="col-lg-9 col-xl-6">
											<textarea name="description" class="form-control {{$errors->has('description') ? 'is-invalid' : ''}}">{{$package->description}}</textarea>
										</div>
									</div>

									<div class="form-group row validate {{$errors->has('cost') ? 'is-invalid' : ''}}">
										<label class="col-xl-3 col-lg-3 col-form-label">*Cost</label>
										<div class="col-lg-9 col-xl-6">
											  <input name="cost" class="form-control {{$errors->has('cost') ? 'is-invalid' : ''}}" type="text" value="{{$package->cost}}">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<div class="row">
									<div class="col-lg-3 col-xl-3"></div>
									<div class="col-lg-9 col-xl-9">
										@can('package_edit')
										<button id="" type="submit" class="btn btn-primary">Submit</button>
										@endcan()
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
