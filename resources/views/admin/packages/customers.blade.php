@extends('admin.layout.admin')

@push('styles')

<link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">

@endpush

@section('title','packages')


@section('content')

<br>

@include('admin.packages.main')

<div class="row js-wrapper">
	<div class="col-xl-12">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h3 class="kt-portlet__head-title">Customers
						<small>Customers whose subscripe this package</small>
					</h3>
				</div>
			</div>

			<div class="kt-portlet__body">
				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer js-promoCustomer-table">
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="js-packageCustomer-table" role="grid" aria-describedby="kt_table_1_info" style="width: 1221px;">
								<thead>
									<tr role="row">
										<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="gift: activate to sort column ascending">Name</th>

										<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="point: activate to sort column ascending">Email</th>

										<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="point: activate to sort column ascending">Phone</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('scripts')

<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script>


    $(document).ready(function(){

    	var id = {!! json_encode($package->id, JSON_HEX_TAG) !!};

        $('#js-packageCustomer-table').DataTable({
        	responsive: true,
			processing: true,
			serverSide: true,
			ajax: '/dashboard/packages/customers/'+id,
			columns: [
				{data: 'name'},
				{data: 'email'},
				{data: 'phone'},
			],
        });
    });


</script>

@endpush
