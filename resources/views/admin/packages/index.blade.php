@extends('admin.layout.admin')

@push('styles')

<link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">

@endpush

@section('title','packages')


@section('content')

<br>

<div class="kt-portlet kt-portlet--mobile js-wrapper">

	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
				packages table
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">

					&nbsp;
					@can('package_create')
					<a href="#" data-toggle="modal" id="newPackage" class="btn btn-brand btn-elevate btn-icon-sm">
						<i class="la la-plus"></i>
						Add New Package
					</a>
					@endcan
				</div>
			</div>
		</div>
	</div>

	<div class="kt-portlet__body">
		<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer js-city-table">
			<div class="row">
				<div class="col-sm-12">
					<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="packages_table" role="grid" aria-describedby="kt_table_1_info" style="width: 1221px;">
						<thead>
							<tr role="row">
								<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">id</th>
								<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">name</th>		

								<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">price</th>

								<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">status</th>
								
								<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 97.5px;" aria-label="Actions">Actions</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade show" id="add_package_modal"  role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; display: none;" aria-modal="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">New City</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				
			</div>
		</div>
	</div>	
	<div class="modal fade show" id="newPackageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true" style="display: hidden; padding-right: 17px;">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">New Package</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<form class="kt-form kt-form js-new-package-form" enctype="multipart/form-data" data-url="{{route('packages.store')}}">
						<div class="kt-portlet__body">
							<div class="kt-section">
								<div class="kt-section__title">
									Baisc Info
								</div>
								<div class="kt-section__content">
									<div class="row">
										<div class="col-sm-3">
											<div class="form-group validate {{$errors->has('name') ? 'is-invalid' : ''}}">
												<label class="form-control-label">*Name:</label>
												<input type="text" name="name" class="form-control">
												@error('name')
											    	<div id="name-error" class="error invalid-feedback">{{$message}}</div>
												@enderror
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group validate {{$errors->has('price') ? 'is-invalid' : ''}}">
												<label class="form-control-label">*Price:</label>
												<input type="text" name="price" class="form-control">
												@error('price')
											    	<div id="price-error" class="error invalid-feedback">{{$message}}</div>
												@enderror
											</div>
										</div>										
									
									</div>
								</div>
							</div>
						
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							<button type="reset" class="btn btn-secondary">reset</button>
							<button type="submit" id="new" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>


</div>


@endsection
@push('scripts')


<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
<script src="{{asset('js/dashboard/packages.js')}}"></script>
<script>
	var can = {
		view: false,
		delete:false
	}

	@can('package_view')
		can.view = true
	@endcan	

	@can('package_delete')
		can.delete = true
	@endcan
</script>
<script>


    $(document).ready(function(){
            var $wrapper = $('.js-wrapper');
            var repLogApp = new packages($wrapper);
    });
</script>
@endpush
