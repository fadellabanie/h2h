@extends('admin.layout.admin')

@push('styles')

<link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">

@endpush

@section('title','services')


@section('content')

<br>
<div class="js-wrapper">
	<div class="row">
		<div class="col-xl-12">
			<!--begin:: Widgets/Applications/User/Profile3-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__body">
					<div class="kt-widget kt-widget--user-profile-3">
						<div class="kt-widget__top">
							<div class="kt-widget__media kt-hidden-">
								@svg(str_replace(' ','',$service->name))
							</div>
							<div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">

							</div>
							<div class="kt-widget__content">
								<div class="kt-widget__head">
									<a href="#" class="kt-widget__username">
										{{$service->name}}
									</a>
									<div class="kt-widget__action">
										<button id="toggleActivation" data-id="{{$service->id}}" type="button" class="btn {{$service->isActive  ==  1 ? 'btn-danger' : 'btn-success'}}">

											{{$service->isActive  ==  1 ? 'deactivate' : 'activate'}}
										</button>
										&nbsp;

									</div>
								</div>

								<div class="kt-widget__info">
									<div class="kt-widget__desc">
										{{$service->description}}
									</div>
{{--									<div class="kt-widget__progress">--}}
{{--										<button type="button" id="DeliveryTime" data-id="{{$service->id}}" data-time="{{$service->delivery_time}}" data-urgent="{{$service->delivery_urgent_time}}" class="btn btn-brand">Edit Delivery Time--}}
{{--										</button>--}}
{{--									</div>--}}
								</div>
							</div>
						</div>
						<div class="kt-widget__bottom">

{{--							<div class="kt-widget__item">--}}
{{--								<div class="kt-widget__icon">--}}
{{--									<i class="flaticon-truck"></i>--}}
{{--								</div>--}}
{{--								<div class="kt-widget__details">--}}
{{--									<span class="kt-widget__title">Delivery Time By Minutes</span>--}}
{{--									<span class="kt-widget__value">{{$service->delivery_time}}</span>--}}
{{--								</div>--}}
{{--							</div>--}}
{{--							<div class="kt-widget__item">--}}
{{--								<div class="kt-widget__icon">--}}
{{--									<i class="flaticon2-delivery-truck"></i>--}}
{{--								</div>--}}
{{--								<div class="kt-widget__details">--}}
{{--									<span class="kt-widget__title">Urgent Delivery Time By Minutes</span>--}}
{{--									<span class="kt-widget__value">--}}
{{--										{{$service->delivery_urgent_time}}--}}
{{--									</span>--}}
{{--								</div>--}}
{{--							</div>--}}

                            <div class="kt-widget__item">
                                <div class="kt-widget__icon">
                                    <i class="flaticon-network"></i>
                                </div>
                                <div class="kt-widget__details">
                                    <span class="kt-widget__title">Customers Count</span>
                                    <span>{{$customers}}</span>
                                </div>
                            </div>


{{--							<div class="kt-widget__item">--}}
{{--								<div class="kt-widget__icon">--}}
{{--									<i class="flaticon-pie-chart"></i>--}}
{{--								</div>--}}
{{--								<div class="kt-widget__details">--}}
{{--									<span class="kt-widget__title">Net</span>--}}
{{--									<span class="kt-widget__value"><span>$</span>164,700</span>--}}
{{--								</div>--}}
{{--							</div>--}}

{{--							<div class="kt-widget__item">--}}
{{--								<div class="kt-widget__icon">--}}
{{--									<i class="flaticon-chat-1"></i>--}}
{{--								</div>--}}
{{--								<div class="kt-widget__details">--}}
{{--									<span class="kt-widget__title">648 Comments</span>--}}
{{--									<a href="#" class="kt-widget__value kt-font-brand">View</a>--}}
{{--								</div>--}}
{{--							</div>--}}

						</div>
					</div>
				</div>
			</div>
			<!--end:: Widgets/Applications/User/Profile3-->
		</div>
	</div>




	<div class="row">
		<div class="kt-portlet kt-portlet--mobile js-wrapper">

			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon2-line-chart"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Types
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							&nbsp;
							<a href="#" id="addNewTypeBtn"  class="btn btn-brand btn-elevate btn-icon-sm">
								<i class="la la-plus"></i>
								New Type
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="kt-portlet__body">
				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer js-region-table">
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="serviceTypesTable" role="grid" aria-describedby="kt_table_1_info" style="width: 1221px;">
								<thead>
									<tr role="row">
										<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">id</th>
										<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">name</th>

										<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">service</th>

										<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">category</th>

										<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">price</th>

										<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 97.5px;" aria-label="Actions">Actions</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


{{--
	<div class="row">
		<div class="kt-portlet kt-portlet--mobile">

			<div class="kt-portlet__head kt-portlet__head--lg">
				<div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon2-line-chart"></i>
					</span>
					<h3 class="kt-portlet__head-title">
						Facilities
					</h3>
				</div>
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<div class="kt-portlet__head-actions">
							&nbsp;
							<a href="#" id="addNewFacilityBtn"  class="btn btn-brand btn-elevate btn-icon-sm">
								<i class="la la-plus"></i>
								New Facility
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="kt-portlet__body">
				<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer js-region-table">
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="serviceFacilities" role="grid" aria-describedby="kt_table_1_info" style="width: 1221px;">
								<thead>
									<tr role="row">
										<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">id</th>
										<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">name</th>

										<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">amount</th>

										<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">type</th>

										<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 97.5px;" aria-label="Actions">Actions</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
--}}


	<!-- start add Type Modal -->

	<div class="modal fade show" id="add_type_modal"  role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; display: none;" aria-modal="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">New Type</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<form enctype="multipart/form-data" class="kt-form kt-form--label-right js-service-type-form" method="POST" data-url="{{route('serviceTypes.store',request('id'))}}"  novalidate="novalidate">
						<div class="kt-portlet__body">

							@if(request('id') == 4)
								<div class="form-group row validate">
									<label class="col-form-label col-lg-3 col-sm-12">Serivce</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<select name="service_id" id="child_service_id" class="form-control select2_service_childs" style="width: 100%">
										</select>
									</div>
								</div>
							@endif

							<div class="form-group row validate">
								<label class="col-form-label col-lg-3 col-sm-12">Type</label>
								<div class="col-lg-9 col-md-9 col-sm-12">
									<select name="type_id" id="typeSelect" class="form-control select2_type" style="width: 100%">
									</select>
								</div>
							</div>

                            <div class="form-group row validate" style="display: none" id="cat_div">
								<label class="col-form-label col-lg-3 col-sm-12">Categories</label>
								<div class="col-lg-9 col-md-9 col-sm-12">
									<select name="category_id[]" id="catsSelect" class="form-control select2_cat" style="width: 100%" multiple>
									</select>
								</div>
							</div>

							<div class="form-group row validate">
								<label class="col-form-label col-lg-3 col-sm-12">Price *</label>
								<div class="col-lg-9 col-md-9 col-sm-12">
									<input type="text"  class="form-control" name="price" max="999999">
								</div>
							</div>
						</div>

						<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<div class="row">
									<div class="col-lg-9 ml-lg-auto">
										<button type="submit" id="NewType" class="btn btn-brand">save</button>
										<button type="reset" class="btn btn-secondary">reset</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- end add Type Modal -->

	<!-- start add Facility Modal -->
{{--
	<div class="modal fade show" id="add_facility_modal"  role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; display: none;" aria-modal="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Facility</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<form enctype="multipart/form-data" class="kt-form kt-form--label-right js-service-facility-form" data-url="{{route('serviceFacilities.store',request('id'))}}"  novalidate="novalidate">
						<div class="kt-portlet__body">
							<div class="form-group row validate">
								<label class="col-form-label col-lg-3 col-sm-12">Facility</label>
								<div class="col-lg-9 col-md-9 col-sm-12">
									<select name="facility_id" id="facilitySelect" class="form-control select2_facility">
									</select>
								</div>
							</div>
							<div class="form-group row validate">
								<label class="col-form-label col-lg-3 col-sm-12">Amount *</label>
								<div class="col-lg-9 col-md-9 col-sm-12">
									<input type="text"  class="form-control" name="amount">
								</div>
							</div>
							<div class="form-group row validate">
								<label class="col-3 col-form-label">Is Percentage</label>
								<div class="col-3">
									<span class="kt-switch kt-switch--icon">
										<label>
											<input type="checkbox" name="isPercentage" value="0" id="isPercentage">
											<span></span>
										</label>
									</span>
								</div>
							</div>
						</div>
						<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<div class="row">
									<div class="col-lg-9 ml-lg-auto">
										<button type="submit" id="NewFacility" class="btn btn-brand">save</button>
										<button type="reset" class="btn btn-secondary">reset</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
--}}
	<!-- end add Facility Modal -->

	<!-- start delivery Time modal -->
	<div class="modal fade show" id="deliveryTime_modal"  role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; display: none;" aria-modal="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Service Delivery Times</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<form enctype="multipart/form-data" class="kt-form kt-form--label-right js-service-Deliverytimes-form" method="POST" action="{{route('services.update',request('id'))}}"  novalidate="novalidate">
						{{csrf_field()}}
						{{method_field('patch')}}
						<div class="kt-portlet__body">

							<div class="form-group row validate">
								<label class="col-form-label col-lg-3 col-sm-12">Delivery Time</label>
								<div class="col-lg-9 col-md-9 col-sm-12">
									<input type="text"  class="form-control delivery_time_picker" name="delivery_time">
								</div>
							</div>

							<div class="form-group row validate">
								<label class="col-form-label col-lg-3 col-sm-12">Urgent Delivery Time</label>
								<div class="col-lg-9 col-md-9 col-sm-12">
									<input type="text"  class="form-control urgent_time_picker" name="urgent_time">
								</div>
							</div>
						</div>
						<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<div class="row">
									<div class="col-lg-9 ml-lg-auto">
										<button type="submit" id="" class="btn btn-brand">save</button>
										<button type="reset" class="btn btn-secondary">reset</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- end delivery modal -->
	@if(request('id') == 4)
		@include('admin.services.childs')
	@endif

</div>
@endsection
@push('scripts')


<script src="{{asset('assets/js/pages/dashboard.js')}}"></script>
<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
<script src="{{asset('js/dashboard/bootstrap-timepicker.js')}}" type="text/javascript"></script>

<script src="{{asset('js/dashboard/services.js')}}"></script>
<script src="{{asset('js/dashboard/serviceFaciliteis.js')}}"></script>

<script>



	$(document).ready(function(){

	    $('.select2_cat').select2();
		var $wrapper = $('.js-wrapper');
		var serviceId = {!! json_encode(request('id'), JSON_HEX_TAG) !!};
		var repLogApp = new service($wrapper,serviceId);
		var serviceFaciliteis = new serviceFacilities($wrapper,serviceId);
	});
</script>
@endpush
