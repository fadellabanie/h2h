@extends('admin.layout.admin')

@push('styles')

<link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">

@endpush

@section('title','services')


@section('content')

<br>




	<div class="row js-wrapper">
		<div class="col-xl-12">
			<div class="kt-portlet">

				<div class="kt-portlet__body">
					<form method="POST" action="{{route('services.updateType', $serviceid)}}" class="kt-form kt-form--label-right js-service-type-form" novalidate="novalidate">

						@csrf

						<div class="kt-portlet__body">

							<h5><bold>Item</bold>{{$servicename}}</h5>
							
						</div>

						<input type="hidden" name="type_id" value="{{$serviceType->id}}">

						@foreach ($selectedCategoriesId as $key => $id)
							
							<input type="hidden" name="category_id[{{$key}}]" value="{{$id}}">
						@endforeach
						
						
	

							<div class="form-group row validate">
								<label class="col-form-label col-lg-1 col-sm-12">Price *</label>
								<div class="col-lg-11 col-md-11 col-sm-12">
									<input type="text"  class="form-control" name="price" value="{{ $price }}">
								</div>
							</div>
						

						<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<div class="row">
									<div class="col-lg-9 ml-lg-auto">
										<button type="submit" id="NewType" class="btn btn-brand">update</button>
										<a href="{{route('services.show', $serviceid)}}" class="btn btn-secondary">back</a>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>


@endsection
@push('scripts')


<script src="{{asset('assets/js/pages/dashboard.js')}}"></script>

<script>



	$(document).ready(function(){

		// $('.select2_cat').select2();
		// $('.select2_type').select2();

		// var selectedCatIds = @json($selectedCategoriesId);

	

		// $('.select2_cat').val(selectedCatIds);
		// $('.select2_cat').trigger('change');
		

		var $wrapper = $('.js-wrapper');

	});
</script>
@endpush
