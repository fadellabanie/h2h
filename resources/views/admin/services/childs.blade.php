<div class="row">
	
	<div class="kt-portlet kt-portlet--mobile js-wrapper">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="kt-font-brand flaticon2-line-chart"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					Childs
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions">
						&nbsp;
						<a href="#" id="addNewChildBtn"  class="btn btn-brand btn-elevate btn-icon-sm">
							<i class="la la-plus"></i>
							New Child
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="kt-portlet__body">
			<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer js-region-table">
				<div class="row">
					<div class="col-sm-12">
						<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="serviceChildsTable" role="grid" aria-describedby="kt_table_1_info" style="width: 1221px;">
							<thead>
								<tr role="row">
									<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">id</th>

									<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">name</th>

									<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 97.5px;" aria-label="Actions">Actions</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<!-- start add Type Modal -->

<div class="modal fade show" id="child_modal"  role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; display: none;" aria-modal="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">New Child</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form enctype="multipart/form-data" class="kt-form kt-form--label-right js-service-child-form" method="POST" data-url="{{route('serviceChild.store',request('id'))}}"  novalidate="novalidate">
					<div class="kt-portlet__body">
						<div class="form-group row validate">
							<label class="col-form-label col-lg-3 col-sm-12">Name *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" id="childName" class="form-control" name="name">
							</div>
						</div>
					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<div class="row">
								<div class="col-lg-9 ml-lg-auto">
									<button type="submit" id="NewServiceChild" class="btn btn-brand">save</button>
									<button type="reset" class="btn btn-secondary">reset</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- end add Type Modal -->
