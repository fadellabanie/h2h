@extends('admin.layout.admin')

@push('styles')


@endpush

@section('title','services')


@section('content')

<br>
<div class="kt-portlet">
	<div class="kt-portlet__body  kt-portlet__body--fit">
		<div class="row row-no-padding row-col-separator-lg">
			@foreach($services as $service)
				<div class="col-md-12 col-lg-6 col-xl-3">
						<!--begin::Total Profit-->
							<div class="kt-widget24">
								<div class="kt-widget24__details">
									<div class="kt-widget24__info">
										@can('service_view')
										<a href="{{route('services.show',$service->id)}}" class="kt-widget24__title">
											{{$service->name}}
										</a>
										@else
										<a href="#" class="kt-widget24__title">
											{{$service->name}}
										</a>
										@endcan
										<span class="kt-widget24__desc">
											{{$service->description}}
										</span>
									</div>
									<span class="kt-widget24__stats kt-font-brand">
										@svg(str_replace(' ','',$service->name))
									</span>
								</div>
								<div class="progress progress--sm">
									<div class="progress-bar kt-bg-brand" role="progressbar" style="width: {{$service->percentge}}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
								<div class="kt-widget24__action">
									<span class="kt-widget24__change">
										Items
									</span>
									<span class="kt-widget24__number">
										{{$service->percentge}}%
									</span>
								</div>
							</div>
						<!--end::Total Profit-->
				</div>
			@endforeach
		</div>
	</div>
</div>



@endsection
@push('scripts')
@endpush
