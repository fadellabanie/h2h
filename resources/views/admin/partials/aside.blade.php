<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
    <!-- begin:: Aside Menu -->
    <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
        <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
            <ul class="kt-menu__nav ">
                <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('dashboard.index')}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-protection"></i><span class="kt-menu__link-text">Dashboard</span></a></li>

                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon-apps"></i><span class="kt-menu__link-text">Manage</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            
                            @can('user_access')

                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('managers.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">managers</span></a></li>

                            @endcan

                            @can('driver_access')
                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('drivers.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">drivers</span></a></li>
                            @endcan

                            @can('facility_access')
                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('facilities.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">facilities</span></a></li>
                            @endcan

                            @can('category_access')
                            <li class="kt-menu__item " aria-haspopup="true">
                                <a href="{{route('categories.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">categories</span></a></li>
                            @endcan

                            @can('type_access')
                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('types.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">types</span></a></li>
                            @endcan

                            @can('package_access')
                            <li class="kt-menu__item " aria-haspopup="true">
                                <a href="{{route('packages.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">packages</span></a>
                            </li>
                            @endcan

                            @can('city_access')
                            <li class="kt-menu__item " aria-haspopup="true">
                                <a href="{{route('cities.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">cities</span></a>
                            </li>
                            @endcan
                            @can('region_access')
                            <li class="kt-menu__item " aria-haspopup="true">
                                <a href="{{route('regions.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">regions</span></a>
                            </li>
                            @endcan

                        </ul>
                    </div>
                </li>

                @can('customer_access')
                <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('customers.index')}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-group"></i><span class="kt-menu__link-text">Customers</span></a></li>
                @endcan

                @can('service_access')
                <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('services.index')}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-menu-3"></i><span class="kt-menu__link-text">Services</span></a></li>
                @endcan

                @can('gift_access')
                <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('gifts.index')}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-gift"></i><span class="kt-menu__link-text">Gifts</span></a></li>
                @endcan

                @can('point_access')
                <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('points.index')}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-coins"></i><span class="kt-menu__link-text">Points</span></a></li>
                @endcan

                @can('order_access')
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon2-medical-records-1"></i><span class="kt-menu__link-text">Orders</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">

                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('orders.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">All</span></a></li>

{{--
                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('orders.calendar')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Calendar</span></a></li>
--}}
                        </ul>
                    </div>
                </li>
                @endcan

                @can('promocode_access')
                <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('promoCodes.index')}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-more-v4"></i><span class="kt-menu__link-text">promo codes</span></a></li>
                @endcan

                @can('invoice_access')
                <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('invoices.index')}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-file-1"></i><span class="kt-menu__link-text">Invoices</span></a></li>
                @endcan

                @can('report_access')
                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon-graphic"></i><span class="kt-menu__link-text">Reports</span><span class="kt-menu__link-badge"></span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('reports.services.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Services</span></a></li>
                        </ul>
                    </div>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('reports.facilities.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Facilities</span></a></li>
                        </ul>
                    </div>
                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('reports.drivers.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Drivers</span></a></li>
                        </ul>
                    </div>
                </li>
                @endcan

                @can('support_access')
                <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('support.index')}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-chat-2"></i><span class="kt-menu__link-text">Support</span></a></li>
                @endcan

                @can('setting_access')
                <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('settings.index')}}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-settings"></i><span class="kt-menu__link-text">Settings</span></a></li>
                @endcan


            </ul>
        </div>
    </div>
    <!-- end:: Aside Menu -->
</div>
