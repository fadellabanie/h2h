@extends('admin.layout.admin')

@push('styles')

    <link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">

@endpush

@section('title','dashboard')


@section('content')

    <div class="kt-portlet">
        <div class="kt-portlet__body  kt-portlet__body--fit">
            <div class="row row-no-padding row-col-separator-lg">
                <div class="col-md-12 col-lg-6 col-xl-3">
                    <!--begin::otal Profit-->
                    <div class="kt-widget24">
                        <div class="kt-widget24__details">
                            <div class="kt-widget24__info">
                                <h4 class="kt-widget24__title">
                                    Customers
                                </h4>
                                <span class="kt-widget24__desc">
									All Customers
                                </span>
                            </div>
                            <span class="kt-widget24__stats kt-font-brand">
                               {{$customers}}
                            </span>
                        </div>
                    </div>
                    <!--end::Total Profit-->
                </div>
                <div class="col-md-12 col-lg-6 col-xl-3">
                    <!--begin::New Feedbacks-->
                    <div class="kt-widget24">
                        <div class="kt-widget24__details">
                            <div class="kt-widget24__info">
                                <h4 class="kt-widget24__title">
                                    Drivers
                                </h4>
                                <span class="kt-widget24__desc">
                                    All Drivers
                                </span>
                            </div>
                            <span class="kt-widget24__stats kt-font-warning">
                                {{$drivers}}
                            </span>
                        </div>
                    </div>
                    <!--end::New Feedbacks-->
                </div>
                <div class="col-md-12 col-lg-6 col-xl-3">
                    <!--begin::New Orders-->
                    <div class="kt-widget24">
                        <div class="kt-widget24__details">
                            <div class="kt-widget24__info">
                                <h4 class="kt-widget24__title">
                                    Orders
                                </h4>
                                <span class="kt-widget24__desc">
                                    All Completed Orders
                                </span>
                            </div>
                            <span class="kt-widget24__stats kt-font-danger">
                                {{$orders}}
                            </span>
                        </div>
                    </div>
                    <!--end::New Orders-->
                </div>
                <div class="col-md-12 col-lg-6 col-xl-3">
                    <!--begin::New Users-->
                    <div class="kt-widget24">
                        <div class="kt-widget24__details">
                            <div class="kt-widget24__info">
                                <h4 class="kt-widget24__title">
                                    Income
                                </h4>
                                <span class="kt-widget24__desc">
                                    Total Income
                                </span>
                            </div>
                            <span class="kt-widget24__stats kt-font-success">
                                {{$income}}
                            </span>
                        </div>
                    </div>

                    <!--end::New Users-->
                </div>
            </div>
        </div>
    </div>

    <br>
    <div class="row">
        <h2>Monthly Order Statistic</h2>
        {!! $orderChart->container() !!}
    </div>
    <br>
    <hr>
    <div class="row">
        <h2>Monthly Income Statistic</h2>
        {!! $incomeChart->container() !!}
    </div>
    <br>
    <hr>
    <div class="row">
        <h2>Daily Customers Statistic</h2>
        {!! $customerChart->container() !!}
    </div>
@endsection
@push('scripts')
    <script src="{{asset('assets/js/pages/dashboard.js')}}"></script>
    {!! $orderChart->script() !!}
    {!! $incomeChart->script() !!}
    {!! $customerChart->script() !!}

@endpush
