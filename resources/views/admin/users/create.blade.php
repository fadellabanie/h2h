@extends('admin.layout.admin')

@push('styles')

<link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">
@endpush

@section('title','users')


@section('content')

<div class="kt-portlet kt-portlet--mobile">


	<div class="kt-portlet__head kt-portlet__head--lg">

		<div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
				Users
			</h3>
		</div>
	</div>

	<div class="kt-portlet__body">
		<div class="kt-grid__item kt-grid__item--fluid kt-app__content">
		<div class="row">
			<div class="col-xl-12">
				<div class="kt-portlet">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">Create New User</small>
							</h3>
						</div>
					</div>
					@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
					<form class="kt-form kt-form--label-right js-update-driver-form" method="post" action="{{route('managers.store')}}">
						{{csrf_field()}}
						<div class="kt-portlet__body">
							<div class="kt-section kt-section--first">
								<div class="kt-section__body">
									<div class="row">
										<label class="col-xl-3"></label>
										<div class="col-lg-9 col-xl-6">
											<h3 class="kt-section__title kt-section__title-sm">Baisc Info:</h3>
										</div>
									</div>
									<div class="form-group row validate ">
										<label class="col-xl-3 col-lg-3 col-form-label">*Name</label>
										<div class="col-lg-9 col-xl-6">
											<input type="text" class="form-control " name="name">
										</div>
									</div>					
									<div class="form-group row validate ">
										<label class="col-xl-3 col-lg-3 col-form-label">*Email</label>
										<div class="col-lg-9 col-xl-6">
											<input class="form-control " type="text" name="email">
										</div>
									</div>								
									<div class="form-group row validate ">
										<label class="col-xl-3 col-lg-3 col-form-label">Address</label>
										<div class="col-lg-9 col-xl-6">
											<input class="form-control" type="text" name="address">
										</div>
									</div>
									<div class="form-group row validate ">
										<label class="col-xl-3 col-lg-3 col-form-label">Phone</label>
										<div class="col-lg-9 col-xl-6">
											<input class="form-control" type="text" name="phone">
										</div>
									</div>	
									<div class="kt-separator kt-separator--border-dashed kt-separator--portlet-fit kt-separator--space-lg"></div>
									<div class="row">
										<label class="col-xl-3"></label>
											<div class="col-lg-9 col-xl-6">
												<h3 class="kt-section__title kt-section__title-sm">Security:</h3>
											</div>
									</div>
									<div class="form-group row validate ">
										<label class="col-xl-3 col-lg-3 col-form-label">password</label>
										<div class="col-lg-9 col-xl-6">
											<input class="form-control" type="password" name="password">
										</div>
									</div>								
									<div class="form-group row validate ">
										<label class="col-xl-3 col-lg-3 col-form-label">password confirmation</label>
										<div class="col-lg-9 col-xl-6">
											<input class="form-control" type="password" name="password_confirmation">
										</div>
									</div>


									<div class="kt-separator kt-separator--border-dashed kt-separator--portlet-fit kt-separator--space-lg"></div>
									<div class="row">

										<label class="col-xl-3"></label>
										<div class="col-lg-9 col-xl-6">
											<h3 class="kt-section__title kt-section__title-sm">Permissions:</h3>
											<div class="kt-checkbox-inline">
												<label class="kt-checkbox kt-checkbox--success">
													<input 
														id="selectAll" 
														type="checkbox">Select all
													<span></span>
												</label>
											</div>
											<div class="form-group row validate ">
												<div class="kt-checkbox-inline">
													@foreach($permissions->sortBy('name') as $permission)
														<label class="kt-checkbox kt-checkbox--success" style="min-width: 180px;">
															<input value="{{$permission->name}}" id="'permission-'.{{$permission->id}} " name="permissions[]" type="checkbox">{{ ucwords(str_replace('_', ' ', $permission->name)) }}
															<span></span>
														</label>
													@endforeach
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="kt-portlet__foot">
							<div class="kt-form__actions">
								<div class="row">
									<div class="col-lg-3 col-xl-3">
									</div>
									<div class="col-lg-9 col-xl-9">
										<button id="updateDriver" type="submit" class="btn btn-primary">Submit</button>&nbsp;
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</div>

</div>

@endsection

@push('scripts')
<script>



	$(document).ready(function(){

		$(':checkbox[id=selectAll]').click (function () {
			$(":checkbox[name='permissions[]']").prop('checked', this.checked);
		});

	});
</script>
@endpush