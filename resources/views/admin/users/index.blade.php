@extends('admin.layout.admin')

@push('styles')

<link rel="stylesheet"  href="{{asset('assets/plugins/custom/datatables/datatables.bundle.css')}}">
@endpush

@section('title','users')


@section('content')



<div class="kt-portlet kt-portlet--mobile">

	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
				admins table
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">

					&nbsp;
					@can('user_create')
					<a href="{{route('users.create')}}" {{-- data-toggle="modal" data-target="#add_manager_modal" --}} class="btn btn-brand btn-elevate btn-icon-sm">
						<i class="la la-plus"></i>
						Add New Manager
					</a>
					@endcan

				</div>
			</div>
		</div>
	</div>

	<div class="kt-portlet__body">
		<div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

			<div class="row">
				<div class="col-sm-12">
					<table class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" id="admins_table_1" role="grid" aria-describedby="kt_table_1_info" style="width: 1221px;">
						<thead>
							<tr role="row">
								<th class="sorting_asc" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 87.25px;" aria-sort="ascending" aria-label="user ID: activate to sort column descending">id</th>
								<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="email: activate to sort column ascending">email</th>
								<th class="sorting" tabindex="0" aria-controls="kt_table_1" rowspan="1" colspan="1" style="width: 83.25px;" aria-label="name: activate to sort column ascending">name</th>
								<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 97.5px;" aria-label="Actions">Actions</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>


		</div>
	</div>

</div>


<div class="modal fade show" id="add_manager_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; display: none;" aria-modal="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">New manager</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form enctype="multipart/form-data" class="kt-form kt-form--label-right" id="kt_form_1" novalidate="novalidate">
					<div class="kt-portlet__body">
						<div class="form-group form-group-last kt-hide">
							<div class="alert alert-danger" role="alert" id="kt_form_1_msg">
								<div class="alert-icon"><i class="flaticon-warning"></i></div>
								<div class="alert-text">
									Oh snap! Change a few things up and try submitting again.
								</div>
								<div class="alert-close">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
									</button>
								</div>
							</div>
						</div>
						<div class="form-group row validate">
							<label class="col-form-label col-lg-3 col-sm-12">Name *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="name" placeholder="Enter your name" aria-describedby="name-error"><div id="name-error" class="error invalid-feedback">This field is required.</div>
							</div>
						</div>						

						<div class="form-group row validate">
							<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="email" placeholder="Enter your email" aria-describedby="email-error"><div id="email-error" class="error invalid-feedback">This field is required.</div>
								<span class="form-text text-muted">We'll never share your email with anyone else.</span>
							</div>
						</div>							
						<div class="form-group row validate">
							<label class="col-form-label col-lg-3 col-sm-12">Password *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="password" class="form-control" name="password" placeholder="Enter your password" aria-describedby="password-error"><div id="password-error" class="error invalid-feedback">This field is required.</div>
								
							</div>
						</div>						
					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<div class="row">
								<div class="col-lg-9 ml-lg-auto">
									<button type="submit" class="btn btn-brand">save</button>
									<button type="reset" class="btn btn-secondary">reset</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade show" id="update_manager_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="padding-right: 17px; display: none;" aria-modal="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">New manager</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				</button>
			</div>
			<div class="modal-body">
				<form enctype="multipart/form-data" class="kt-form kt-form--label-right" id="update_form_1" novalidate="novalidate">
					<div class="kt-portlet__body">
						<div class="form-group form-group-last kt-hide">
							<div class="alert alert-danger" role="alert" id="update_form_1_msg">
								<div class="alert-icon"><i class="flaticon-warning"></i></div>
								<div class="alert-text">
									Oh snap! Change a few things up and try submitting again.
								</div>
								<div class="alert-close">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
									</button>
								</div>
							</div>
						</div>
						<div class="form-group row validate">
							<label class="col-form-label col-lg-3 col-sm-12">Name *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="name" placeholder="Enter your name" aria-describedby="name-error"><div id="name-error" class="error invalid-feedback">This field is required.</div>
							</div>
						</div>						

						<div class="form-group row validate">
							<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="text" class="form-control" name="email" placeholder="Enter your email" aria-describedby="email-error"><div id="email-error" class="error invalid-feedback">This field is required.</div>
								<span class="form-text text-muted">We'll never share your email with anyone else.</span>
							</div>
						</div>							
						<div class="form-group row validate">
							<label class="col-form-label col-lg-3 col-sm-12">Password *</label>
							<div class="col-lg-9 col-md-9 col-sm-12">
								<input type="password" class="form-control" name="password" placeholder="Enter your password" aria-describedby="password-error"><div id="password-error" class="error invalid-feedback">This field is required.</div>
								
							</div>
						</div>						
					</div>
					<div class="kt-portlet__foot">
						<div class="kt-form__actions">
							<div class="row">
								<div class="col-lg-9 ml-lg-auto">
									<button type="submit" class="btn btn-brand">save</button>
									<button type="reset" class="btn btn-secondary">reset</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@push('scripts')


<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script>
	var userObj = {!! json_encode(auth()->user(), JSON_HEX_TAG) !!};

	var can = {
		edit: false,
		delete: false
	};

	@can('user_edit')
		can.edit = true
	@endcan	

	@can('user_delete')
		can.delete = true
	@endcan
	
</script>


<script src="{{asset('js/dashboard/admins.js')}}">
	
</script>
@endpush
