<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domains\Type\entity\Color;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Color::class, function (Faker $faker) {
    return [
        'name' => $faker->safeColorName
    ];
});
