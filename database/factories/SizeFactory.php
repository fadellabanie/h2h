<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domains\Type\entity\Size;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Size::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
