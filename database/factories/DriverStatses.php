<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domains\Driver\entity\DriverStatus;
use App\Model;
use Faker\Generator as Faker;

$factory->define(DriverStatus::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
