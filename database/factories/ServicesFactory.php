<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domains\Service\entity\Service;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Service::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
