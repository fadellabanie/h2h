<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domains\Package\entity\Package;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Package::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'cost' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 10),
    ];
});
