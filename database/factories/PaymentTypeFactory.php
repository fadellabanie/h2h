<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domains\Payment\entity\PaymentType;
use App\Model;
use Faker\Generator as Faker;

$factory->define(PaymentType::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
