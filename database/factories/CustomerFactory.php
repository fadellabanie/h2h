<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domains\Customer\entity\Customer;
use App\Domains\User\entity\User;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {
    return [
        
        'user_id' => function(){
        	return factory(User::class)->create()->id;
        }
    ];
});
