<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domains\Facility\entity\Facility;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Facility::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
