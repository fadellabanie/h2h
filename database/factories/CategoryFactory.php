<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domains\Category\entity\Category;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
