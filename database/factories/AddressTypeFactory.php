<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domains\Customer\entity\AddressType;
use App\Model;
use Faker\Generator as Faker;

$factory->define(AddressType::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
