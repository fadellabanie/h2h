<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domains\Gift\entity\GiftType;
use App\Model;
use Faker\Generator as Faker;

$factory->define(GiftType::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
