<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domains\User\entity\UserGroup;
use Faker\Generator as Faker;

$factory->define(UserGroup::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
