<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domains\Order\entity\OrderStatus;
use App\Model;
use Faker\Generator as Faker;

$factory->define(OrderStatus::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
