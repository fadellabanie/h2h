<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domains\Invoice\entity\InvoiceStatus;
use App\Model;
use Faker\Generator as Faker;

$factory->define(InvoiceStatus::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
