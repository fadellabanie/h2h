<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionRoleTableSeeder extends Seeder
{

	//use DisableForeignKeys;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->disableForeignKeys();

        $admin = Role::create(['name' => 'admin']);
        $manager = Role::create(['name' => 'manager']);


        $permissions = [

        	['id' => 1, 'name' => 'user_access',],
            ['id' => 2, 'name' => 'user_create',],
            ['id' => 3, 'name' => 'user_edit',],
            ['id' => 4, 'name' => 'user_view',],
            ['id' => 5, 'name' => 'user_delete',],        	


            ['id' => 6, 'name' => 'driver_access',],
            ['id' => 7, 'name' => 'driver_create',],
            ['id' => 8, 'name' => 'driver_edit',],
            ['id' => 9, 'name' => 'driver_view',],
            ['id' => 10, 'name' => 'drive_delete',],            



            ['id' => 11, 'name' => 'facility_access',],
            ['id' => 12, 'name' => 'facility_create',],
            ['id' => 13, 'name' => 'facility_edit',],
            ['id' => 14, 'name' => 'facility_view',],
            ['id' => 15, 'name' => 'facility_delete',],            


            ['id' => 21, 'name' => 'category_access',],
            ['id' => 22, 'name' => 'category_create',],
            ['id' => 23, 'name' => 'category_edit',],
            ['id' => 24, 'name' => 'category_view',],
            ['id' => 25, 'name' => 'category_delete',],            


            ['id' => 26, 'name' => 'type_access',],
            ['id' => 27, 'name' => 'type_create',],
            ['id' => 28, 'name' => 'type_edit',],
            ['id' => 29, 'name' => 'type_view',],
            ['id' => 30, 'name' => 'type_delete',],            


            ['id' => 31, 'name' => 'package_access',],
            ['id' => 32, 'name' => 'package_create',],
            ['id' => 33, 'name' => 'package_edit',],
            ['id' => 34, 'name' => 'package_view',],
            ['id' => 35, 'name' => 'package_delete',],            


            ['id' => 36, 'name' => 'city_access',],
            ['id' => 37, 'name' => 'city_create',],
            ['id' => 38, 'name' => 'city_edit',],
            ['id' => 39, 'name' => 'city_view',],
            ['id' => 40, 'name' => 'city_delete',],            


            ['id' => 41, 'name' => 'region_access',],
            ['id' => 42, 'name' => 'region_create',],
            ['id' => 43, 'name' => 'region_edit',],
            ['id' => 44, 'name' => 'region_view',],
            ['id' => 45, 'name' => 'region_delete',],            


            ['id' => 46, 'name' => 'customer_access',],
            ['id' => 47, 'name' => 'customer_create',],
            ['id' => 48, 'name' => 'customer_edit',],
            ['id' => 49, 'name' => 'customer_view',],
            ['id' => 50, 'name' => 'customer_delete',],


            ['id' => 51, 'name' => 'service_access',],
            ['id' => 52, 'name' => 'service_create',],
            ['id' => 53, 'name' => 'service_edit',],
            ['id' => 54, 'name' => 'service_view',],
            ['id' => 55, 'name' => 'service_delete',],            


            ['id' => 56, 'name' => 'gift_access',],
            ['id' => 57, 'name' => 'gift_create',],
            ['id' => 58, 'name' => 'gift_edit',],
            ['id' => 59, 'name' => 'gift_view',],
            ['id' => 60, 'name' => 'gift_delete',],            

            ['id' => 61, 'name' => 'point_access',],
            ['id' => 62, 'name' => 'point_delete',],            


            ['id' => 63, 'name' => 'order_access',],
            ['id' => 64, 'name' => 'order_view',],
            ['id' => 65, 'name' => 'order_delete',],


            ['id' => 66, 'name' => 'promocode_access',],
            ['id' => 67, 'name' => 'promocode_create',],
            ['id' => 68, 'name' => 'promocode_edit',],
            ['id' => 69, 'name' => 'promocode_view',],
            ['id' => 70, 'name' => 'promocode_delete',],            


            ['id' => 71, 'name' => 'invoice_access',],
            ['id' => 72, 'name' => 'invoice_view',],


            ['id' => 73, 'name' => 'report_access',],



            ['id' => 74, 'name' => 'support_access',],
            ['id' => 75, 'name' => 'support_view',],
            ['id' => 76, 'name' => 'support_delete',],


            ['id' => 77, 'name' => 'setting_access',],

        ];


        foreach ($permissions as $item) {
            Permission::create($item);
        }

        $admin->syncPermissions(Permission::all());

    }
}
