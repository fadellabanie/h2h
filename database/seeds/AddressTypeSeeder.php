<?php

use App\Domains\Customer\entity\AddressType;
use Illuminate\Database\Seeder;

class AddressTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(AddressType::class,3)
        ->create()
        ->each(function($type,$k){

        	if ($k == 0) {

        		$type->update(['name' => 'home']);
        	}

        	if ($k == 1) {
        		$type->update(['name' => 'work']);
        	}        	

        	if ($k == 2) {
        		$type->update(['name' => 'other']);
        	}
        });
    }
}
