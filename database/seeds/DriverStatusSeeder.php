<?php

use App\Domains\Driver\entity\DriverStatus;
use Illuminate\Database\Seeder;

class DriverStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(DriverStatus::class,2)
        	->create()
        	->each(function($status,$k){

        		if ($k == 0) {
        			$status->update(['name' => 'busy']);
        		}
        		if ($k == 1) {
        			$status->update(['name' => 'available']);
        		}
        		
        	});
    }
}
