<?php

use Illuminate\Database\Seeder;
use App\Domains\User\entity\User;

class UserRoleTableSeeder extends Seeder
{

	//use DisableForeignKeys;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->disableForeignKeys();
        User::find(1)->assignRole('admin');
        //$this->enableForeignKeys();
    }
}
