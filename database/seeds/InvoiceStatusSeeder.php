<?php

use App\Domains\Invoice\entity\InvoiceStatus;
use Illuminate\Database\Seeder;

class InvoiceStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(InvoiceStatus::class,3)
        	->create()
        	->each(function($invoice,$k){

        		if ($k == 0) {
        			$invoice->update(['name' => 'issued']);
        		}

        		if ($k == 1) {
        			$invoice->update(['name' => 'cancelled']);
        		}             

                if ($k == 2) {
                    $invoice->update(['name' => 'completed']);
                }
        		
        	});
    }
}
