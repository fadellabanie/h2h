<?php

use App\Domains\Service\entity\Service;
use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Service::class,4)
        ->create()
        ->each(function($ser,$k){

        	if ($k == 0) {
        		$ser->update(['name' => 'Wash & Press', 'description' => 'wash clothes']);
        	}

        	if ($k == 1) {
        		$ser->update(['name' => 'Dry Clean' , 'description' => 'Dry Clean']);
        	}

        	if ($k == 2) {
        		$ser->update(['name' => 'Stream Press' , 'description' => 'Stream Press']);
        	}

        	if ($k == 3) {
        		$ser->update(['name' => 'Talioring', 'description' => 'Talioring']);

                //$ser->childs()->saveMany(factory(Service::class,3)->make());
        	}
        	
        });
    }
}
