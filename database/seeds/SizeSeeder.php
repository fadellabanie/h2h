<?php

use App\Domains\Type\entity\Size;
use Illuminate\Database\Seeder;

class SizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Size::class,7)
        	->create()
        	->each(function($size,$k){

        		if ($k == 0) {
        			$size->update(['name' => 'XS']);
        		}

        		if ($k == 1) {
        			$size->update(['name' => 'S']);
        		}        		

        		if ($k == 2) {
        			$size->update(['name' => 'M']);
        		}        		

        		if ($k == 3) {
        			$size->update(['name' => 'L']);
        		}        		

        		if ($k == 4) {
        			$size->update(['name' => 'XL']);
        		}        		

        		if ($k == 5) {
        			$size->update(['name' => '2XL']);
        		}        		

        		if ($k == 6) {
        			$size->update(['name' => '3XL']);
        		}

        	});
    }
}
