<?php

use App\Domains\User\entity\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	
    	factory(User::class)
    		->create()
    		->each(function($user,$k){

    			if ($k == 0) {
    				$user->update(['user_group_id' => 1]);
    			}
    			

    		});
    }
}
