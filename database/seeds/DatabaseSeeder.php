<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserGroupTableSeder::class);


        //$this->call(UsersTableSeeder::class);
        $this->call(PermissionRoleTableSeeder::class);
        $this->call(UserRoleTableSeeder::class);

        $this->call(DriverStatusSeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(PackageSeeder::class);

        //seeders
        $this->call(CategorySeeder::class);
        $this->call(OrderStatusesSeeder::class);
        $this->call(FacilitySeeder::class);
        $this->call(AddressTypeSeeder::class);
        $this->call(CustomerSeeder::class);
        $this->call(ColorSeeder::class);
        $this->call(SizeSeeder::class);
        $this->call(PaymentTypeSeeder::class);
        $this->call(InvoiceStatusSeeder::class);
        //$this->call(GiftTypeSeeder::class);
    }
}
