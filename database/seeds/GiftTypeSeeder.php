<?php

use App\Domains\Gift\entity\GiftType;
use Illuminate\Database\Seeder;

class GiftTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(GiftType::class,2)
        ->create()
        ->each(function($gift,$k){

        	if ($k == 0) {
        		$gift->update(['name' => 'wallet']);
        	}
        	
        	if ($k == 1) {
        		$gift->update(['name' => 'code']);
        	}
        	
        });
    }
}
