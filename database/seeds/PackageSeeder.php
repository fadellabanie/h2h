<?php

use App\Domains\Package\entity\Package;
use Illuminate\Database\Seeder;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Package::class,3)
        ->create()
        ->each(function($package,$k){

        	if ($k == 0) {
        		$package->update(['name' => 'silver']);
        	}

        	if ($k == 1) {
        		$package->update(['name' => 'gold']);
        	}

        	if ($k == 2) {
        		$package->update(['name' => 'platnium']);
        	}
        });
    }
}
