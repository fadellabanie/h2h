<?php

use App\Domains\Facility\entity\Facility;
use App\Domains\Service\entity\Service;
use Illuminate\Database\Seeder;

class FacilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Facility::class,4)
        	->create()
        	->each(function($facility,$k){
                if ($k == 0) {
                    $facility->update([
                        'name' => 'incense',
                    ]);
                }

                if ($k == 1) {
                    $facility->update([
                        'parent_id' => 1,
                        'name' => 'Silver Oud',
                        'isPercentage' => 1,
                        'amount' => 20
                    ]);
                }

                if ($k == 2) {
                    $facility->update([
                        'parent_id' => 1,
                        'name' => 'Gold Oud',
                        'isPercentage' => 1,
                        'amount' => 10
                    ]);
                }

                if ($k == 3) {
                    $facility->update([
                        'parent_id' => 1,
                        'name' => 'Amber',
                        'isPercentage' => 1,
                        'amount' => 15
                    ]);
                }

        	});

//        $service = Service::where('id', '!=', 4)->get();
//
//        $facilities = Facility::whereNotNull('parent_id')->pluck('id');
//
//        $service->each(function($s) use ($facilities){
//
//            foreach ($facilities as $value) {
//
//                if ($value == 2) {
//                    $s->facilities()->attach($value,['amount' => 30,'isPercentage' => 1]);
//                }
//
//                if ($value == 3) {
//                    $s->facilities()->attach($value,['amount' => 50,'isPercentage' => 1]);
//                }
//
//                if ($value == 4) {
//                    $s->facilities()->attach($value,['amount' => 40,'isPercentage' => 1]);
//                }
//            }
//
//        });
    }
}
