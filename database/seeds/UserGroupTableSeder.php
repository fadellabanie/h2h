<?php

use App\Domains\User\entity\User;
use App\Domains\User\entity\UserGroup;
use Illuminate\Database\Seeder;

class UserGroupTableSeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(UserGroup::class,3)
        	->create()
        	->each(function($group,$k){
        		
        		if ($k == 0) {

        			$group->update([
        				'name' => 'managers'
        			]);

        			$user = factory(User::class)->make();
        			$group->users()->save($user);
        		}

        		if ($k == 1) {

        			$group->update([
        				'name' => 'drivers'
        			]);
        		}

        		if ($k == 2) {

        			$group->update([
        				'name' => 'customers'
        			]);
        		}

        		
        	});
    }
}
