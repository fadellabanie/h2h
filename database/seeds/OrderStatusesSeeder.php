<?php

use App\Domains\Order\entity\OrderStatus;
use Illuminate\Database\Seeder;

class OrderStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(OrderStatus::class,9)
        	->create()
        	->each(function($status,$k){

        		if ($k == 0) {
        			$status->update(['name' => 'Basket']);
        		}

        		if ($k == 1) {
        			$status->update(['name' => 'Placed']);
        		}             

                if ($k == 2) {
                    $status->update(['name' => 'Requested']);
                }                

                if ($k == 3) {
                    $status->update(['name' => 'Cancelled']);
                }

                if ($k == 4) {
                    $status->update(['name' => 'Accepted']);
                }

                if ($k == 5) {
                    $status->update(['name' => 'Picked up']);
                }                

                if ($k == 6) {
                    $status->update(['name' => 'Processing']);
                }                

                if ($k == 7) {
                    $status->update(['name' => 'Out Of Delivery']);
                }

                if ($k == 8) {
                    $status->update(['name' => 'Delivered']);
                }                

        	});
    }
}
