<?php

use App\Domains\Payment\entity\PaymentType;
use Illuminate\Database\Seeder;

class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PaymentType::class,3)
        ->create()
        ->each(function($type,$k){

        	if ($k == 0) {

        		$type->update([
        			'name' => 'Cod'
        		]);
        	}        	

        	if ($k == 1) {

        		$type->update([
        			'name' => 'Wallet'
        		]);
        	}        	
        	if ($k == 2) {

        		$type->update([
        			'name' => 'Credit Card'
        		]);
        	}
        });
    }
}
