<?php

use App\Domains\Category\entity\Category;
use App\Domains\Type\entity\Type;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cats = factory(Category::class,4)->create()
        ->each(function($v,$k){

        	if ($k == 0) {
        		$v->update(['name' => 'Men']);
        	}

        	if ($k == 1) {
        		$v->update(['name' => 'Woman']);
        	}          

            if ($k == 2) {
                $v->update(['name' => 'House Items']);
            }

            if ($k == 3) {
                $v->update(['name' => 'Kids']);
            }
        });

        $cats->each(function($cat){
        	$cat->types()->saveMany(factory(Type::class,2)->make());
        });
    }
}
