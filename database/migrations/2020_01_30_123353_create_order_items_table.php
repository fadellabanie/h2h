<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('category_id');
            //$table->unsignedBigInteger('quantity');
            $table->float('amount'); // price for one item
            $table->float('total_amount'); // price qunantities * amount + facilities * qunantities
            $table->unsignedBigInteger('service_id');
            $table->unsignedBigInteger('type_id');
            $table->integer('count');
            //$table->json('facilities');
            $table->unsignedBigInteger('color_id')->nullable();
            $table->unsignedBigInteger('size_id')->nullable();
            //$table->unsignedBigInteger('facility_id')->nullable();
            $table->string('brand')->nullable();
            $table->string('image')->nullable();
            $table->text('instruction')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
