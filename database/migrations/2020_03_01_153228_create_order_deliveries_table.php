<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_deliveries', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->unsignedBigInteger('order_id'); 
            $table->unsignedBigInteger('driver_id');
            //$table->unsignedBigInteger('address_id');

            $table->timestamp('accepted_at');
            $table->timestamp('picked_at')->nullable();
            $table->timestamp('dropped_at')->nullable();
            $table->timestamp('outDelivery_at')->nullable();
            $table->timestamp('delivered_at')->nullable();
                        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_deliveries');
    }
}
