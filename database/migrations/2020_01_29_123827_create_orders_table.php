<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuId');
            $table->boolean('is_urgent')->default(0);
            $table->boolean('is_hanged')->default(0);
            $table->unsignedBigInteger('total_quantity');
            $table->float('total_amount');
            $table->float('amount');

            $table->unsignedBigInteger('facility_id')->nullable();
            $table->boolean('isPercentage')->nullable();
            $table->float('facility_amount')->nullable();


            $table->unsignedBigInteger('status_id');
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('address_id')->nullable();
            $table->unsignedBigInteger('promo_code_id')->nullable();
            $table->unsignedBigInteger('payment_type_id')->nullable();
            $table->unsignedBigInteger('pickup_time_id')->nullable();
            $table->text('comment')->nullable();

            $table->date('collection_date')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');

    }
}
