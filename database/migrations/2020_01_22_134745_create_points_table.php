<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('points', function (Blueprint $table) {
            $table->bigIncrements('id');
            //$table->integer('pointable_id');
            //$table->string('pointable_type');
            $table->integer('customer_id');
            $table->integer('days_to_expired');
            $table->integer('order_id');
            $table->date('expire_at');
            //$table->float('customer_points'); ///current customer point
            //$table->float('used_points')->default(0);
            $table->float('points'); //current points after this action

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('points');
    }
}
