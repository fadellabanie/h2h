<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacilityServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facility_service', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('service_id');
            $table->unsignedBigInteger('facility_id');
            $table->boolean('isPercentage')->default(0);
            $table->float('amount');
            $table->timestamps();


            $table->foreign('facility_id')
                ->references('id')
                ->on('facilities');

            $table->foreign('service_id')
                ->references('id')
                ->on('services');

            $table->unique(['service_id','facility_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facility_service');
    }
}
