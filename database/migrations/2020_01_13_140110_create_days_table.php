<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('days', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('driver_id');
            $table->boolean('Saturday');
            $table->boolean('Sunday');
            $table->boolean('Monday');
            $table->boolean('Tuesday');
            $table->boolean('Wednesday');
            $table->boolean('Thursday');
            $table->boolean('Friday');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('days');
    }
}
