<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriverRegionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_region', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('region_id');
            $table->unsignedBigInteger('driver_id');


            $table->timestamps();

            // $table->foreign('region_id')
            //     ->references('id')
            //     ->on('regions');

            // $table->foreign('driver_id')
            //     ->references('id')
            //     ->on('drivers');

            $table->unique(['region_id','driver_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_region');
    }
}
