<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            // $table->unsignedBigInteger('region_id');
            $table->unsignedBigInteger('driver_status_id');
            $table->string('license')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('image_url')->nullable();
            $table->string('file_url')->nullable();
            
            ## Add from fadel
             $table->decimal('lng', 10, 7)->nullable();
            $table->decimal('lat', 10, 7)->nullable();
            
            // $table->time('time_from');
            // $table->time('time_to');
            $table->timestamps();

            // $table->foreign('user_id')
            //     ->references('id')
            //     ->on('users')
            //     ->onDelete('restrict');             

            // $table->foreign('driver_status_id')
            //     ->references('id')
            //     ->on('driver_statuses')
            //     ->onDelete('restrict');             

            // $table->foreign('region_id')
            //     ->references('id')
            //     ->on('regions')
            //     ->onDelete('restrict'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
