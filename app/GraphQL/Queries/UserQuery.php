<?php

namespace App\GraphQL\Queries;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Domains\Customer\service\Customer;
use Illuminate\Support\Facades\Hash;
use App\Domains\User\entity\User;
class UserQuery extends Controller{

    function me(){
        return auth()->guard('api')->user();
    }

    

}