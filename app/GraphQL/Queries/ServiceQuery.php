<?php

namespace App\GraphQL\Queries;
use App\Domains\Service\entity\Service;

class ServiceQuery{

    function all(){
        $services = Service::where('isActive',1)->get();
        return $services;
    }

}