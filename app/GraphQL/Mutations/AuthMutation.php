<?php

namespace App\GraphQL\Mutations;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Domains\Customer\service\Customer;
use Illuminate\Support\Facades\Hash;
use App\Domains\User\entity\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthMutation extends Controller{

    

    private $customer;

    public function __construct(Customer $customer)
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
        $this->customer = $customer;

    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login_api($root,$argc)
    {
        $credentials = Arr::only($argc,['email','password']);
        $token = "Unauthorized";
        if (auth()->guard('api')->attempt(['email'=>$credentials['email'],'password'=>$credentials['password'] , 'isActive'=>'1'])) {
            $token = auth()->guard('api')->attempt(['email'=>$credentials['email'],'password'=>$credentials['password']]);
            return $token;
        }

        return $token;
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        if (auth()->guard('api')->user()){
            auth()->guard('api')->logout();
            return 'Successfully logged out';
        }else{

            return 'you are already logged out';
        }
    }
    public function register($root, $argc)
    {
        $argc['password'] = bcrypt($argc['password']);
        $user = $this->customer->new($argc);
        $credentials = Arr::only($argc,['email','password']);
        if (! $token = auth()->guard('api')->attempt($credentials)) {
            return  'Unauthorized';
        }
        return $token;
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->guard('api')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->guard('api')->factory()->getTTL() * 60
        ]);
    }


    public function change_password(){
        if(Hash::check(request()['old_password'], auth()->guard('api')->user()->password))
        {
            $user_id = auth()->guard('api')->user()->id;                       
            $obj_user = User::find($user_id);
            $obj_user->password = Hash::make(request()['new_password']);
            $obj_user->save();
            // auth()->guard('api')->user()->password  = bcrypt(request()['new_password']);
            return response()->json(['message' => 'Successfully Changed !!']);

        }else{
            return response()->json(['message' => 'The old password is incorrect']);
        }
    }

}