<?php

namespace App\GraphQL\Scalars;

use GraphQL\Type\Definition\ScalarType;
use Safe\Exceptions\JsonException;

/**
 * Read more about scalars here http://webonyx.github.io/graphql-php/type-system/scalar-types/
 */
class JsonScalar extends ScalarType
{
    /**
     * Serializes an internal value to include in a response.
     *
     * @param  mixed  $value
     * @return mixed
     */
    public function serialize($value)
    {
        // Assuming the internal representation of the value is always correct
        return json_encode($value);

        // TODO validate if it might be incorrect
    }

    /**
     * Parses an externally provided value (query variable) to use as an input
     *
     * @param  mixed  $value
     * @return mixed
     */
    public function parseValue($value)
    {
        // TODO implement validation

        return $this->decodeJSON($value);
    }

    /**
     * Parses an externally provided literal value (hardcoded in GraphQL query) to use as an input.
     *
     * E.g.
     * {
     *   user(email: "user@example.com")
     * }
     *
     * @param  \GraphQL\Language\AST\Node  $valueNode
     * @param  mixed[]|null  $variables
     * @return mixed
     */
    public function parseLiteral($valueNode, ?array $variables = null)
    {
        // TODO implement validation

        //  if (!property_exists($valueNode, 'value')) {
        //     throw new Error(
        //         'Can only parse literals that contain a value, got '.Utils::printSafeJson($valueNode)
        //     );
        // }

        return $this->decodeJSON($valueNode->value);
    }

    /**
     * Try to decode a user-given value into JSON.
     *
     * @param mixed $value
     *
     * @throws Error
     *
     * @return mixed
     */
    protected function decodeJSON($value)
    {
        $parsed = json_decode($value);

        // try {
        // } catch (JsonException $jsonException) {
        //     throw new Error(
        //         $jsonException->getMessage()
        //     );
        // }

        return $parsed;
    }
}
