<?php

namespace App\Listeners;

use App\Domains\Driver\entity\DriverStatus;
use App\Domains\Order\entity\OrderStatus;
use App\Domains\Order\repository\OrderDeliveryRepository;
use App\Events\OrderRequestEvent;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class DriverOrderListner
{

    protected $repo;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(OrderDeliveryRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Handle the event.
     *
     * @param  OrderRequestEvent  $event
     * @return void
     */
    public function handle(OrderRequestEvent $event)
    {

        $this->repo->acceptOrder($event->order);
        
    }

    public function failed($exception)
    {
       dd($exception);
    }
}
