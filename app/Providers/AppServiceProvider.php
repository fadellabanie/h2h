<?php

namespace App\Providers;

use App\Domains\User\contract\ManagerRepositoryInterface;
use App\Domains\User\repository\ManagerRepository;
use App\Http\View\Composers\ProfileComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $this->app->bind(ManagerRepositoryInterface::class,ManagerRepository::class);

        // View::composer('admin.drivers.show', function ($view) {
        //     //$id = route()->getParameter('id');
        //     dd(request('id'));
        // });

        // View::composer(
        //     'admin.drivers.show',ProfileComposer::class
        // );
    }
}
