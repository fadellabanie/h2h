<?php

namespace App\Providers;

use App\Http\View\Composers\AdminComposer;
use App\Http\View\Composers\ProfileComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        View::composer(
            ['admin.drivers.overview','admin.drivers.personal','admin.drivers.account'],ProfileComposer::class
        );

        View::composer(
            'admin.layout.admin',AdminComposer::class
        );
    }
}