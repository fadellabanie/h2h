<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class WalletResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return $this->collection->transform(function($item){
            return [
                'invoice' => $item->invoiceno,
                'remaining' => $item->remaining,
                'draw' => $item->draw,
            ];
        });
    }
}
