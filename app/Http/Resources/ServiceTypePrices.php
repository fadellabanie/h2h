<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ServiceTypePrices extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        $data = $this->collection->transform(function($item){
            return [
                'id' => $item->id,
                'name' => $item->name,
                'types' => $item->types->transform(function($type){
                    return [
                        'id' => $type->id,
                        'name' => $type->name,
                        'price' => optional($type->services)[0]['pivot']['price']
                    ];
                })
            ];
        });

        return $data;
    }
}
