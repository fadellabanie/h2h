<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CustomerAddressedReponse extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        $address = $this->collection->transform(function($item){

            return [
                'id' => $item->id,
                'building_no' => $item->building_no,
                'floor_no' => $item->floor_no,
                'flat_no' => $item->flat_no,
                'street' => $item->street,
                'city' => optional($item->city)->name,
                'region' => optional($item->region)->name,
                'lat' => $item->lat,
                'lng' => $item->lng
            ];
//            return [
//                'id' => $item->id,
//                'name' => $item->name,
//                'addresses' => $item->addresses->transform(function($address){
//                    return [
//                        'id' => $address->id,
//                        'building_no' => $address->building_no,
//                        'floor_no' => $address->floor_no,
//                        'flat_no' => $address->flat_no,
//                        'street' => $address->street,
//                        'city_id' => $address->city->id,
//                        'city' => $address->city->name,
//                        'region_id' => $address->region->id,
//                        'region_name' => $address->region->name,
//                    ];
//                })
//            ];
        });

        return $address;
    }
}
