<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderItemsResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return $this->collection->transform(function($item){
            return [
                'id' => $item->id,
                'order_id' => $item->order_id,
                'amount' => $item->amount,
                'total_amount' => $item->total_amount,
                'facility_name' => isset($item->facilities[0]) ? $item->facilities[0]['name'] : '',
                'facility_id' => isset($item->facilities[0]) ? $item->facilities[0]['id'] : '',
                'type' => $item->type->name,
                'service' => $item->service->name,
                'category' => $item->category->name,
                'color' => optional($item->color)->name,
                'size' => optional($item->size)->name,
                'brand' => $item->brand,
            ];
        });
    }
}
