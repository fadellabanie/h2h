<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class GiftsResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  $this->collection->transform(function ($gift){
             return [
                 'id' => $gift->id,
                 'name' => $gift->name,
                 'type' => $gift->gift_type_id == 1 ? 'wallet' : 'code',
                 'potins' => $gift->points,
                 'code' => $gift->code,
                 'from' => $gift->from,
                 'to' => $gift->to
             ];
        });
        //return parent::toArray($request);
    }
}
