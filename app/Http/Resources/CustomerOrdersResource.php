<?php

namespace App\Http\Resources;

use App\Domains\Setting\entity\Setting;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CustomerOrdersResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    
    public function toArray($request)
    {
        //return parent::toArray($request);
        return  $this->collection->transform(function ($order){
            return [
                'id' => $order->id,
                'orderNo' => $order->uuId,
                'is_urgent' => $order->is_urgent,
                'is_hanged' => $order->is_hanged,
                'total_quantity' => $order->total_quantity,
                'total_amount' => $order->total_amount,
                'amount' => $order->amount,
                'status' => optional($order->status)->name,
                'status_id' => optional($order->status)->id,
                'pickup_time' => $order->pickupTime,
                'collection_date' => $order->collection_date,
                'tax' => optional(Setting::first())->tax ? optional(Setting::first())->tax : 0,
                // 'items' => $order->items->transform(function ($item){
                //     return [
                //         'amount' => $item->amount,
                //         'total_amount' => $item->total_amount,
                //         'facility_name' => isset($item->facilities[0]) ? $item->facilities[0]['name'] : '',
                //         'facility_id' => isset($item->facilities[0]) ? $item->facilities[0]['id'] : '',
                //         'type' => $item->type->name,
                //         'color' => optional($item->color)->name,
                //         'size' => optional($item->size)->name,
                //         'brand' => $item->brand,
                //     ];
                // }),
                'address' => $order->address == null ? null : [
                    'building_no' => $order->address->building_no,
                    'floor_no' => $order->address->floor_no,
                    'flat_no' => $order->address->flat_no,
                    'street' => $order->address->street,
                    'city' => optional($order->address->city)->name,
                    'region' => optional($order->address->region)->name,
                    'delivery_cost' => optional($order->address->city)->delivery_cost,
                ]
            ];
        });
    }
}
