<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderBasketResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $order = $this->collection->transform(function($item){
            return [
                'order_id' => $item->id,
                'items' => $item->items->map(function($i){
                    return [
                        'id' => $i->id,
                        'amount' => floatval($i->amount),
                        'color' => optional($i->color)->name,
                        'size' => optional($i->size)->name,
                        'brand' => $i->brand,
                        'total_amount' => floatval($i->total_amount),
                        'type' => $i->type->name,
                        'service' => $i->service->name,
                        'service_id' => $i->service->id,
                        'category' => $i->category->name,
                        'category_id' => $i->category->id,
                        'type_id' => $i->type->id,
                        'count' => $i->count,
//                        'facility' => $i->facilityServicePrice($i->facility_id,$i->service_id),
//                        'facility_name' => optional($i->facility)->name,
                        //'facility_id' => isset($i->facilities[0]) ? $i->facilities[0]['id'] : '',
                    ];
                }),

                // 'facilities' => $item->items->map(function($i){
                //     return collect($i->facilities)->transform(function($fac) use($i){
                //         return [
                //             'name' => $fac['name'],
                //             'amount' => $fac['amount'],
                //             'isPercentage' => $fac['isPercentage'],
                //             'price' => $i->amount * ($fac['amount'] / 100) ,
                //         ];
                //     });
                // })[0],
                'facility' => optional($item->facility)->name,
                'facility_amount' => floatval($item->facility_amount),
                'isPercentage' => $item->isPercentage,
                'total_quantity' => floatval($item->total_quantity),
                'order_subtotal' => floatval($item->amount),
                'order_total_amount'=> floatval($item->total_amount)
            ];
        });

        return $order[0];
    }
}
