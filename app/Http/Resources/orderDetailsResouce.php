<?php

namespace App\Http\Resources;

use App\Domains\Setting\entity\Setting;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class orderDetailsResouce extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        //return parent::toArray($request);

        //return  $this->collection->transform(function ($order){
            return [
                'id' => $this->id,
                'orderNo' => $this->uuId,
                'is_urgent' => $this->is_urgent,
                'is_hanged' => $this->is_hanged,
                'total_quantity' => $this->total_quantity,
                'total_amount' => floatval($this->total_amount),
                'subtotal' => floatval($this->amount),
                'status' => optional($this->status)->name,
                'status_id' => optional($this->status)->id,
                'pickup_time' => $this->pickupTime,
                'collection_date' => $this->collection_date,
                'promocode' => $this->promoCode,
                'promo_amount' => $this->getCodeAmount(),
                'payment_type' => $this->PaymentType->name,
                'tax' => optional(Setting::first())->tax,
                'facility' => optional($this->facility)->name,
                'facility_id' => optional($this->facility)->id,
                'facility_amount' => $this->facility_amount,
                'isPercentage' => $this->isPercentage,
                'phone' => optional($this->customer)->user->phone,
                'name' => optional($this->customer)->user->name,
                'items' => $this->items->transform(function ($item){
                    return [
                        'amount' => floatval($item->amount),
                        'total_amount' => floatval($item->total_amount),
                        'facility_name' => optional($item->facility)->name,
                        'facility_id' => optional($item->facility)->id,
                        'count' => $item->count,
                        'type' => $item->type->name,
//                        'color' => optional($item->color)->name,
//                        'size' => optional($item->size)->name,
//                        'brand' => $item->brand,
//                        'facility' => $item->facilityServicePrice($item->facility_id,$item->service_id)
                    ];
                }),
                'address' => $this->address == null ? null : [
                    'building_no' => $this->address->building_no,
                    'floor_no' => $this->address->floor_no,
                    'flat_no' => $this->address->flat_no,
                    'street' => $this->address->street,
                    'city' => optional($this->address->city)->name,
                    'delivery_cost' => optional($this->address->city)->delivery_cost,
                    'region' => optional($this->address->region)->name
                ],
                'invoice' => $this->invoice == null ? null : [
                    'invoiceno' => $this->invoice->invoiceno,
                    'subtotal' => $this->invoice->subtotal,
                    'total' => $this->invoice->total,
                    'tax' => $this->invoice->tax,
                    'delivery_cost' => $this->invoice->delivery_cost,
                ]
            ];
        //});
    }
}
