<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SingleItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
            'id' => $this->id,
            'order_id' => $this->order_id,
            'amount' => $this->amount,
            'total_amount' => $this->total_amount,
            'facility_name' => isset($this->facilities[0]) ? $this->facilities[0]['name'] : '',
            'facility_id' => isset($this->facilities[0]) ? $this->facilities[0]['id'] : '',
            'type' => $this->type->name,
            'type_id' => $this->type->id,
            'service_id' => $this->service->id,
            'service' => $this->service->name,
            'category_id' => $this->category->id,
            'category' => $this->category->name,
            'color' => optional($this->color)->name,
            'size' => optional($this->size)->name,
            'brand' => $this->brand,
            'instruction' => $this->instruction,
        ];
    }
}
