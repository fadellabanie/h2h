<?php


namespace App\Http\View\Composers;

use App\Domains\Driver\repository\DriverRepository;
use App\Repositories\UserRepository;
use Illuminate\View\View;

class ProfileComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $driverRepo;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(DriverRepository $driverRepo)
    {
        $this->driverRepo = $driverRepo;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
    	$driver = $this->driverRepo->findDriver(request('id'),['user.group','regions']);
       	$view->with('driver',$driver);
    }
}