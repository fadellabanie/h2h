<?php


namespace App\Http\View\Composers;

use App\Domains\Setting\entity\Setting;
use Illuminate\View\View;

class AdminComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $setting;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(Setting $setting)
    {
        $this->setting = $setting;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $setting = $this->setting->first();

        if (!$setting) {
            $setting = '';
        }

       	$view->with('setting',$setting);
    }
}