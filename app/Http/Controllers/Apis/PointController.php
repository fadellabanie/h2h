<?php

namespace App\Http\Controllers\Apis;

use Illuminate\Http\Request;
use App\Domains\Point\repository\PointRepository;
use App\Http\Controllers\Controller;
use DataTables;

class PointController extends Controller 
{
    private $model;
    
	public function __construct(PointRepository $model)
	{
        
		$this->model = $model;
    }

    public function index(){
		return Datatables::eloquent($this->model->select(['id','title','point']))->make(true);
    }

    
}