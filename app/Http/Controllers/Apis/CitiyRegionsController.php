<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Region\entity\Region;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CitiyRegionsController extends Controller
{
    

    public function index()
    {
    	$this->validate(request(),[
    		'city_id' => 'required|numeric|exists:cities,id'
    	]);

    	return Region::where('city_id',request('city_id'))->get();
    }
}
