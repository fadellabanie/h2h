<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Category\entity\Category;
use App\Domains\Facility\repository\FacilityRepository;
use App\Domains\Order\entity\OrderItem;
use App\Domains\Service\entity\Service;
use App\Domains\Service\repository\ServiceRepository;
use App\Domains\Type\repository\TypeRepository;
use App\Domains\User\contract\ManagerRepositoryInterface;
use App\Domains\User\entity\User;
use App\Domains\User\service\ManagerService;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddNewManagerRequest;
use App\Http\Resources\ServiceTypePrices;
use DataTables;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;


class ServiceController extends Controller
{
    private $service;

    public function __construct(ServiceRepository $service)
    {
        $this->service = $service;
    }

    public function guard()
    {
        return Auth::guard('api');
    }

    public function index()
    {
        return $this->service->all();
    }


    public function types(TypeRepository $repo)
    {
        $this->validate(request(),[
            'service_id' => 'required|exists:services,id',
            'category_id' => 'required|exists:categories,id'
        ]);

        return $repo->all(request('service_id'),request('category_id'));
    }


    public function prices()
    {
        $this->validate(request(),[
            'service_id' => 'required|exists:services,id',
        ]);

        $service_id = request('service_id');

        $data = Category::with(['types' => function($q){
            $q->with(['services' => function($s){
                $s->where('service_type.service_id',request('service_id'))
                    ->whereNotNull('service_type.price');
            }]);
        }])
            ->get();

        //return $data;
        return new ServiceTypePrices($data);
    }

    public function facilities(FacilityRepository $repo)
    {
        $this->validate(request(),[
            'service_id' => 'required|exists:services,id',
        ]);

        return $repo->facilitiesForService(request('service_id'));
    }

    public function childs($service_id)
    {
        $childs = Service::where('parent_id',$service_id)->get();

        return $childs;
    }

    public function typeChilds()
    {

        Validator::make(request()->json()->all(), [
            'type_id' => 'required|numeric|exists:types,id',
            'service_id' => 'required|numeric|exists:services,id',
            'category_id' => 'required|numeric|exists:categories,id',
        ])->validate();

        $data = request()->json()->all();
        $type_id = $data['type_id'];
        $service_id = $data['service_id'];
        $cat_id = $data['category_id'];


        $data =  Service::with(['types' => function($q) use($type_id,$cat_id){
            $q->where('types.id',$type_id)
                ->whereHas('categories',function($q) use($cat_id){
                    $q->where('categories.id',$cat_id);
                });
        }])->where('services.parent_id',$service_id)->get();

        return $data->transform(function ($data){
            return [
                'id' => $data->id,
                'name' => $data->name,
                'description' => $data->description,
                'isActive' => $data->isActive,
                'parent_id' => $data->parent_id,
                'price' => (string) optional($data->types)[0]['pivot']['price']
            ];
        });

//
//        return Service::whereHas('types',function($q) use($type_id,$cat_id){
//            $q->where('types.id',$type_id)
//            ->whereHas('categories',function($q) use($cat_id){
//                $q->where('categories.id',$cat_id);
//            });
//        })->where('services.parent_id',$service_id)->get();

    }

    public function typePrices()
    {

        Validator::make(request()->json()->all(), [
            'type_id' => 'required|numeric|exists:types,id',
            'service_id' => 'required|numeric|exists:services,id',
        ])->validate();

        $data = request()->json()->all();

        $customer = auth('api')->user()->customer;

        $order = $customer->customerBasket();

        if (!$order) {
            return response()->json(['errors' => 'sorry order not found'],500);
        }

        $count = OrderItem::where('type_id',$data['type_id'])
            ->where('service_id',$data['service_id'])
            ->where('order_id',$order->id)
            ->count();

        return response()->json(['count' => $count]);
    }
}
