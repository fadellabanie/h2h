<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Order\entity\Order;
use App\Domains\Order\exception\PromoCodeNotValidException;
use App\Domains\Order\service\OrderService;
use App\Domains\PromoCode\entity\PromoCode;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PromoCodesController extends Controller
{

	public function index(OrderService $orderService)
	{

        Validator::make(request()->json()->all(), [
            'promo_code' => 'required|string',
            //'order_id' => 'required|numeric|exists:orders,id',
        ])->validate();

        $customer = auth('api')->user()->customer;

        $data = request()->json()->all();

        $now = Carbon::now()->format('Y-m-d');

		$promoCode = PromoCode::where('code',$data['promo_code'])
		->whereDate('from','<=',$now)
		->whereDate('to', '>=',$now)
		->where('status',1)
		->first();

		if (!$promoCode) {
			throw new PromoCodeNotValidException;
		}

		$usedCode = DB::table('customer_promo')
						->where('customer_id',$customer->id)
						->where('promoCode_id',$promoCode->id)
						->first();
		if ($usedCode) {
			throw new PromoCodeNotValidException;
		}		

		
		//$order = Order::where('id',$data['order_id'])->firstOrFail();
		
        $customer = auth('api')->user()->customer;

		//$order = $orderService->addPromoCode($promoCode,$customer);

		return response()->json([
			'id' => $promoCode->id,
			'amount' => $promoCode->amount,
			'isPercentage' => $promoCode->isPercentage,

		]);
	}
}
