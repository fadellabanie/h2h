<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Fcm\DroppedofMessage;
use App\Domains\Fcm\PickedupMessage;
use App\Domains\Order\entity\Order;
use App\Domains\Order\entity\OrderStatus;
use App\Domains\Point\entity\Point;
use App\Domains\Setting\entity\Setting;
use App\Domains\User\entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Domains\Driver\entity\Driver;

class DriversController extends Controller
{


	public function index()
	{

		$driver = auth('api')->user()->driver;

		$today = Carbon::now()->format('Y-m-d');

		$completedOrders = DB::table('order_deliveries')
							->join('orders','order_deliveries.order_id','orders.id')
							->where('order_deliveries.driver_id',$driver->id)
							->where('orders.status_id',OrderStatus::DELIVERIED)
							->count();

        $todayBooking = DB::table('order_deliveries')
                            ->join('orders','order_deliveries.order_id','orders.id')
                            ->join('order_statuses','orders.status_id','order_statuses.id')
                            ->where('orders.status_id',OrderStatus::ACCEPTED)
                            ->where('order_deliveries.driver_id',$driver->id)
                            ->whereDate('accepted_at',$today)
                            ->count();

		$totalBooking = DB::table('order_deliveries')
							->join('orders','order_deliveries.order_id','orders.id')
							->where('order_deliveries.driver_id',$driver->id)
							->count();


		return response()->json([
			'completedOrders' => $completedOrders,
			'todayBooking' => $todayBooking,
			'totalBooking' => $totalBooking,
		]);
	}


	public function pickedup()
	{

        Validator::make(request()->json()->all(), [
            'order_id' => 'required|numeric|exists:orders,id',
        ])->validate();

        $data = request()->json()->all();

       // $driver = auth('api')->user()->driver;

		$today = Carbon::now();

        $order = Order::where('id',$data['order_id'])
        				->where('status_id',OrderStatus::ACCEPTED)
        				->whereHas('delivery',function($q){
        					$q->whereNull('order_deliveries.picked_at');
        				})->firstOrFail();

        $order->update([
        	'status_id' => OrderStatus::PICKEDUP
        ]);

        $order->delivery()->update([
        	'picked_at' => $today
        ]);

        $customer_id = $order->customer_id;

		$user = User::whereHas('customer',function($q) use($customer_id){
            $q->where('customers.id',$customer_id);
        })->first();

        $message = new PickedupMessage($user,$order->id);

        $message->sendMessage();

        return response()->json(['success' => 'Order is picked up successfully']);
	}


	public function dropoff()
	{

		Validator::make(request()->json()->all(), [
            'order_id' => 'required|numeric|exists:orders,id',
        ])->validate();

        $data = request()->json()->all();

        $driver = auth('api')->user()->driver;

		$today = Carbon::now();

        $order = Order::where('id',$data['order_id'])
        				->where('status_id',OrderStatus::PICKEDUP)
        				->whereHas('delivery',function($q){
        					$q->whereNull('order_deliveries.dropped_at');
        				})->firstOrFail();

        $order->update([
        	'status_id' => OrderStatus::PROCESSING
        ]);

        $order->delivery()->update([
        	'dropped_at' => $today
        ]);

        $customer_id = $order->customer_id;

        $user = User::whereHas('customer',function($q) use($customer_id){
            $q->where('customers.id',$customer_id);
        })->first();

        $message = new DroppedofMessage($user,$order->id);

        $message->sendMessage();

        return response()->json(['success' => 'Order is dropped off successfully']);
	}

	public function delivered()
	{

		Validator::make(request()->json()->all(), [
            'order_id' => 'required|numeric|exists:orders,id',
        ])->validate();

        $data = request()->json()->all();

        $driver = auth('api')->user()->driver;

		$today = Carbon::now();

        $order = Order::where('id',$data['order_id'])
        				->where('status_id',OrderStatus::OUTOFDELIVERY)
        				->whereHas('delivery',function($q){
        					$q->whereNull('order_deliveries.delivered_at');
        				})->firstOrFail();

        $order->update([
        	'status_id' => OrderStatus::DELIVERIED
        ]);

        $order->delivery()->update([
        	'delivered_at' => $today
        ]);

        $order->invoice()->update([
        	'invoice_status_id' => 3
        ]);

        $setting = Setting::first();

        if ($setting && $setting->expire_days != null && $setting->fixed_point != null) {
           $this->addPoint($order,$setting);
        }
        return response()->json(['success' => 'Order is delivered successfully']);
	}

    private function addPoint($order,$setting)
    {

        $result = $order->total_amount / 10;

        $points = $result * $setting->fixed_point;

        $expire_date = Carbon::now()->addDays($setting->expire_days)->format('Y-m-d');

        Point::create([
            'customer_id' => $order->customer_id,
            'days_to_expired' => $setting->expire_days,
            'order_id' =>  $order->id,
            'expire_at' => $expire_date,
            'points' => $points
        ]);
    }
    
    public function getLocation(Request $request)
    {
      
       $driver = Auth::user()->driver;
       if(! $driver){
         return response()->json(['error' => 'unauthorized']);

       }
      
       db::table('drivers')->whereId($driver->id)
        ->update([
            'lat'=> $request->lat,
            'lng'=> $request->lng,
        ]);
            
        return response()->json(['success' => 'now you are active']);
    }
}
