<?php

namespace App\Http\Controllers\Apis;

use App\Domains\User\entity\User;
use App\Http\Controllers\Controller;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\PasswordReset;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class ForgotPasswordController extends Controller
{


	public function create()
	{

		Validator::make(request()->json()->all(), [
            'email' => 'required|string|email',
        ])->validate();

		$data = request()->json()->all();

        $user = User::where('email', $data['email'])->first();

        if (!$user)

            return response()->json([
                'message' => 'We can`t find a user with that e-mail address.'
            ], 404);


        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => str_random(5)
             ]
        );

        if ($user && $passwordReset)
            $user->notify(
                new PasswordResetRequest($passwordReset->token)
            );

        return response()->json([
            'message' => 'We have e-mailed your password reset link!'
        ]);

	}


	public function find($token)
    {
        $passwordReset = PasswordReset::where('token',$token)
            ->first();
        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        }
        return response()->json($passwordReset);
    }


    public function reset(Request $request)
    {

        Validator::make(request()->json()->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string|confirmed',
            'token' => 'required|string'
        ])->validate();

        $data = request()->json()->all();


        $passwordReset = PasswordReset::where('token',$data['token'])
        ->where('email',$data['email'])->first();
        // $passwordReset = PasswordReset::where([
        //     ['token', $data['token'],
        //     ['email', $data['email']],
        // ])->first();

        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        $user = User::where('email', $passwordReset->email)->first();
        if (!$user)
            return response()->json([
                'message' => 'We can`t find a user with that e-mail address.'
            ], 404);

        $user->password = bcrypt($request->password);
        $user->save();
        $passwordReset->delete();
        $user->notify(new PasswordResetSuccess($passwordReset));
        return response()->json($user);
    }

}
