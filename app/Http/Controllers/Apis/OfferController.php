<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Offer\repository\OfferRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;

class OfferController extends Controller
{

	private $offers;

	public function __construct(OfferRepository $offers)
	{
		$this->offers = $offers;
	}

    
    public function index()
    {
		return Datatables::eloquent($this->offers->select(['id','offer_title','offer_image_url']))->make(true);
    }



	
	
	
	
}
