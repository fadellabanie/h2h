<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Order\service\OrderService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderFacilitiesController extends Controller
{

    private $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'facility_id' => 'required|numeric|exists:facilities,id',
        ]);

        $data = $request->all();

        return $this->orderService->addFacilities($data['facility_id']);

    }

    public function delete()
    {
        return $this->orderService->removeFacility();
    }

}
