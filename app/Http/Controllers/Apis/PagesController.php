<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Setting\entity\Setting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    

    public function about()
    {
    	
    	$setting = Setting::first();

    	return response()->json(['about' => $setting->about]);
    }

    public function terms()
    {
    	$setting = Setting::first();

    	return response()->json(['terms' => $setting->terms]);
    }
}
