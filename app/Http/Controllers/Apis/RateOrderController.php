<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Order\entity\Order;
use App\Domains\Service\entity\Service;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RateOrderController extends Controller
{



	public function index()
	{
		$this->validate(request(),[
            'order_id' => 'required|numeric|exists:orders,id',
		]);

		$order = Order::where('id',request('order_id'))->first();

		$rate =  $order->rates()->average('rate');

		return response()->json(['rate' => $rate]);
	}

    public function store()
    {


    	Validator::make(request()->json()->all(), [
            'order_id' => 'required|numeric|exists:orders,id',
            'rate' => 'required|numeric',
            'note' => 'nullable|string|min:2|max:256',
        ])->validate();

    	$data = request()->json()->all();
        $customer = auth('api')->user()->customer;

        $rate = DB::table('customer_order_rate_service')
            ->where('order_id',$data['order_id'])
            ->where('customer_id',$customer->id)
            ->first();

        if ($rate) {
            return response()->json(['errors' => 'sorry you already rate this before'],500);
        }

        $customer->rates()->attach($data['order_id'],[
        	'rate' => $data['rate'],
        	'note' => $data['note']
        ]);


        return response()->json(['success' => 'rate added success']);
    }
}
