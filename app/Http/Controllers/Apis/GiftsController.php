<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Gift\entity\Gift;
use App\Domains\Point\entity\Point;
use App\Http\Controllers\Controller;
use App\Http\Resources\GiftsResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GiftsController extends controller
{

    public function index()
    {

        $now = Carbon::now()->format('Y-m-d');

        $gifts =  Gift::where('isActive',1)
            ->whereDate('to','>=',$now)
            ->paginate(30);

        return new GiftsResource($gifts);
    }

    public function redeem()
    {
        Validator::make(request()->json()->all(), [
            'gift_id' => 'required|numeric|exists:gifts,id',
        ])->validate();


        $data = request()->json()->all();

        $customer = auth('api')->user()->customer;

        $nowDate = Carbon::now()->format('Y-m-d');

        $gift = Gift::where('id',$data['gift_id'])
            ->whereDate('from','<=',$nowDate)
            ->whereDate('to','>=',$nowDate)
            ->where('isActive',1)
            ->first();

        if (!$gift){
            return response()->json(['error' => 'sorry you can`t use this gift']);
        }

        //$customerPoints = $customer->points;

        $now = Carbon::now()->format('Y-m-d');

        $customerPoints = Point::where('customer_id',$customer->id)
                        ->whereDate('expire_at','>=',$now)
                        ->sum('points');

        $requiredPoints = $gift->points;

        if ($customer->gifts()->exists()){
            return response()->json(['errors' => 'sorry you already use this gift'],500);
        }

        if ($customerPoints < $requiredPoints){
            return response()->json(['errors' => 'you don`t have enough points'],500);
        }

        //TO DO send mail with gift code

        //calculate customer points
        $remainPoints = $customerPoints - $requiredPoints;
        $customer->update([
            'points' => $remainPoints
        ]);

        //add customer gift
        $customer->gifts()->attach($gift->id);

        Point::create([
           'pointable_id' => $gift->id,
           'pointable_type' => Gift::class,
           'customer_id' => $customer->id,
           'customer_points' => $customerPoints,
           'used_points' => $gift->points,
           'points' => $remainPoints
        ]);


        return response()->json(['success' => 'we will send gift code to your mail']);
    }
}
