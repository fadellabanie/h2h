<?php

namespace App\Http\Controllers\Apis;

use App\Domains\City\entity\City;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CitiesController extends Controller
{
    


    public function index()
    {
    	return City::all();
    }
}
