<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Order\entity\OrderStatus;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Domains\Driver\entity\Driver;
use App\Domains\Order\entity\Order;
class DriverOrdersController extends Controller
{


    public function completed()
    {

        $driver = auth('api')->user()->driver;

        return DB::table('order_deliveries')
            ->join('orders','order_deliveries.order_id','orders.id')
            ->where('order_deliveries.driver_id',$driver->id)
            ->where('orders.status_id',OrderStatus::DELIVERIED)
            ->select('orders.id','order_deliveries.delivered_at')
            ->get();
    }

     public function listDrivers()
    {
        $drivers = Driver::all();
        
        return response()->json([
            'success' => 'now you are active',
            'data' =>  $drivers,
            ]);

           
    }
        
    public function pickup()
    {
    
     $driver = auth('api')->user()->driver;
        return DB::table('order_deliveries')
            ->join('orders','order_deliveries.order_id','orders.id')
            ->join('addresses','orders.address_id','addresses.id')
            ->join('pickup_times','orders.pickup_time_id','pickup_times.id')
            ->join('invoices','orders.id','invoices.order_id')
            ->join('order_statuses','orders.status_id','order_statuses.id')
            ->where('order_deliveries.driver_id',21)
            
           //fadl ->where('orders.status_id',OrderStatus::ACCEPTED)REQUESTED
             ->where('orders.status_id',OrderStatus::REQUESTED)
           // fadl ->orWhere('orders.status_id',OrderStatus::PICKEDUP)
            //->whereNull('order_deliveries.picked_at')
            ->select('orders.id','orders.collection_date','pickup_times.from','pickup_times.to','invoices.invoiceno','order_statuses.name','addresses.lat','addresses.lng','orders.status_id')
            ->get();
    }
    public function acceptOrder($order_id)
    {
       
          //$driver = auth('api')->user()->driver;
         
           DB::table('orders')->where('id',$order_id)->update(['status_id'=>OrderStatus::ACCEPTED]);
        
       
          
          DB::table('order_deliveries')
          ->insert([
              'order_id' => $order_id,
              'driver_id' =>21 ,
              'accepted_at' => Carbon::now(),
              
              
              ]);
              
          return response()->json([
            'success' => 'Order ACCEPTED'
        ]);
        
    }
    public function dropoff()
    {
        
        
        
/*
Fadl
        $driver = auth('api')->user()->driver;

        return DB::table('order_deliveries')
            ->join('orders','order_deliveries.order_id','orders.id')
            ->join('invoices','orders.id','invoices.order_id')
            ->join('order_statuses','orders.status_id','order_statuses.id')
            ->join('addresses','orders.address_id','addresses.id')
            ->where('order_deliveries.driver_id',$driver->id)
            //->where('orders.status_id',OrderStatus::PICKEDUP)
            ->where('orders.status_id',OrderStatus::OUTOFDELIVERY)
            //->whereNull('order_deliveries.dropped_at')
            ->select('orders.id','order_deliveries.picked_at','invoices.invoiceno','order_statuses.name','addresses.lat','addresses.lng','orders.status_id')
            ->get();
*/
    }

    public function today()
    {

        $driver = auth('api')->user()->driver;
        $now = Carbon::now()->format('Y-m-d');

        return DB::table('order_deliveries')
            ->join('orders','order_deliveries.order_id','orders.id')
            ->join('addresses','orders.address_id','addresses.id')
            ->join('invoices','orders.id','invoices.order_id')
            ->join('order_statuses','orders.status_id','order_statuses.id')
            ->where('order_deliveries.driver_id',$driver->id)
            ->where('orders.status_id',OrderStatus::ACCEPTED)
            ->whereDate('order_deliveries.accepted_at',$now)
            // ->whereNull('order_deliveries.dropped_at')
            // ->whereNull('order_deliveries.picked_at')
            ->select('orders.id','order_deliveries.accepted_at','invoices.invoiceno','order_statuses.name','addresses.lat','addresses.lng')
            ->get();
    }
}
