<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Category\repository\CategoryRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{

	private $catRepo;

	public function __construct(CategoryRepository $catRepo)
	{
		$this->catRepo = $catRepo;
	}

	public function index()
	{
		return $this->catRepo->all();
	}

}
