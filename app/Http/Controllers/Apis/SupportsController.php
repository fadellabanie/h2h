<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Support\entity\Support;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SupportsController extends Controller
{

    public function store()
    {

        Validator::make(request()->json()->all(), [
            'title' => 'required|min:2|max:256|string',
            'description' => 'required|min:2|max:256|string',
        ])->validate();

        $data = request()->json()->all();

        $customer = auth('api')->user()->customer;

        Support::create([
            'title' => $data['title'],
            'description' => $data['description'],
            'customer_id' => $customer->id
        ]);

        return response()->json(['success' => 'Email Sent']);
    }
}
