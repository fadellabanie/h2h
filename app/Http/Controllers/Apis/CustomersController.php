<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Point\entity\Point;
use App\Http\Controllers\Controller;
use App\Notification;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CustomersController extends Controller
{



    public function points()
    {
    	$customer_id = auth('api')->user()->customer->id;

        $now = Carbon::now()->format('Y-m-d');

        $points = Point::where('customer_id',$customer_id)
                        ->whereDate('expire_at','>=',$now)
                        ->sum('points');

    	return response()->json(['points' => $points]);
    }


    public function wallet()
    {
    	$customer = auth('api')->user()->customer;

    	return response()->json(['wallet' => $customer->wallet]);
    }

    public function notifications()
    {
        $user = auth('api')->user();

        return Notification::where('reciver_id',$user->id)->get();
    }
}
