<?php

namespace App\Http\Controllers\Apis;

use App\Domains\User\entity\User;
use App\Domains\Customer\entity\Customer;
use App\Domains\Customer\entity\Address;
use App\Domains\Customer\repository\AddressRepository;
use App\Domains\Invoice\repository\InvoiceRepository;
use App\Domains\OrderItem\service\OrderItemService;
use App\Domains\Order\entity\Order;
use App\Domains\Order\service\OrderService;
use App\Domains\Setting\entity\Setting;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Resources\OrderBasketResource;
use App\Http\Resources\orderDetailsResouce;
use App\Jobs\OrderDeliveryJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OrdersController extends Controller
{


	private $orderService;

	public function __construct(OrderService $orderService)
	{
		$this->orderService = $orderService;
	}



    public function basket()
    {
        $customer = auth('api')->user()->customer;

        //return $customer;
        $order = $customer->basket();

        if ($order->isEmpty()) {
            return response()->json(['errors' => 'no order in the basket yet']);
        }

        return new OrderBasketResource($order);
    }


    public function update()
    {

        Validator::make(request()->json()->all(), [
            'promo_code' => 'present|nullable|string',
            'is_hanged' => 'required|boolean',
            'is_urgent' => 'required|boolean',
            'comment' => 'present|nullable|string',
            'amount' => 'required|numeric',
            'promo_amount' => 'present|nullable|numeric',
            'total_amount' => 'required|numeric'
        ])->validate();


        $data = request()->json()->all();

        $customer = auth('api')->user()->customer;

        $order = $this->orderService->update($data, $customer);

        return response()->json([
            'success' => 'order updated success'
        ]);
    }

    public function store(Request $request)
    {

        $this->validate($request, [
    		'service_id'    => 'required|numeric|exists:services,id',
    		'category_id'   => 'required|numeric|exists:categories,id',
            'type_id'       => 'required|numeric|exists:types,id',
    		'count'         => 'required|numeric'
        ]);

    	$data = $request->all();

        $customer = auth('api')->user()->customer;
    
    	$order = $this->orderService->store($data, $customer);
    
        return response()->json([
            'success' => 'order added success',
            'order_id' => $order->id
        ]);
    }

    public function incrementItem(Request $request)
    {
     
        $this->validate($request, [
            'category_id'   => 'required|numeric|exists:categories,id',
    		'service_id'    => 'required|numeric|exists:services,id',
            'type_id'       => 'required|numeric|exists:types,id',
        ]);

        $data = $request->all();

        $customer = auth('api')->user()->customer;
    
        $order = $this->orderService->increment($data, $customer);
        
        return response()->json([
            'success' => 'item added successfully',
            'total_quantity' => $order->total_quantity 
        ]);
    }

    public function decrementItem(Request $request)
    {
        $this->validate($request, [
            'category_id'   => 'required|numeric|exists:categories,id',
    		'service_id'    => 'required|numeric|exists:services,id',
            'type_id'       => 'required|numeric|exists:types,id',
        ]);

        $data = $request->all();

        $customer = auth('api')->user()->customer;
    
        $order = $this->orderService->decrement($data, $customer);
        
        return response()->json([
            'success' => 'item removed successfully',
            'total_quantity' =>  $order->total_quantity ?? '0'
        ]);
    }


    public function confirm(InvoiceRepository $invoiceRepo)
    {

        Validator::make(request()->json()->all(), [
            'payment_type_id' => 'required|numeric|exists:payment_types,id',
        ])->validate();

        $data = request()->json()->all();

        $customer = auth('api')->user()->customer;

        $order = $this->orderService->confirmOrder($data,$customer);

        $invoice = $invoiceRepo->generate($order);

        if ($data['payment_type_id'] == 2) {

            $customer->validateWallet($invoice);

            $customer->decrement('wallet',$invoice->total);
            $draw = $invoice->total;
            $remaining = $customer->wallet;
            DB::table('wallet_transform')
                ->insert([
                    'invoice_id' => $invoice->id,
                    'draw' => $draw,
                    'remaining' => $remaining,
                    'customer_id' => $customer->id
                ]);

            $invoice->update([
                'invoice_status_id' => 3
            ]);
        }

        if ($data['payment_type_id'] == 3) {
            $invoice->update([
                'invoice_status_id' => 3
            ]);
        }

        OrderDeliveryJob::dispatch($order);
            //->delay(now()->addMinutes(1));

        return response()->json([
            'success' => 'Order Confirmed'
        ]);

    }

    public function place()
    {

        Validator::make(request()->json()->all(), [
            'address_id' => 'required|numeric|exists:addresses,id',
            'pickup_time_id' => 'required|numeric|exists:pickup_times,id',
            'collection_date' => 'required|date_format:Y-m-d',
        ])->validate();

        $customer = auth('api')->user()->customer;

        $data = request()->json()->all();

        $address = Address::findOrFail($data['address_id']);

        $this->orderService->placeOrder($data,$customer,$address);

        return response()->json([
            'success' => 'Order Placed Success'
        ]);
    }

    public function cancel(Request $request)
    {

        $this->validate($request, [
            'order_id' => 'required|numeric|exists:orders,id',
        ]);

        $customer = auth('api')->user()->customer;
        $data = $request->all();

        $this->orderService->cancelOrder($data['order_id']);

        return response()->json(['success' => 'Order Cancelled Success']);
    }


    public function track()
    {
        Validator::make(request()->json()->all(), [
            'order_id' => 'required|numeric|exists:orders,id',
        ])->validate();
        $data = request()->json()->all();

        $order = Order::where('id',$data['order_id'])->with('status')->firstOrFail();

        return response()->json([
            'status' => optional($order->status)->name
        ]);

    }

    public function urgent()
    {

        Validator::make(request()->json()->all(), [
            'order_id' => 'required|numeric|exists:orders,id',
        ])->validate();

        $data = request()->json()->all();
        $order = Order::where('id',$data['order_id'])->firstOrFail();

        $this->orderService->addUrgentToOrder($order,$data['is_urgent']);

        return response()->json([
            'total_amount' => $order->total_amount,
            'amount' => $order->amount
        ]);
    }

    public function details($order_id)
    {
        $order = Order::where('id',request('order_id'))->with('items.type','items.service','items.color','items.size','invoice','pickupTime','items.facility','address','customer.user')
        ->firstOrFail();
       
        $setting = Setting::first();
        //return $order;
        return new orderDetailsResouce($order);
    }
}
