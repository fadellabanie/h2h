<?php

namespace App\Http\Controllers\Apis;

use App\Domains\User\contract\ManagerRepositoryInterface;
use App\Domains\User\entity\User;
use App\Domains\User\service\ManagerService;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddNewManagerRequest;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Domains\Customer\service\Customer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Http\JsonResponse;
class CustomerController extends Controller 
{
    use Notifiable;
    private $customer;

	public function __construct(Customer $customer)
	{
		$this->customer = $customer;
	}
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

	function login(){
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => Str::random(60),
            'expires_in' => $this->guard()->factory()->getTTL() * 1000000
        ]);
    }

    public function guard()
    {
        return Auth::guard('api');
    }

    public function register()
    {
       
        $this->validate(request(),[
    		'name' => 'required|min:2|max:256|string|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/',
            'email' => 'required|email|ends_with:yahoo.com,gmail.com,hotmail.com|unique:users,email',
            'password' => 'required|min:4|max:255',
            'address' => 'present|nullable',
            'phone' => 'present|nullable|regex:/(01)[0-9]{9}/|unique:users,phone',
            'package_id'=>'required'
            
        ]);
        $user = $this->customer->new(request()->all());
        
        $token = auth()->login($user);

      return $this->respondWithToken($token);
    }


    public function logout() {
        Auth::guard('api')->logout();

        return response()->json([
            'status' => 'success',
            'message' => 'logout'
        ], 200);
    }
}
