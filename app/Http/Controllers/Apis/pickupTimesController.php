<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Setting\entity\PickupTime;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class pickupTimesController extends Controller
{
    

    public function index()
    {
    	return PickupTime::get(['id','from','to']);
    }
}
