<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Order\entity\OrderItem;
use App\Domains\Order\entity\OrderStatus;
use App\Domains\Order\service\OrderService;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderBasketResource;
use App\Http\Resources\OrderItemsResource;
use App\Http\Resources\SingleItemResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OrderItemsController extends Controller
{

    private $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }


    public function index()
    {

        Validator::make(request()->json()->all(), [
            'type_id' => 'required|numeric|exists:types,id',
            'service_id' => 'required|numeric|exists:services,id',
            'category_id' => 'required|numeric|exists:categories,id',
        ])->validate();

        $data = request()->json()->all();

        $customer = auth('api')->user()->customer;
        $order = $customer->customerBasket();

        $items = OrderItem::where('order_id',$order->id)
        ->where('type_id',$data['type_id'])
        ->where('service_id',$data['service_id'])
        ->where('category_id',$data['category_id'])
        ->with('service','type','category','size','color')
        ->get();

        return new OrderItemsResource($items);
    }


    public function show($item_id)
    {
        $item = OrderItem::where('id',$item_id)
            ->whereHas('order',function($q){
                $q->where('status_id',OrderStatus::BASKET);
            })
            ->with('service','type','category','size','color')->firstOrFail();

        return new SingleItemResource($item);
    }

    public function update($item_id)
    {

        Validator::make(request()->json()->all(), [
            'type_id' => 'required|numeric|exists:types,id',
            'service_id' => 'required|numeric|exists:services,id',
            'category_id' => 'required|numeric|exists:categories,id',
        ])->validate();

    }

    public function delete()
    {

        Validator::make(request()->json()->all(), [
            'order_id' => 'required|numeric|exists:orders,id',
            'item_id' => 'required|numeric|exists:order_items,id',
        ])->validate();

        $data = request()->json()->all();


        $this->orderService->deleteItem($data['order_id'],$data['item_id']);


        return response()->json([
            'success' => 'item deleted success'
        ]);
    }

    public function facilities()
    {

        Validator::make(request()->json()->all(), [
            'facility_id' => 'present|nullable|numeric|exists:facilities,id',
        ])->validate();

        $data = request()->json()->all();

        $order = $this->orderService->toggleFacilities($data);

        return response()->json([
            'order' => $order,
            'items' => $order->items->transform(function($item){
                return [
                    'id' => $item->id,
                    'order_id' => $item->order_id,
                    'amount' => $item->amount,
                    'total_amount' => $item->total_amount,
                    'type' => optional($item->type)->name,
                    'type_id' => optional($item->type)->id,
                    'service' => optional($item->service)->name,
                    'service_id' => optional($item->service)->id,
                    'category' => optional($item->category)->name,
                    'category_id' => optional($item->category)->id,
                    'color' => optional($item->color)->name,
                    'size' => optional($item->size)->name,
                    'brand' => $item->brand,
                    'facility' => $item->facility,
                    'facility_name' => optional($item->facility)->name,
                    'count' => $item->count
                ];
            })
        ]);

        return new OrderItemsResource($order->items);

    }
}

