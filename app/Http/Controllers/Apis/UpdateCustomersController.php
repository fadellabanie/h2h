<?php

namespace App\Http\Controllers\Apis;

use App\Exceptions\PasswordValidException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UpdateCustomersController extends Controller
{


	public function name()
	{

		Validator::make(request()->json()->all(), [
    		'name' => 'required|string|min:2|max:255|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/',
        ])->validate();


    	$data = request()->json()->all();

    	$user = auth('api')->user();

    	$user->update(['name' => $data['name']]);

    	return response()->json(['success' => 'name updated success']);

    }

    public function phone()
    {


    	Validator::make(request()->json()->all(), [
            'phone' => 'required|numeric|regex:/(01)[0-9]{6}/|unique:users,phone',
            'password' => 'required|confirmed'
        ])->validate();

    	$data = request()->json()->all();

    	$user = auth('api')->user();


    	$this->checkPassword($data['password'],$user->password);


    	$user->update(['phone' => $data['phone']]);

    	return response()->json(['success' => 'phone updated success']);

    }

    public function email()
    {

    	$user = auth('api')->user();

    	Validator::make(request()->json()->all(), [
            'email' => 'required|unique:users,email,'.$user->id,
            'password' => 'required|confirmed'
        ])->validate();

    	$data = request()->json()->all();

    	$this->checkPassword($data['password'],$user->password);

    	$user->update(['email' => $data['email']]);

    	return response()->json(['success' => 'email updated success']);


    }


    public function dateOfBirth()
    {

    	Validator::make(request()->json()->all(), [
            'date_of_birth' => 'required|date_format:Y-m-d',
        ])->validate();


        $customer = auth('api')->user()->customer;
        $data = request()->json()->all();

    	$customer->update(['date_of_birth' => $data['date_of_birth']]);

    	return response()->json(['success' => 'date  updated success']);

    }

    public function gender()
    {
    	Validator::make(request()->json()->all(), [
            'gender' => 'required|boolean',
        ])->validate();

        $customer = auth('api')->user()->customer;
        $data = request()->json()->all();

    	$customer->update(['gender' => $data['gender']]);

    	return response()->json(['success' => 'gender  updated success']);

    }

    public function password()
    {

    	Validator::make(request()->json()->all(), [
            'old_password' => 'required',
            'password' => 'required|confirmed'
        ])->validate();


    	$data = request()->json()->all();

    	$user = auth('api')->user();


    	$this->checkPassword($data['old_password'],$user->password);

    	$user->update(['password' => bcrypt($data['password'])]);

    	return response()->json(['success' => 'Password updated successfully']);

    }


    public function image()
    {
    	$this->validate(request(),[
            'image' =>  'required|image|mimes:jpeg,jpg,png|max:10000',
    	]);


        $customer = auth('api')->user()->customer;

		if ($customer->image != null ) {
			unlink(public_path('uploads/'.$customer->image));
		}

		$customer->update([
			'image' => request('image')->store('customers/avatars/'.$customer->id,'uploads')
		]);

        $link = auth('api')->user()->customer->image;
		return response()->json([
            'success' => 'image  updated success',
            'link' => $link
        ]);

    }

    private function checkPassword($newPass,$oldPass)
    {
    	if (Hash::check($newPass,$oldPass)) {
    		return ;
    	}

    	throw new PasswordValidException();

    }

}
