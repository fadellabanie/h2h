<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Type\entity\Size;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SizesController extends Controller
{
    

    public function index()
    {
    	return Size::all();
    }
}
