<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Customer\entity\Address;
use App\Domains\Customer\entity\AddressType;
use App\Domains\Customer\repository\AddressRepository;
use App\Domains\Customer\repository\CustomerRepository;
use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerAddressedReponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AddressesController extends Controller
{


    public function index()
    {

        $this->validate(request(),[
            'address_type_id' => 'required|numeric|exists:address_types,id'
        ]);


    	$customer_id = auth('api')->user()->customer->id;

    	//return $customer_id;
        $addresses = Address::where('address_type_id',request('address_type_id'))
            ->where('customer_id',$customer_id)
            ->with('city','region')
            ->get();

//        $addresses = DB::table('address_types')
//                        ->join('addresses','address_types.id','addresses.address_type_id')
//                        ->join('cities','addresses.city_id','cities.id')
//                        ->join('regions','addresses.region_id','regions.id')
//                        ->where('addresses.customer_id',$customer_id)
//                        ->where('address_types.id',request('address_type_id'))
//
//                        ->groupBy('address_types.id')
//                        ->select('address_types.name as type','addresses.building_no','addresses.floor_no','addresses.flat_no','addresses.street','cities.name','regions.name')
//                        ->get();
//
//        return $addresses;


        return new CustomerAddressedReponse($addresses);
    }


    public function store(AddressRepository $repo)
    {

    	$this->validate(request(),[
            'address_type_id' => 'required|numeric|exists:address_types,id',
            'city_id' => 'required|numeric|exists:cities,id',
            'region_id' => 'required|numeric|exists:regions,id',
            'building_no' => 'required|string',
            'floor_no' => 'required|numeric',
            'flat_no' => 'required|numeric',
            'street' => 'required|string',
    	]);


    	$customer_id = auth('api')->user()->customer->id;

    	$repo->store(request()->json()->all(),$customer_id);

    	return response()->json(['success' => 'added success']);
    }


    public function delete($id)
    {
        $customer_id = auth('api')->user()->customer->id;

        Address::where('id',$id)
            ->where('customer_id',$customer_id)
            ->delete();

        return response()->json(['success' => 'Address is deleted successfully']);
    }
}
