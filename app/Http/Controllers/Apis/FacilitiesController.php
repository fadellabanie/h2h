<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Facility\entity\Facility;
use App\Domains\Facility\repository\FacilityRepository;
use App\Domains\Order\service\OrderService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FacilitiesController extends Controller
{


	private $facilityRepo;

	public function __construct(FacilityRepository $facilityRepo)
	{
		$this->facilityRepo = $facilityRepo;
	}


    public function index()
    {

        $facilitoes = Facility::whereNotNull('parent_id')->get();

        return $facilitoes;
        //return response()->json(['total_amount' => $order->total_amount]);

    }

    public function childs()
    {
    	$this->validate(request(),[
    		'parent_id' => 'required|numeric|exists:facilities,id'
    	]);

    	return $this->facilityRepo->childs(request('parent_id'));
    }


}
