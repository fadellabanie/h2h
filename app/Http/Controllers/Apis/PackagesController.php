<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Package\entity\Package;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PackagesController extends Controller
{


    public function index()
    {
        return Package::where('isActive',1)->paginate(30);
    }

    public function store()
    {

        Validator::make(request()->json()->all(), [
            'package_id' => 'required|numeric|exists:packages,id',
        ])->validate();

        $customer = auth('api')->user()->customer;

        $data = request()->json()->all();
        $package = Package::where('isActive',1)->where('id',$data['package_id'])->firstOrFail();
        $amount = $package->cost;

        $package->customers()->attach($customer->id,[
        	'amount' => $amount
        ]);

        $customer->increment('wallet',$package->cost);

        return response()->json(['success' => 'Purchased successfully']);
    }
}
