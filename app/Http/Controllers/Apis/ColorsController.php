<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Type\entity\Color;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ColorsController extends Controller
{
    

    public function index()
    {
    	return Color::all();
    }
}
