<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Invoice\entity\Invoice;
use App\Domains\Invoice\repository\InvoiceRepository;
use App\Domains\Setting\entity\Setting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use PDF;


class InvoicesController extends Controller
{


    public function index(InvoiceRepository $invoiceRepo)
    {

    	Validator::make(request()->json()->all(), [
            'invoiceno' => 'required|string|exists:invoices,invoiceno',
        ])->validate();


    	$data = request()->json()->all();

        $invoice = Invoice::where('invoiceno',$data['invoiceno'])->with('order.customer')->first();

        $customer = auth('api')->user()->customer;

        if ($invoice->order->customer->id !== $customer->id) {
            return response()->json(['errors' => 'sorry you can`t view this invoice'],500);
        }

        return response()->download(storage_path('/app/invoices/'.$invoice->invoiceno.'.pdf','local'));

    // 	   $invoiceId = Invoice::where('order_id',$data['order_id'])->first();

  	//     $invoice = $invoiceRepo->find($invoiceId->id,['order.items.service','order.items.type','order.items.color','order.items.size','order.address','order.customer.user']);

    //     $pdf = PDF::loadView('admin.invoices.pdf',compact('invoice'));

    //     $link = $pdf->save(public_path().'/uploads/customers/invoices/'.$invoice->order->uuId.''.$invoice->invoiceno.'.pdf');

    //     $url = asset('uploads/customers/invoices/'.$invoice->order->uuId.''.$invoice->invoiceno.'.pdf');

    //     return response()->json(['invoice' => $url]);


    }
}
