<?php

namespace App\Http\Controllers\Apis;

use App\Http\Controllers\Controller;
use App\Http\Resources\WalletResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WalletsController extends Controller
{


    public function index()
    {
    	$customer = auth('api')->user()->customer;

    	$q = DB::table('wallet_transform')
    		->join('invoices','wallet_transform.invoice_id','invoices.id')
    		->join('customers','wallet_transform.customer_id','customers.id')
    		->where('customer_id',$customer->id)
    		->select('customers.wallet','invoices.invoiceno','wallet_transform.remaining','wallet_transform.draw')->get();

    	return new WalletResource($q);
    }
}
