<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Order\entity\Order;
use App\Domains\Order\entity\OrderStatus;
use App\Http\Controllers\Controller;
use App\Http\Resources\CustomerOrdersResource;
use Illuminate\Http\Request;

class CustomerOrdersController extends Controller
{



    public function index()
    {

    	$customer = auth('api')->user()->customer;
    	$query = Order::query();

    	$data = $query->where('customer_id',$customer->id)->paginate(10);

    	// if (request('active') == 1) {
    	// 	$query
    	// 		->where('status_id','>=',OrderStatus::ACCEPTED)
    	// 		->where('status_id','<=',OrderStatus::OUTOFDELIVERY)
    	// 		->orWhere('status_id','=',OrderStatus::REQUESTED);
    	// }

    	// if (request('active') == 0) {
    	// 	$query
    	// 		->where('status_id','=',OrderStatus::BASKET)
    	// 		->orWhere('status_id','<=',OrderStatus::PLACED)
    	// 		->orWhere('status_id','=',OrderStatus::CANCELLED)
    	// 		->orWhere('status_id','=',OrderStatus::DELIVERIED);
    	// }

     //    $data = $query->with('items','address.city','address.region','status','items')->paginate(10);
        return new CustomerOrdersResource($data);
    }
}
