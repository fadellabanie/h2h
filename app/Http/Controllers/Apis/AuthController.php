<?php

namespace App\Http\Controllers\Apis;

use App\Domains\Customer\service\Customer;
use App\Domains\User\entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    private $customer;

    public function __construct(Customer $customer)
    {
        $this->middleware('auth:api', ['except' => ['login','register','social']]);
        $this->customer = $customer;

    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        $this->validate($request, [
            'email' => 'required',
            'password' => 'required|min:4|max:255',
            'device_id' => 'nullable|string',
        ]);


        $credentials = $this->getCredentials();

        //$credentials = request(['email', 'password']);

        if (! $token = auth()->guard('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user = auth('api')->user();

        //return request('device_id');
        $this->addDeviceId($user,request('device_id'));

        return $this->respondWithToken($token);
    }

    private function addDeviceId($user,$device_id)
    {
        return $user->update(['device_id' => $device_id]);
    }

    private function getCredentials()
    {

        if(is_numeric(request('email'))){
            return ['phone'=>request('email'),'password'=>request('password')];
        }

        return ['email' => request('email'), 'password' => request('password')];
    }
    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->guard('api')->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        if (auth()->guard('api')->user()){
            auth('api')->user()->update(['device_id' => null]);
            auth()->guard('api')->logout();
            return response()->json(['message' => 'Successfully logged out']);
        }else{

            return response()->json(['message' => 'you are already logged out']);
        }
    }


    public function register(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|min:2|max:256|string',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:4|max:255|confirmed',
            'phone' => 'required|unique:users,phone',
        ]);

        $data = $request->all();

        $this->customer->new($data);

        // email data
        $email_data = array(
            'name' => $data['name'],
            'email' => $data['email'],
        );

        // send email with the template
        Mail::send('welcome_email', $email_data, function ($message) use ($email_data) {
            $message->to($email_data['email'], $email_data['name'])
                ->subject('Welcome to Hand 2 Hanger')
                ->from('info@h2h.theportal.agency', 'Hand 2 Hanger');
        });

        return response()->json(['success' => 'created succes']);

    }


    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->guard('api')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {

        $user = auth('api')->user();

        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'name' => $user->name,
            'email' => $user->email,
            'phone' => $user->phone,
            'image' => optional($user->customer)->image,
            'date_of_birth' => optional($user->customer)->date_of_birth,
            'gender' => optional($user->customer)->gender,
            'wallet' => optional($user->customer)->wallet
        ]);
    }

    public function social(Request $request)
    {

        $this->validate($request, [
            'password'  => 'required|min:4|max:255',
            'provider'  => 'required|string',
            'token'     => 'required|string',
            'device_id' => 'required|string'
        ]);

        $user = Socialite::driver($request->provider)->stateless()->user();

        // dd($user);
        
        $email = $user->getEmail();
        
        $persistUser = User::where('email', $email)->first();
        
        if ($persistUser) {

            $token = auth()->guard('api')->attempt(['email' => $email, 'password' => $request->password ]);

            if (!$token) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }

            $user = auth('api')->user();

            $this->addDeviceId($user, $request->device_id);
            
            return $this->respondWithToken($token);
        }


        $data['name'] = $user->getName();
        $data['email'] = $email;
        $data['device_id'] = $request->device_id;
        $data['token'] = $request->token;

        $this->customer->new($data);

        $token = auth()->guard('api')->attempt([$email, $request->password]);

        if (!$token) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $user = auth('api')->user();
        $this->addDeviceId($user, $request->device_id);

        return $this->respondWithToken($token);
    }


    public function change_password(){
        if(Hash::check(request()['old_password'], auth()->guard('api')->user()->password))
        {
            $user_id = auth()->guard('api')->user()->id;
            $obj_user = User::find($user_id);
            $obj_user->password = Hash::make(request()['new_password']);
            $obj_user->save();
            // auth()->guard('api')->user()->password  = bcrypt(request()['new_password']);
            return response()->json(['message' => 'Successfully Changed !!']);

        }else{
            return response()->json(['message' => 'The old password is incorrect']);
        }
    }
}
