<?php

namespace App\Http\Controllers\Admin\Packages;

use App\Domains\Package\entity\Package;
use App\Domains\Package\repository\PackageRepository;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class PackagesController extends Controller
{

	private $packageRepo;

	public function __construct(PackageRepository $packageRepo)
	{
		$this->packageRepo = $packageRepo;
	}

    public function index()
    {

    	if (!Gate::allows('package_access')) {
            return abort(401);
        }


    	if (request()->ajax()) {
    		return Datatables::eloquent($this->packageRepo->select([]))->make(true);
    	}
    	return view('admin.packages.index');
	}

    public function show($id)
    {

    	if (!Gate::allows('package_view')) {
            return abort(401);
        }

    	$package = Package::findOrFail($id);

    	return view('admin.packages.show',compact('package'));
    }


	public function delete($id)
    {
    	if (!Gate::allows('package_delete')) {
            return abort(401);
        }

    	return $this->packageRepo->delete($id);
	}

	public function toggle($id)
	{

		$package = Package::findOrFail($id);

		if ($package->isActive == 1) {
			$package->update([
				'isActive' => 0
			]);
		}else{
			$package->update([
				'isActive' => 1
			]);
		}

		return back();
	}


	public function store(){

		if (!Gate::allows('package_create')) {
            return response()->json(['errors' => [
                'name' => 'sorry you don`t have permission'
            ]],401);
        }
		$this->validate(request(),[
    		'name' => 'required|min:2|max:256|string|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/',
            'price' => 'required',
        ]);
        return $this->packageRepo->new(request()->all());
	}

	public function update($id)
	{

		if (!Gate::allows('package_edit')) {
            return abort(401);
        }

		$this->validate(request(),[
			'name' => 'required|string|min:2|max:256',
			'cost' => 'required|numeric',
			'description' => 'string|nullable'
		]);


		Package::where('id',$id)->update([
			'name' => request('name'),
			'cost' => request('cost'),
			'description' => request('description')
		]);

		return back();
	}


	public function customers($package_id)
	{

		if (request()->ajax()) {

			$query = DB::table('customers')
	            ->join('users','customers.user_id','users.id')
	            ->join('customer_package','customer_package.customer_id','customers.id')
	            ->where('customer_package.package_id',$package_id)
	            ->select('users.*');

        	return Datatables::of($query)->make();

    	}

		$package = Package::findOrFail($package_id);

		return view('admin.packages.customers',compact('package'));
	}
}
