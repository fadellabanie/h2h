<?php

namespace App\Http\Controllers\Admin\Gift;

use App\Domains\Customer\repository\CustomerRepository;
use App\Domains\Gift\entity\Gift;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class GiftsCustomersController extends Controller
{


    public function index($giftId)
    {

        $gift = Gift::findOrFail($giftId);

        return view('admin.gifts.customers',compact('gift'));
    }


    public function show($id)
    {
        $query = DB::table('customers')
            ->join('users','customers.user_id','users.id')
            ->join('customer_gift','customer_gift.customer_id','customers.id')
            ->where('customer_gift.gift_id',$id)
            ->select('users.*');

        return Datatables::of($query)->make();
    }

    public function delete(CustomerRepository $repo, $id,$giftId)
    {
        $customer = $repo->user($id);

        $customer->gifts()->detach($giftId);
    }
}
