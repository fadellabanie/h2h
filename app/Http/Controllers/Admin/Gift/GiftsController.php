<?php

namespace App\Http\Controllers\Admin\Gift;

use App\Domains\Gift\entity\Gift;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class GiftsController extends Controller
{



    public function index()
    {

        if (!Gate::allows('gift_access')) {
            return abort(401);
        }

    	if (request()->ajax()) {
    		$gifts = Gift::select('gifts.*');
    		return Datatables::eloquent($gifts)->make(true);
    	}

    	return view('admin.gifts.index');
    }


    public function store()
    {

        if (!Gate::allows('gift_create')) {
            return response()->json(['errors' => [
                'name' => 'sorry you don`t have permission'
            ]],401);
        }

    	$this->validate(request(),[
    		'name' => 'required|string|min:2|max:256|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/',
            'description' => 'nullable|string',
    		'from' => 'required|date_format:Y-m-d',
            'to' => 'required|date_format:Y-m-d',
            'points' => 'required|numeric',
            'code' => 'required|unique:gifts,code'
    	]);

    	return Gift::create(request()->all());

    }


    public function show($id)
    {

        if (!Gate::allows('gift_view')) {
            return abort(401);
        }

    	$gift = Gift::findOrFail($id);

    	return view('admin.gifts.show',compact('gift'));
    }


    public function update($id)
    {

        if (!Gate::allows('gift_edit')) {
            return abort(401);
        }

        $this->validate(request(),[
            'name' => 'required|string|min:2|max:256|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/',
            'from' => 'required|date_format:Y-m-d',
            'to' => 'required|date_format:Y-m-d',
            'points' => 'required|numeric',
            'code' => 'required|unique:gifts,code,'.$id,
            'description' => 'nullable|string',
        ]);


        Gift::where('id',$id)->update([
            'name' => request('name'),
            'from' => request('from'),
            'to' => request('to'),
            'points' => request('points'),
            'code' => request('code'),
            'description' => request('description'),
        ]);

        return back();

    }


    public function delete($id)
    {

        if (!Gate::allows('gift_delete')) {
            return abort(401);
        }

    	$gift = Gift::findOrFail($id);

    	if ($gift->customers()->exists()) {

    		return response()->json(['error' => 'sorry can`t delete gift']);
    	}

    	$gift->delete();
    }


    public function toggle($gitId)
    {
        $gift = Gift::findOrFail($gitId);

        if ($gift->isActive == 1){
            $gift->update(['isActive' => 0]);
            return back();
        }

        $gift->update(['isActive' => 1]);

        return back();
    }
}
