<?php

namespace App\Http\Controllers\Admin\Orders;

use App\Domains\Order\entity\OrderDelivery;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;


class OrderDeliveriesController extends Controller
{
    
    public function index($driver_id)
    {
    	
    	$q = OrderDelivery::with('driver','order.customer.user')
    		->where('driver_id',$driver_id)
    		->select('order_deliveries.*');

    	return Datatables::eloquent($q)
			->filter(function($query){

				if (request('orderNo') != '') {
                    $query->where('orders.uuId', '=', request('orderNo'));
                }

                if (request('customer_id') != '') {
                    $query->whereHas('order.customer',function($q){
                    	$q->where('customers.id',request('customer_id'));
                    });
                }

                if (request('status_id') != '') {

                	if (request('status_id') == 1) {
                    	$query->whereNotNull('order_deliveries.accepted_at');
                	}                	
                	if (request('status_id') == 2) {
                    	$query->whereNotNull('order_deliveries.picked_at');
                	}                	
                	if (request('status_id') == 3) {
                    	$query->whereNotNull('order_deliveries.dropped_at');
                	}                	
                	if (request('status_id') == 4) {
                    	$query->whereNotNull('order_deliveries.outDelivery_at');
                	}                	
                	if (request('status_id') == 5) {
                    	$query->whereNotNull('order_deliveries.delivered_at');
                	}
                }
			})
			->make(true);

    }
}
