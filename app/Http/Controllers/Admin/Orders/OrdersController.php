<?php

namespace App\Http\Controllers\Admin\Orders;

use App\Domains\Driver\repository\DriverRepository;
use App\Domains\Fcm\OutOfDeliveryMessage;
use App\Domains\Order\entity\Order;
use App\Domains\Order\entity\OrderStatus;
use App\Domains\User\entity\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class OrdersController extends Controller
{


		public function index()
	{

        if (!Gate::allows('order_access')) {
            return abort(401);
        }

		if (request()->ajax()) {

			$query = Order::with('customer.user','delivery.driver.user')->select('orders.*')
				->when (request('orderNo') != '', function ($query) {
				$query->where('orders.uuId', '=', request('orderNo'));
				})
				->when (request('customer_id') != '', function ($query) {
					$query->whereHas('customer.user',function($q){
						$q->where('customers.id',request('customer_id'));
					});
				})
				->when (request('from') != '', function ($query) {
					$query->whereDate('orders.created_at', '>=', request('from'));
				})
				->when (request('to') != '', function ($query) {
					$query->whereDate('orders.created_at', '<=', request('to'));
				})
				->when (request('status_id') != '', function ($query) {
					$query->where('orders.status_id', '=', request('status_id'));
				})
				->when (request('driver_id') != '', function ($query) {
					$query->whereHas('delivery.driver',function($q){
						$q->where('order_deliveries.driver_id',request('driver_id'));
					});
				});

			return Datatables::eloquent($query)
//			->filter(function($query){
//
//				if (request('orderNo') != '') {
//                    $query->where('orders.uuId', '=', request('orderNo'));
//                }
//
//                if (request('customer_id') != '') {
//                    $query->whereHas('customer.user',function($q){
//                    	$q->where('customers.id',request('customer_id'));
//                    });
//                }
//
//                if (request('from') != '') {
//                    $query->whereDate('orders.created_at', '>=', request('from'));
//                }
//
//                if (request('to') != '') {
//                    $query->whereDate('orders.created_at', '<=', request('to'));
//                }
//
//                if (request('status_id') != '') {
//                    $query->where('orders.status_id', '=', request('status_id'));
//                }
//
//                if (request('driver_id') != '') {
//                    $query->whereHas('delivery.driver',function($q){
//                    	$q->where('order_deliveries.driver_id',request('driver_id'));
//                    });
//                }
//
//			})
			->make(true);
		}

		$drivers = User::where('user_group_id',2)->get(['name','id']);

		return view('admin.orders.index',compact('drivers'));
	}


	public function show($order_id)
	{

		if (!Gate::allows('order_view')) {
            return abort(401);
        }

		$order = Order::where('id',$order_id)->with('customer.user','delivery.driver.user','address','items.type','items.color','items.size')->first();

		return view('admin.orders.show',compact('order'));
	}

	public function delete($order_id)
	{

		if (!Gate::allows('order_delete')) {
            return abort(401);
        }
        
		$order = Order::findOrFail($order_id);

		if ($order->invoice()->exists()) {
			$order->invoice()->delete();
		}

		if ($order->address()->exists()) {
			$order->address()->delete();
		}

		if ($order->items()->exists()) {
			$order->items()->delete();
		}

		if ($order->delivery()->exists()) {
			$order->delivery()->delete();
		}

		$order->delete();

		return response()->json(['success' => 'order deleted success']);
	}


	public function calendar()
	{
		if (request()->ajax()) {

			// $start = (!empty($_GET["start"])) ? ($_GET["start"]) : ('');
   //       	$end = (!empty($_GET["end"])) ? ($_GET["end"]) : ('');

   //       	$data = Event::whereDate('start', '>=', $start)->whereDate('end',   '<=', $end)->get(['id','title','start', 'end']);
   //       	return Response::json($data);
		}
		return view('admin.orders.calendar');
	}

	public function calendarData()
	{

		$start = Carbon::parse(request('start'))->format('Y-m-d');
		$end = Carbon::parse(request('end'))->format('Y-m-d');

		$orders = Order::where('status_id',OrderStatus::PICKEDUP)
		->whereHas('delivery',function($q){
			$q->whereNotNull('picked_at')
				->whereNotNull('dropped_at');
		})
		->whereDate('created_at','>=',$start)
		->whereDate('created_at','<=',$end)
		->with('delivery','items.service')
		->get();

		$data = $orders->transform(function($order){
			return [
				'title' => $order->uuId,
				'start' => Carbon::parse($order->delivery->picked_at)->format('Y-m-d H:i:s'),
				'end' => $this->addEndTime($order),
				'className' => $order->is_urgent == 0 ? 'fc-event-info' : 'fc-event-danger',
				'url' => 'dashboard/orders/'.$order->id
			];
		});

		return response()->json($data);
	}


	public function addEndTime($order)
	{

		if ($order->is_urgent == 1) {
			//$hours = Carbon::parse($order->service->delivery_urgent_time);
			$minutes = $order->items->map(function($item){
				return $item->service->delivery_urgent_time;
			})->sum();

			return Carbon::parse($order->delivery->picked_at)
				->addMinutes($minutes)
				->format('Y-m-d H:i:s');
		}

		//$hours = Carbon::parse($order->service->delivery_time);
		$minutes = $order->items->map(function($item){
			return $item->service->delivery_time;
		})->sum();

		//return $hours;
		return Carbon::parse($order->delivery->picked_at)
				->addMinutes($minutes)
				->format('Y-m-d H:i:s');
	}


	public function charge($orderUuid)
	{
		$order = Order::where('uuId',$orderUuid)
		->whereHas('delivery',function($q){
			$q->whereNull('outDelivery_at')
				->whereNull('delivered_at')
				->whereNotNull('dropped_at')
				->whereNotNull('picked_at')
				->whereNotNull('accepted_at');
		})
		->first();

		if (!$order) {
			return back();
		}



		$order->update([
			'status_id' => OrderStatus::OUTOFDELIVERY
		]);

		$order->delivery()->update(['outDelivery_at' => Carbon::now()]);

		$customer_id = $order->customer_id;

		$user = User::whereHas('customer',function($q) use($customer_id){
            $q->where('customers.id',$customer_id);
        })->first();

        $message = new OutOfDeliveryMessage($user,$order->id);

        $message->sendMessage();


		return back();
	}
}
