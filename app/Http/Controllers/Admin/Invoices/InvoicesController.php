<?php

namespace App\Http\Controllers\Admin\Invoices;

use App\Domains\Invoice\entity\Invoice;
use App\Domains\Invoice\repository\InvoiceRepository;
use App\Domains\Setting\entity\Setting;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class InvoicesController extends Controller
{

    private $invoiceRepo;

    public function __construct(InvoiceRepository $invoiceRepo)
    {
        $this->invoiceRepo = $invoiceRepo;
    }


    public function index()
    {


        if (!Gate::allows('invoice_access')) {
            return abort(401);
        }

       	if (request()->ajax()) {

    		$invoices = DB::table('invoices')
    		->join('orders','invoices.order_id','orders.id')
    		->join('customers','orders.customer_id','customers.id')
            ->join('users','customers.user_id','users.id')
            ->join('invoice_statuses','invoices.invoice_status_id','invoice_statuses.id')
    		->select('invoices.*','invoices.id as inId','orders.*','users.name as user','invoice_statuses.*');

        	return Datatables::of($invoices)
            ->filter(function ($query){

                if (request('invoiceno') != '') {
                    $query->where('invoices.invoiceno', '=', request('invoiceno'));
                }

                if (request('orderNo') != '') {
                    $query->where('orders.uuId', '=', request('orderNo'));
                }

                if (request('customer_id') != '') {
                    $query->where('orders.customer_id', '=', request('customer_id'));
                }

                if (request('from') != '') {
                    $query->whereDate('invoices.created_at', '>=', request('from'));
                }

                if (request('to') != '') {
                    $query->whereDate('invoices.created_at', '<=', request('to'));
                }

                if (request('status_id') != '') {
                    $query->where('invoices.invoice_status_id', '=', request('status_id'));
                }

            })
            ->make(true);
    	}

    	return view('admin.invoices.index');
    }


    public function show($invoiceId)
    {


        if (!Gate::allows('invoice_view')) {
            return abort(401);
        }
        
        $invoice = $this->invoiceRepo->find($invoiceId,['order.items.service','order.items.type','order.facility','order.address','order.customer.user']);

        $setting = Setting::first();

        return view('admin.invoices.show',compact('invoice','setting'));
    }

    public function download($invoice_id)
    {
        $invoice = Invoice::findOrFail($invoice_id);
        return response()->download(storage_path('/app/invoices/'.$invoice->invoiceno.'.pdf','local'));
    }
}
