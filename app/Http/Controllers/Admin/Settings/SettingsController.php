<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Domains\Setting\entity\Setting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingsController extends Controller
{


    public function index()
    {

    	$setting = Setting::first();
    	return view('admin.settings.index',compact('setting'));
    }


    public function store()
    {

    	$this->validate(request(),[
    		'logo' => 'nullable|mimes:jpeg,jpg,png|max:10000',
    		'fixed_point' => 'nullable|numeric',
            'tax' => 'nullable|numeric',
            'expire_days' => 'nullable|numeric',
    	]);

    	$setting = Setting::first();

    	if (!$setting) {

    		$setting = Setting::create([
    			'fixed_point' => request('fixed_point') == "" ? null : request('fixed_point'),
                'logo' => request()->hasFile('logo') ?  request('logo')->store('settings','uploads') : null,
                'tax' => request('tax') == "" ? null : request('tax'),
                'address' => request('address') == "" ? null : request('address'),
                'expire_days' => request('expire_days') == "" ? null : request('expire_days'),
    		]);

    		return back();
    	}

        $setting->fixed_point = request('fixed_point') == "" ? null : request('fixed_point');
        $setting->tax = request('tax') == "" ? null : request('tax');
        $setting->address = request('address') == "" ? null : request('address');
        $setting->expire_days = request('expire_days') == "" ? null : request('expire_days');

        if ($setting->logo != null && request()->hasFile('logo')) {
            unlink(public_path('uploads/'.$setting->logo));
            $setting->logo = request('logo')->store('settings','uploads');
        }


        $setting->save();


    	return back();
    }


    public function storePage()
    {


        $this->validate(request(),[
            'about' => 'nullable',
            'terms' => 'nullable',
        ]);

        $setting = Setting::first();

        if (!$setting) {
            Setting::create(['about' => request('about'),'terms' => request('terms')]);

            return back();
        }

        $setting->update([
            'about' => request('about'),
            'terms' => request('terms'),
        ]);

        return back();
    }
}
