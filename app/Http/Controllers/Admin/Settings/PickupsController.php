<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Domains\Setting\entity\PickupTime;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DataTables;
use Illuminate\Http\Request;

class PickupsController extends Controller
{
    

	public function index()
	{
		$times = PickupTime::select(['id','from','to']);
		return Datatables::eloquent($times)->make(true);
	}


	public function store()
	{
		$this->validate(request(),[
			'from' => 'required',
			'to' => 'required',
		]);

		PickupTime::create(['from' => request('from'),'to' => request('to')]);

		return;
	}

	public function delete($id)
	{

		$time = PickupTime::where('id',$id)->first();

		if ($time->orders()->exists()) {

			return response()->json(['errors' => 'sorry can`t delete'],400);
		}

		$time->delete();
		
		return response()->json(['success' => 'deleted success']);
	}
}
