<?php

namespace App\Http\Controllers\Admin\Point;

use App\Domains\Point\entity\Point;
use App\Domains\Point\repository\PointRepository;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PointController extends Controller
{


    public function index()
    {

        if (!Gate::allows('point_access')) {
            return abort(401);
        }

    	if (request()->ajax()) {
            $q = Point::with('customer.user','order')->select('points.*');
    		return Datatables::eloquent($q)->make(true);
    	}

    	return view('admin.points.index');
    }
    

    public function delete($id)
    {

        if (!Gate::allows('point_delete')) {
            return abort(401);
        }
        
        return Point::where('id',$id)->delete();
    }
}
