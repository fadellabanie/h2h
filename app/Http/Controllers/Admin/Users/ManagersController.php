<?php

namespace App\Http\Controllers\Admin\Users;

use App\Domains\User\contract\ManagerRepositoryInterface;
use App\Domains\User\entity\User;
use App\Domains\User\service\ManagerService;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddNewManagerRequest;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Gate;

class ManagersController extends Controller
{

	private $managerService;

	public function __construct(ManagerRepositoryInterface $managerRepo)
	{
		$this->managerService = $managerRepo;
	}

    


    public function index()
    {

        if (!Gate::allows('user_access')) {
            return abort(401);
        }

    	if (request()->ajax()) {

    		return Datatables::eloquent($this->managerService->select(['id','email','name']))->make(true);
    	}

        return view('admin.users.index');
    }


    public function store(AddNewManagerRequest $request)
    {
    	$request->validated();
        $data = request()->all();
        $data['user_group_id'] = 1;

        $user = $this->managerService->store($data);

        $user->assignRole('manager');
        $user->givePermissionTo($request->permissions);

        return redirect()->route('managers.index');
    }

    public function show($id)
    {
    	return $this->managerService->findManager($id);
    }

    public function update($id)
    {	

    	$this->validate(request(),[
    		'name' => 'required|string|min:2|max:255|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/',
            'email' => 'required|email|ends_with:yahoo.com,gmail.com,hotmail.com|unique:users,email,'.$id,
            'password' => 'present|nullable|confirmed|min:5',
            'phone' => 'present|nullable|unique:users,phone,'.$id
    	]);

    	$this->managerService->update(request()->except('_token','_method','permissions','password_confirmation'),$id);

        $user = User::findOrFail($id);

        $user->syncPermissions(request('permissions'));


        if (!request()->ajax()) {
            Session::flash('success', 'updated success');
            return redirect()->route('managers.index');
        }


    }

    public function delete($id)
    {
    	return $this->managerService->delete($id);
    }

    public function toggleActivation($id)
    {
        $this->managerService->toggleActivation($id);
    }


    public function create()
    {

        if (!Gate::allows('user_create')) {
            return abort(401);
        }
        $permissions = \Spatie\Permission\Models\Permission::all();

        return view('admin.users.create',compact('permissions'));
    }

    public function edit($user_id)
    {

        if (!Gate::allows('user_edit')) {
            return abort(401);
        }
        $user = User::findOrFail($user_id);

        $permissions = \Spatie\Permission\Models\Permission::all();

        return view('admin.users.edit',compact('user','permissions'));
    }
}
