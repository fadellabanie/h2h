<?php

namespace App\Http\Controllers\Admin\serviceFacilities;

use App\Domains\Facility\entity\Facility;
use App\Domains\Facility\repository\FacilityRepository;
use App\Domains\Service\repository\ServiceRepository;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;

class serviceFacilitiesController extends Controller
{

	private $serviceRepo;
	private $facility;

	public function __construct(ServiceRepository $serviceRepo,FacilityRepository $facility)
	{
		$this->serviceRepo = $serviceRepo;
		$this->facility = $facility;
	}

    
	public function index($serviceId)
	{

		$q = $this->facility->getModel()
		->join('facility_service','facilities.id','facility_service.facility_id')
		->where('facility_service.service_id',$serviceId)
		->select('facilities.*','facility_service.amount','facility_service.isPercentage');

		return Datatables::eloquent($q)->make(true);
	}

	public function store($serviceId)
	{
		$this->validate(request(),[
			'facility_id' => 'required|numeric|exists:facilities,id',
			'amount' => 'required|numeric',
			'isPercentage' => 'nullable|boolean',
		]);

		$this->serviceRepo->findService($serviceId)
			->facilities()
			->detach(request('facility_id'));		

		$this->serviceRepo->findService($serviceId)
			->facilities()
			->attach([request('facility_id') => [ 
				'amount' => request('amount'),
				'isPercentage' => request()->has('isPercentage') ? 1 : 0 ,
			] ] );

	}


	public function delete($serviceId,$facility_id)
	{
		$this->serviceRepo->findService($serviceId)
			->facilities()
			->detach($facility_id);	
	}

	public function find($serviceId)
	{
		$term = trim(request('q'));

		if (empty($term)) {
            return \Response::json([]);
        }

        $types = Facility::whereDoesntHave('services',function($q) use ($serviceId){
        	$q->where('services.id', '=', $serviceId);
        })
        ->where('name', 'LIKE' , '%' .request('q'). '%')
        ->whereNotNull('parent_id')
        ->limit(50)
        ->get();

        $formatted_types = [];

        foreach ($types as $type) {
            $formatted_types[] = ['id' => $type->id, 'text' => $type->name];
        }

        return \Response::json($formatted_types);
	}

}
