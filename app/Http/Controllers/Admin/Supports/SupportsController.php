<?php

namespace App\Http\Controllers\Admin\Supports;

use App\Domains\Customer\entity\Customer;
use App\Domains\Support\entity\Support;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;

class SupportsController extends Controller
{


    public function index()
    {

        if (!Gate::allows('support_access')) {
            return abort(401);
        }

        if (request()->ajax()) {

            $query = Support::with('customer.user')->select('supports.*');
            return Datatables::eloquent($query)->make(true);
        }
        return view('admin.supports.index');
    }

    public function show($id)
    {

        if (!Gate::allows('support_view')) {
            return abort(401);
        }

        $support = Support::where('id',$id)->with('customer.user')->firstOrFail();

        return view('admin.supports.show',compact('support'));
    }


    public function reply($id)
    {
        $this->validate(request(),[
            'reply' => 'required|string'
        ]);

        $support = Support::where('id',$id)->with('customer.user')->firstOrFail();

        $support->update(['reply' => request('reply')]);

        $customer = Customer::findOrFail($support->customer_id);

        // email data
        $email_data = array(
            'name' => $customer->user->name,
            'email' => $customer->user->email,
            'content' => request('reply')
        );

        // send email with the template
        Mail::send('reply_email', $email_data, function ($message) use ($email_data,$support) {
            $message->to($email_data['email'], $email_data['name'])
                ->subject('Reply To '.$support->title)
                ->from('info@h2h.theportal.agency', 'Hand 2 Hanger');
        });

        return back();

        //TODO SEND MAIL

    }

    public function delete($id)
    {
        if (!Gate::allows('support_delete')) {
            return response()->json(['errors' => [
                'name' => 'sorry you don`t have permission'
            ]],401);

        }

        Support::where('id',$id)->delete();
    }
}
