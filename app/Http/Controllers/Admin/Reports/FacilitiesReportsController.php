<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Domains\Facility\entity\Facility;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
class FacilitiesReportsController extends Controller
{

	public function index()
	{


        if (!Gate::allows('report_access')) {
            return abort(401);
        }
        
		if (request()->wantsJson()){
			//return DB::raw('JSON_CONTAINS(order_items.facilities->"$[0].id')->get();
            $query = DB::table('facilities')
                ->join('orders','facilities.id','orders.facility_id')
                ->select('facilities.id','facilities.name','orders.facility_amount')
                ->addSelect(DB::raw('count(orders.facility_id) as ordersCount'))
                ->groupBy('facilities.id','facilities.name','orders.facility_amount');
            return Datatables::of($query)
                ->filter(function ($query){
                    if (request('facility_id') != '') {
                        $query->where('orders.facility_id', '=', request('facility_id'));
                    }

                    if (request('from') != '') {
                        $query->whereDate('orders.created_at', '>=', request('from'));
                    }
                    if (request('to') != '') {
                        $query->whereDate('orders.created_at', '<=', request('to'));
                    }
                })
                ->make(true);
        }

        $facilities = Facility::get(['name','id']);

		return view('admin.reports.facilities.index',compact('facilities'));
	}
}
