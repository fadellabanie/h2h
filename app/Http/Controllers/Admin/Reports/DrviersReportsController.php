<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Domains\City\entity\City;
use App\Domains\Driver\entity\Driver;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class DrviersReportsController extends Controller
{
    

	public function index()
	{


        if (!Gate::allows('report_access')) {
            return abort(401);
        }
        
		if (request()->wantsJson()){

			$query = DB::table('orders')
                ->join('order_deliveries','orders.id','order_deliveries.order_id')
				->join('drivers','order_deliveries.driver_id','drivers.id')
				->join('users','drivers.user_id','users.id')
                ->join('invoices','orders.id','invoices.order_id')
                ->join('addresses','orders.address_id','addresses.id')
                ->join('cities','addresses.city_id','cities.id')
                ->select('orders.uuId as orderNo','cities.name as city','invoices.delivery_cost','users.name as user');

            return Datatables::of($query)
                ->filter(function ($query){

                    if (request('driver_id') != '') {
                        $query->where('drivers.id', '=', request('driver_id'));
                    }                    

                    if (request('city_id') != '') {
                        $query->where('cities.id', '=', request('city_id'));
                    }

                    if (request('from') != '') {
                        $query->whereDate('orders.created_at', '>=', request('from'));
                    }
                    if (request('to') != '') {
                        $query->whereDate('orders.created_at', '<=', request('to'));
                    }
                })
                ->make(true);
		}

		$drivers = Driver::with('user')->get();
		$cities = City::get(['id','name']);
		return view('admin.reports.drivers.index',compact('drivers','cities'));
	}

}





