<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Domains\Service\entity\Service;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class ServicesReportsController extends Controller
{

    public function index()
    {

        if (!Gate::allows('report_access')) {
            return abort(401);
        }

        if (request()->wantsJson()){

            $query = DB::table('services')
                ->join('order_items','services.id','order_items.service_id')
                ->select('services.id','services.name')
                ->addSelect(DB::raw('count(order_items.service_id) as itemsCount'))
                ->addSelect(DB::raw('sum(order_items.amount) as itemsAmount'))
                ->groupBy('services.id','services.name');
            return Datatables::of($query)
                ->filter(function ($query){
                    if (request('service_id') != '') {
                        $query->where('services.id', '=', request('service_id'));
                    }

                    if (request('from') != '') {
                        $query->whereDate('order_items.created_at', '>=', request('from'));
                    }
                    if (request('to') != '') {
                        $query->whereDate('order_items.created_at', '<=', request('to'));
                    }
                })
                ->make(true);
        }

        $services = Service::get(['name','id']);

        return view('admin.reports.services.index',compact('services'));
    }
}
