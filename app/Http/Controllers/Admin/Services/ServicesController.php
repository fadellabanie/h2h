<?php

namespace App\Http\Controllers\Admin\Services;

use App\Domains\Order\entity\Order;
use App\Domains\Order\entity\OrderItem;
use App\Domains\Service\repository\ServiceRepository;
use App\Domains\Type\repository\TypeRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use App\Domains\Type\entity\Type;
use App\Domains\Service\entity\Service;
use App\Domains\Category\entity\Category;

class ServicesController extends Controller
{

	private $serviceRepo;
	private $typeRepo;

	public function __construct(ServiceRepository $serviceRepo,TypeRepository $type)
	{
		$this->serviceRepo = $serviceRepo;
		$this->typeRepo = $type;
	}




	public function index()
	{

        if (!Gate::allows('service_access')) {
            return abort(401);
        }

		$services = $this->serviceRepo->all();

		foreach ($services as &$service){
		    $all = OrderItem::where('service_id',$service->id)->count();
            $total = OrderItem::count();
            $service->percentge = $all == 0 ? 0 : $all / $total * 100;
        }

		return view('admin.services.index',compact('services'));
	}


	public function show($id)
	{

		// dd('dsadas');

		if (!Gate::allows('service_view')) {
            return abort(401);
        }
		$service = $this->serviceRepo->findService($id);
		
		if($service->parent_id != null){
			if($service->parent_id == 1 || $service->parent_id == 2 || $service->parent_id == 3 || $service->parent_id == 4){
				return redirect()->route('services.show', $service->parent_id);
			}else{
				return redirect()->route('services.index');
			}
		}

        $customers = DB::table('customers')
                        ->join('orders','customers.id','orders.customer_id')
                        ->join('order_items','orders.id','order_items.order_id')
                        ->where('order_items.service_id',$id)
                        ->count();

		return view('admin.services.show',compact('service','customers'));
	}


	public function edit($id,$serviceid)
	{

        $serviceType = Type::with('services', 'categories')->find($id);

		$servicePrice = $serviceType->services->where('id', $serviceid)->first();
		
		$price = 0;
		$servicename = '';

		if ($servicePrice) {
			$price = $servicePrice->pivot->price;
			$servicename = $servicePrice->name;
		}
		
		$selectedCategoriesId = $serviceType->categories->pluck('id')->toArray();

		return view('admin.services.edit-type',compact('serviceType', 'selectedCategoriesId', 'price', 'serviceid', 'servicename'));
	}


	public function updateType(Request $request, $serviceid)
	{
		
		$this->validate($request,[
			'type_id' => 'required|numeric|exists:types,id',
			'price' => 'required|numeric|digits_between:1,6',
            'category_id' => 'required|array'
		]);
			
		// dd($request->all());
		//$service = $this->serviceRepo->findService($serviceId);

		foreach ($request->category_id as $id){

		    $row = DB::table('service_type')
				->where('category_id',$id)
                ->where('type_id',$request->type_id)
                ->where('service_id',$serviceid)
                ->get();

			// dd($row);

		    if (count($row) > 0){
				// echo 'update: '. $id .'<br><br>';
				$testDb = DB::table('service_type')->where('id',$row->first()->id)
				->update([
					'type_id' => $request->type_id,
					'service_id' => $serviceid,
					'category_id' => $id,
					'price' => $request->price
				]);

				// dd($testDb);
            } else {
				// echo 'insert: '. $id .'<br><br>';
				DB::table('service_type')->insert([
					'type_id' => $request->type_id,
					'service_id' => $serviceid,
					'category_id' => $id,
					'price' => $request->price
				]);
			}

		}

		// dd('');
		return redirect()->route('services.show', $serviceid);
		
	}

	public function toggleActivation($id)
	{

		$this->serviceRepo->toggleActivation($id);

		return response()->json(['success' => 'statues changed success']);
	}


	public function update($id)
	{

		$this->validate(request(),[
			'delivery_time' => 'nullable|numeric',
			'delivery_urgent_time' => 'nullable|numeric',
		]);

		$data['delivery_time'] = request('delivery_time') != '' ? request('delivery_time'): null;

		$data['delivery_urgent_time'] = request('urgent_time') != '' ? request('urgent_time'): null;

		$this->serviceRepo->update($data,$id);

		return back();
	}
}
