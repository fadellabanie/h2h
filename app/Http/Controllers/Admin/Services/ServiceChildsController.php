<?php

namespace App\Http\Controllers\Admin\Services;

use App\Domains\Service\repository\ServiceRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;

class ServiceChildsController extends Controller
{


	private $serviceRepo;

	public function __construct(ServiceRepository $serviceRepo)
	{
		$this->serviceRepo = $serviceRepo;
	}

    public function index($serviceId)
    {
    	$data = $this->serviceRepo->selectChilds($serviceId);

		return Datatables::eloquent($data)->make(true);
    }


    public function store($serviceId)
    {
    	$this->validate(request(),[
    		'name' => 'required|min:2|max:256|string|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/',
    	]);

    	return $this->serviceRepo->createChild(request()->all(),$serviceId);
    }

    public function update($id)
    {
    	$this->validate(request(),[
    		'name' => 'required|min:2|max:256|string|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/',
    	]);

    	return $this->serviceRepo->update(request()->all(),$id);
    }


    public function delete($serviceId)
    {
    	return $this->serviceRepo->delete($serviceId);
    }


}
