<?php

namespace App\Http\Controllers\Admin\Categories;

use App\Domains\Category\entity\Category;
use App\Domains\Category\repository\CategoryRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Gate;

class CategoriesController extends Controller
{

	private $catRepo;

	public function __construct(CategoryRepository $catRepo)
	{
		$this->catRepo = $catRepo;
	}

    public function index()
    {

        if (!Gate::allows('category_access')) {
            return abort(401);
        }

    	if (request()->ajax()) {
    		return Datatables::eloquent($this->catRepo->select(['id','name']))->make(true);
    	}

    	return view('admin.categories.index');
    }

    public function all()
    {
    	return $this->catRepo->all();
    }

    public function store()
    {
        if (!Gate::allows('category_create')) {
            return response()->json(['errors' => [
                'name' => 'sorry you don`t have permission'
            ]],401);
        }

    	$this->validate(request(),[
    		'name' => 'required|string|min:2|max:255|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/',
    	]);

    	return $this->catRepo->store(request()->all());
    }

    public function show($id)
    {
    	return $this->catRepo->findCat($id);
    }

    public function update($id)
    {

        if (!Gate::allows('category_edit')) {
            return response()->json(['errors' => [
                'name' => 'sorry you don`t have permission'
            ]],401);
        }

    	$this->validate(request(),[
    		'name' => 'required|string|min:2|max:255|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/',
    	]);

    	return $this->catRepo->update(request()->all(),$id);
    }

    public function delete($id)
    {
        if (!Gate::allows('category_delete')) {
            return response()->json(['errors' => [
                'name' => 'sorry you don`t have permission'
            ]],401);
        }
    	return $this->catRepo->delete($id);
	}

	public function types($type_id)
    {
        return Category::whereHas('types',function ($q) use ($type_id){
            $q->where('type_id',$type_id);
        })->get();
    }

}
