<?php

namespace App\Http\Controllers\Admin\PromoCodes;

use App\Domains\Customer\repository\CustomerRepository;
use App\Domains\PromoCode\repository\PromoCodeRepository;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PromoCodeCustomersController extends Controller
{

	private $promoRepo;

	public function __construct(PromoCodeRepository $promoRepo)
	{
		$this->promoRepo = $promoRepo;
	}

    

    public function index($id)
    {
    	$promoCode = $this->promoRepo->find($id);

        return view('admin.promoCodes.customers',compact('promoCode'));
    }

    public function show($id)
    {
        $query = DB::table('customers')
            ->join('users','customers.user_id','users.id')
            ->join('customer_promo','customer_promo.customer_id','customers.id')
            ->where('customer_promo.promoCode_id',$id)
            ->select('users.*');

        return Datatables::of($query)->make();
    }

    public function delete(CustomerRepository $repo, $id,$promoID)
    {
        $customer = $repo->user($id);

        $customer->promoCodes()->detach($promoID);
    }
}
