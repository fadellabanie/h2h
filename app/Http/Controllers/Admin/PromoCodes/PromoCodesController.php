<?php

namespace App\Http\Controllers\Admin\PromoCodes;

use App\Domains\PromoCode\entity\PromoCode;
use App\Domains\PromoCode\repository\PromoCodeRepository;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PromoCodesController extends Controller
{

	private $promoRepo;

	public function __construct(PromoCodeRepository $promoRepo)
	{
		$this->promoRepo = $promoRepo;
	}

    public function index()
    {


        if (!Gate::allows('promocode_access')) {
            return abort(401);
        }

    	if (request()->ajax()) {
    		return Datatables::eloquent($this->promoRepo->select([]))->make(true);
    	}
    	return view('admin.promoCodes.index');
    }

    public function store()
    {

        if (!Gate::allows('promocode_create')) {
            return response()->json(['errors' => [
                'name' => 'sorry you can`t take this action'
            ]],401);
        }

        $this->validate(request(),[
            'name' => 'required|min:2|max:256|string',
            'from' => 'required|date_format:Y-m-d',
            'to' => 'required|date_format:Y-m-d',
            'amount' => 'required|numeric',
            'code' => 'required|unique:promo_codes,code'
            //'isPercentage' => 'present|boolean'
        ]);

        $this->promoRepo->store(request()->all());
    }

    public function show($id)
    {
        $promoCode = $this->promoRepo->find($id);
        return view('admin.promoCodes.show',compact('promoCode'));
    }


    public function update($id)
    {

        if (!Gate::allows('promocode_edit')) {
            return response()->json(['errors' => [
                'name' => 'sorry you can`t take this action'
            ]],401);
        }

        $this->validate(request(),[
            'name' => 'required|min:2|max:256|string',
            'from' => 'required|date_format:Y-m-d',
            'to' => 'required|date_format:Y-m-d',
            'amount' => 'required|numeric',
            'isPercentage' => 'nullable|boolean',
            'code' => 'required|unique:promo_codes,code,'.$id,
        ]);

        $this->promoRepo->update($id,request()->all());

        return back();
    }

    public function toggle($id)
    {
        $this->promoRepo->toggle($id);

        return back();
    }

    public function delete($id)
    {

        if (!Gate::allows('promocode_delete')) {
            return abort(401);
        }

        $code = PromoCode::where('id',$id)->firstOrFail();

        if ($code->orders()->exists()) {

            return response()->json(['errors' => 'Sorry this code cannot be deleted it has orders'],500);
        }

        if ($code->customers()->exists()) {
            return response()->json(['errors' => 'Sorry this code cannot be deleted it has customers'],500);
        }

        $code->delete();

    }
}
