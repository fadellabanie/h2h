<?php

namespace App\Http\Controllers\Admin\Regions;

use App\Domains\Region\repository\RegionRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Gate;


class RegionsController extends Controller
{

	private $regionRepo;

	public function __construct(RegionRepository $regionRepo)
	{
		$this->regionRepo = $regionRepo;
	}


    public function index()
    {

        if (!Gate::allows('region_access')) {
            return abort(401);
        }

    	if (request()->ajax()) {
    		return Datatables::eloquent($this->regionRepo->select(['city']))->make(true);
    	}

    	return view('admin.regions.index');
    }
    

    public function all()
    {
    	return $this->regionRepo->all();
    }

    public function store()
    {
        
        if (!Gate::allows('region_create')) {
            return response()->json(['errors' => [
                'name' => 'sorry you don`t have permission'
            ]],401);
        }

    	$this->validate(request(),[
    		'name' => 'required|min:2|max:256|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/',
    		'city_id' => 'required|numeric|exists:cities,id',
            'lat' => 'present|nullable',
            'lan' => 'present|nullable',
    	]);

    	$this->regionRepo->store(request()->all());
    }


    public function update($id)
    {

        if (!Gate::allows('region_edit')) {
            return response()->json(['errors' => [
                'name' => 'sorry you don`t have permission'
            ]],401);
        }

    	$this->validate(request(),[
    		'name' => 'required|min:2|max:256|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/',
    		'city_id' => 'required|numeric|exists:cities,id'
    	]);

    	return $this->regionRepo->update(request()->all(),$id);
    }

    public function delete($id)
    {
        
        if (!Gate::allows('region_delete')) {
            return response()->json(['errors' => [
                'name' => 'sorry you don`t have permission'
            ]],401);
        }

    	return $this->regionRepo->delete($id);
    }
}
