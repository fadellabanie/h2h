<?php

namespace App\Http\Controllers\Admin\Drivers;


use App\Domains\Driver\entity\Driver;
use App\Domains\Driver\service\Driver as DriverService;
use App\Domains\User\repository\ManagerRepository;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class DriversController extends Controller
{

	private $driver;

	public function __construct(DriverService $driver)
	{
		$this->driver = $driver;
	}

	public function index()
	{

        if (!Gate::allows('driver_access')) {
            return abort(401);
        }

		if (request()->ajax()) {
    		return Datatables::eloquent($this->driver->data(['user','status']))->make(true);
    	}

		return view('admin.drivers.index');
	}


    public function store(Request $request)
    {

        if (!Gate::allows('driver_create')) {
            return response()->json(['errors' => [
                'name' => 'sorry you can`t take this action'
            ]],401);
        }

        $this->validate($request, [
    		'name' => 'required|min:2|max:256|string',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:4|max:255',
            'address' => 'present|nullable',
            'region_id' => 'required|array',
            'region_id.*' => 'required|numeric|exists:regions,id',
            'license' =>  'present|nullable',
            'date_of_birth' =>  'present|nullable|date_format:Y-m-d',
            'phone' => 'present|nullable|unique:users,phone',
            'file' => 'nullable|file'
        ]);

        //$path = request()->file('file')->store('driver/files','local');

        //$data = request()->all();

        return $this->driver->new($request->all());
    }

    public function show($id)
    {
        if (!Gate::allows('driver_view')) {
            return abort(401);
        }
    	return view('admin.drivers.overview',compact('id'));
    }

    public function edit($id)
    {

        if (!Gate::allows('driver_edit')) {
            return abort(401);
        }
    	return view('admin.drivers.personal');
    }

    public function update($id)
    {

        if (!Gate::allows('driver_edit')) {
            return abort(401);
        }

        $this->validate(request(),[
            'region_id' => 'required|numeric|exists:regions,id',
            'license' =>  'present|nullable',
            'date_of_birth' =>  'present|nullable',
            'driver_image' =>  'nullable|mimes:jpeg,jpg,png|max:10000',
            'region_id' => 'required|array',
            'region_id.*' => 'required|numeric|exists:regions,id',
            'file' => 'nullable|file'
        ]);

        $this->driver->update($id,request()->except('_token','city_id','driver_image'));

        if (request()->hasFile('driver_image')) {

            $this->driver->driverImage($id,request('driver_image'));
        }

        if (request()->hasFile('file')) {
            $this->driver->driverFile($id,request('file'));
        }


        return back();

    }

    public function account($id)
    {
        if (!Gate::allows('driver_edit')) {
            return abort(401);
        }
        return view('admin.drivers.account');
    }

    public function delete($driver_id)
    {

        $driver = Driver::where('id',$driver_id)->firstOrFail();

        if ($driver->delivery()->exists()) {
            return response()->json(['errors' => 'sorry can`t delete driver'],500);
        }

        $driver->user()->delete();
        $driver->regions()->sync([]);
        $driver->delete();
        return ;
    }

    public function download($driver_id)
    {

        $driver = Driver::where('id',$driver_id)->first();

        return response()->download(storage_path('/app/'.$driver->file_url,'local'));
    }
}
