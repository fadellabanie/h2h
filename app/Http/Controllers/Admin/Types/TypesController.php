<?php

namespace App\Http\Controllers\Admin\Types;

use App\Domains\Type\entity\Type;
use App\Domains\Type\repository\TypeRepository;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class TypesController extends Controller
{

	private $typesRepo;

	public function __construct(TypeRepository $typesRepo)
	{
		$this->typesRepo = $typesRepo;
	}

  
	public function index()
	{
		
		if (!Gate::allows('type_access')) {
            return abort(401);
        }

       	if (request()->ajax()) {
    		return Datatables::eloquent($this->typesRepo->select(['categories']))
    		    ->addColumn('category', function ($type) {
                    return $type->categories->map(function($cat) {
                        return "<button class='btn btn-label-primary btn-sm'>$cat->name</button>";
                    })->implode(' ');
                })
            ->rawColumns(['category'])
    		->make(true);
    	}
		return view('admin.types.index');
	}

	public function store()
	{

		if (!Gate::allows('type_create')) {
            return response()->json(['errors' => [
                'name' => 'sorry you don`t have permission'
            ]],401);
        }

		$this->validate(request(),[
			'name' => 'required|string|min:2|max:256',
			'description' => 'present|nullable|string|min:2|max:255',
			'category_id' => 'required|array',
			'category_id.*' => 'required|numeric|exists:categories,id',
		]);

		$this->typesRepo->store(request()->all());

	}


	public function update($id)
	{

		if (!Gate::allows('type_edit')) {
            return response()->json(['errors' => [
                'name' => 'sorry you don`t have permission'
            ]],401);
        }

		$this->validate(request(),[
			'name' => 'required|min:2|max:256',
			'description' => 'present|nullable',
			'category_id' => 'required|array',
			'category_id.*' => 'required|numeric|exists:categories,id',
		]);

		$this->typesRepo->update(request()->all(),$id);

	}

	public function delete($id)
	{

		if (!Gate::allows('type_delete')) {
            return response()->json(['errors' => [
                'name' => 'sorry you don`t have permission'
            ]],401);
        }

		return $this->typesRepo->delete($id);
	}

}
