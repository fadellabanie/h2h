<?php

namespace App\Http\Controllers\Admin\Customer;

use App\Domains\Customer\repository\AddressRepository;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;

class CustomerAddressesController extends Controller
{


	private $addressRepo;

	public function __construct(AddressRepository $addressRepo)
	{
		$this->addressRepo = $addressRepo;
	}

    

    public function index($customer_id)
    {

    	if (request()->ajax()) {
            return Datatables::eloquent($this->addressRepo->select($customer_id))->make(true);
    	}
    }
}
