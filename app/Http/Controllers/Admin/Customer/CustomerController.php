<?php

namespace App\Http\Controllers\Admin\Customer;

use App\Domains\Customer\entity\Customer as model;
use App\Domains\Customer\repository\CustomerRepository;
use App\Domains\Customer\service\Customer;
use App\Domains\User\entity\User;

use App\Domains\Customer\entity\Customer as CustomerEntity;
use App\Domains\Order\entity\Order;
use App\Domains\User\repository\ManagerRepository;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;


class CustomerController extends Controller
{

    private $customer;

	public function __construct(Customer $customer)
	{
		$this->customer = $customer;
	}

	public function index()
	{

        if (!Gate::allows('customer_access')) {
            return abort(401);
        }


		if (request()->ajax()) {
            return Datatables::eloquent($this->customer->data(['user']))->make(true);
    	}
        // return Datatables::eloquent($this->customer->data(['user']))->make(true);
		return view('admin.customers.index');
	}


    public function store()
    {
        if (!Gate::allows('customer_create')) {
            return response()->json(['errors' => [
                'name' => 'sorry you don`t have permission'
            ]],401);
        }

        $this->validate(request(),[
    		'name' => 'required|min:2|max:256',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:4|max:255',
            'address' => 'present|nullable',
            'phone' => 'present|nullable|unique:users,phone',
            // 'package_id'=>'required'

        ]);
        // return request()->except(['day'])
        return $this->customer->new(request()->all());
    }

    public function show($id)
    {

        if (!Gate::allows('customer_view')) {
            return abort(401);
        }

        $customer = model::where('id',$id)->with('user')->first();

        $ordersCount = $customer->orders()->count();

        $total_profit = \DB::table('invoices')
                            ->join('orders','invoices.order_id','orders.id')
                            ->join('customers','orders.customer_id','customers.id')
                            ->where('invoices.invoice_status_id',3)
                            ->where('customers.id',$id)
                            ->sum('invoices.total');

    	return view('admin.customers.profile',compact('customer','ordersCount','total_profit'));
    }

    public function edit($id)
    {
    	return view('admin.drivers.personal');
    }

    public function editCustomer($id)
    {
        
        $customer = CustomerEntity::with('user')->findOrFail($id);

    	return view('admin.customers.edit', compact('customer'));
    }

    public function updateCustomer($id, Request $request)
    {
        
        $customer = CustomerEntity::with('user')->findOrFail($id);

        $user = User::find($customer->user->id);

        // dd($customer, $user);
        
        $this->validate($request,[
            'name' => 'required|min:2|max:256',
            'email' => 'required|email|unique:users,email,'.$customer->user->id,
            'address' => 'present|nullable',
            'phone' => 'present|nullable|unique:users,phone,'.$customer->user->id
            // 'package_id'=>'required'
            
        ]);

        // dd($user, $request->address);

            $user->name = $request->name;
            $user->email = $request->email;
            $user->address = $request->address;
            $user->phone = $request->phone;

            $user->save();


            // dd($this->customer->new(request()->all());

        return redirect()->route('customers.index');
    }

    public function find()
    {

        $term = trim(request('q'));

        if (empty($term)) {
            return \Response::json([]);
        }

        $customers = model::whereHas('user',function($q){
            $q->where('name', 'LIKE' , '%' .request('q'). '%');
        })
        ->limit(5)
        ->get();

        $formatted_customers = [];

        foreach ($customers as $customer) {
            $formatted_customers[] = ['id' => $customer->id, 'text' => $customer->user->name];
        }

        return \Response::json($formatted_customers);

    }

    public function update($id)
    {

        if (!Gate::allows('customer_edit')) {
            return abort(401);
        }
        $this->validate(request(),[
            'region_id' => 'required|numeric|exists:regions,id',
            'license' =>  'present|nullable|numeric',
            'date_of_birth' =>  'present|nullable',
            'driver_image' =>  'nullable|mimes:jpeg,jpg,png|max:10000',
            'region_id' => 'required|array',
            'region_id.*' => 'required|numeric|exists:regions,id',
        ]);

        $this->driver->update($id,request()->except('_token','city_id','driver_image'));

        if (request()->hasFile('driver_image')) {

            $this->driver->driverImage($id,request('driver_image'));
        }

        return back();

    }

    public function account($id)
    {
        return view('admin.drivers.account');
    }

    public function orders($customer_id)
    {
        $orders = Order::where('customer_id',$customer_id)->select('orders.*');

        if (request()->ajax()) {
            return Datatables::eloquent($orders)->make(true);
        }
    }

}
