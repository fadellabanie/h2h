<?php

namespace App\Http\Controllers\Admin\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DataTables;

class CustomerInvoicesController extends Controller
{
    


    public function index($customer_id)
    {
    	$invoices = \DB::table('invoices')
    				->join('orders','invoices.order_id','orders.id')
    				->join('customers','orders.customer_id','customers.id')
    				->where('customers.id',$customer_id)
    				->select('invoices.*');

    	return DataTables::of($invoices)->make(true);
    }
}
