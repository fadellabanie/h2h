<?php

namespace App\Http\Controllers\Admin\Customer;

use App\Domains\Point\entity\Point;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;

class CustomerPointsController extends Controller
{
    

    public function index($customer_id)
    {
    	$q = Point::with('customer.user','order')->select('points.*');
    	
    	return Datatables::eloquent($q)->make(true);
    }

}
