<?php

namespace App\Http\Controllers\Admin\Cities;

use App\Domains\City\repository\CityRepository;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class CitiesController extends Controller
{

	private $cityRepo;

	public function __construct(CityRepository $cityRepo)
	{
		$this->cityRepo = $cityRepo;
	}

    public function index()
    {
    	

        if (!Gate::allows('city_access')) {
            return abort(401);
        }


       	if (request()->ajax()) {
    		return Datatables::eloquent($this->cityRepo->select(['id','name','delivery_cost']))->make(true);
    	}

    	return view('admin.cities.index');
    }

    public function show($id)
    {

    	return $this->cityRepo->findById($id);		
    }

    public function store()
    {

        if (!Gate::allows('city_create')) {
            return response()->json(['errors' => [
                'name' => 'sorry you don`t have permission'
            ]],401);
        }

    	$this->validate(request(),[
    		'name' => 'required|string|min:2|max:256|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/',
            'delivery_cost' => 'required|numeric',
            
    	]);

    	$this->cityRepo->store(request()->all());
    }

    public function update($id)
    {

        if (!Gate::allows('city_edit')) {
            return response()->json(['errors' => [
                'name' => 'sorry you don`t have permission'
            ]],401);
        }

    	$this->validate(request(),[
    		'name' => 'required|string|min:2|max:256|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/',
            'delivery_cost' => 'required|numeric',
    	]);

    	$this->cityRepo->update(request()->all(),$id);
    }

    public function all()
    {
        return $this->cityRepo->all();    
    }

    public function delete($id)
    {

        if (!Gate::allows('city_delete')) {
            return response()->json(['errors' => [
                'name' => 'sorry you don`t have permission'
            ]],401);
        }
        
    	return $this->cityRepo->delete($id);
    }

    public function cityRegions($city_id)
    {
        return $this->cityRepo->regions($city_id);
    }
}