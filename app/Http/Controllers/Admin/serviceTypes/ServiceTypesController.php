<?php

namespace App\Http\Controllers\Admin\serviceTypes;

use App\Domains\Service\repository\ServiceRepository;
use App\Domains\Type\entity\Type;
use App\Domains\Type\repository\TypeRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\DB;

class ServiceTypesController extends Controller
{

	private $serviceRepo;
	
	private $typeRepo;

	public function __construct(ServiceRepository $serviceRepo,TypeRepository $type)
	{
		$this->serviceRepo = $serviceRepo;
		$this->typeRepo = $type;
	}

	public function index($service_id)
	{

		$query = $this->typeRepo->getModel()
		->join('service_type','types.id','service_type.type_id')
		->join('services','services.id','service_type.service_id')
		->join('categories','service_type.category_id','categories.id')
		->where(function($q) use ($service_id) {
			$q->where('service_type.service_id',$service_id)->orWhere('services.parent_id',$service_id);
		})
		//->with('categories')
		->select('types.*','service_type.price','services.name as service' ,'services.id as serviceId','categories.name as category','service_type.category_id');

		return Datatables::eloquent($query)
    		    ->addColumn('category', function ($type) {

                    return "<button class='btn btn-label-primary btn-sm'>$type->category</button>";

//                    return $type->categories->map(function($cat) {
//                        return "<button class='btn btn-label-primary btn-sm'>$cat->name</button>";
//                    })->implode(' ');
                })
            ->rawColumns(['category'])
    		->make(true);
	}

	public function store($serviceId)
	{
		$this->validate(request(),[
			'type_id' => 'required|numeric|exists:types,id',
			'price' => 'required|numeric|digits_between:1,6',
            'category_id' => 'required|array'
		]);

		$i = 0;

		//$service = $this->serviceRepo->findService($serviceId);

		foreach (request('category_id') as $id){

		    $row = DB::table('service_type')
                ->where('type_id',request('type_id'))
                ->where('service_id',$serviceId)
                ->where('category_id',$id)
                ->exists();

		    if ($row){
		        continue;
            }

			$i++;

		    DB::table('service_type')->insert([
		        'type_id' => request('type_id'),
		        'service_id' => $serviceId,
		        'category_id' => $id,
                'price' => request('price')
            ]);
			

//            $service->types()
//                ->attach([
//
//                    request('type_id') => [ 'price' => request('price')]
//                ] );
        }
//
//		$this->serviceRepo->findService($serviceId)
//			->types()
//			->detach(request('type_id'));
//
//		$this->serviceRepo->findService($serviceId)
//			->types()
//			->attach([request('type_id') => [ 'price' => request('price')] ] );

		if($i == 0 && count(request('category_id')) != 0){
			return response()->json(['errors' => 'can not add because all entries already exists']);
		}else{
			return response()->json(['success' => 'data inserted successfully']);
		}
	}
	
	public function delete($serviceId,$type_id,$cat_id)
	{

	    DB::table('service_type')
            ->where('type_id',$type_id)
            ->where('service_id',$serviceId)
            ->where('category_id',$cat_id)
            ->delete();
//		$this->serviceRepo->findService($serviceId)
//			->types()
//			->detach($type_id);
	}


	public function childs($service_id)
	{
		return $this->serviceRepo->childs($service_id);
	}

	public function find($serviceId)
	{
		$term = trim(request('q'));

		if (empty($term)) {
            return \Response::json([]);
        }

        $types = Type::whereDoesntHave('services',function($q) use ($serviceId){
        	$q->where('services.id', '=', $serviceId);
        })
        ->where('name', 'LIKE' , '%' .request('q'). '%')
        ->limit(5)
        ->get();

        $formatted_types = [];

        foreach ($types as $type) {
            $formatted_types[] = ['id' => $type->id, 'text' => $type->name];
        }

        return \Response::json($formatted_types);

	}
}
