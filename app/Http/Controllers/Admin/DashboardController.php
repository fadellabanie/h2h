<?php

namespace App\Http\Controllers\Admin;

use App\Charts\CustomersChart;
use App\Charts\IncomeChart;
use App\Charts\OrderMonthesChart;
use App\Domains\Customer\entity\Customer;
use App\Domains\Driver\entity\Driver;
use App\Domains\Invoice\entity\Invoice;
use App\Domains\Order\entity\Order;
use App\Domains\Order\entity\OrderStatus;
use App\Domains\User\entity\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;

class DashboardController extends Controller
{


    public function index()
    {

        $customers = Customer::count();
        $drivers = Driver::count();
        $orders = Order::where('status_id',OrderStatus::DELIVERIED)->count();
        $income = Invoice::where('invoice_status_id',3)->sum('total');
        $orderData = collect([]);

        //Customer Chart
        $customerChart = new CustomersChart();
        $customersDataChart = Customer::orderBy('created_at')
                        ->get()
                        ->groupBy(function ($customer){
                            return Carbon::parse($customer->created_at)->format('Y-m-d');
                        })
                        ->map(function ($c){
                            return count($c);
                        });
        $customerChart->labels($customersDataChart->keys());
        $customerChart->barWidth(0.1);

        $customerChart->options([
            'responsive' => true,
            'maintainAspectRatio'=> false,
            'tooltips' => [
                'intersect' => false,
                'mode' => 'nearest',
                'xPadding' => 5,
                'yPadding' => 5,
                'caretPadding' => 5
            ]
        ]);
        $customerChart->dataset('Customers','bar',$customersDataChart->values())
            ->color("rgb(220, 50, 130)")
            ->backgroundcolor("rgb(220, 50, 130)")
            ->fill(true);


        //Order Chart
        for ($i = 1 ; $i <= 12 ; $i++) {
            $q = Order::whereMonth('created_at',$i)
                ->whereYear('created_at',Carbon::now()->year)
                ->where('status_id',OrderStatus::DELIVERIED)
                ->count();
            $orderData->push($q);
        }
        $orderChart = new OrderMonthesChart();
        $orderChart->labels(["Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agu","Sep","Okt","Nov","Des"]);
        $orderChart->dataset('Orders', 'line', $orderData)
            ->color("rgb(255, 99, 132)")
            ->backgroundcolor("rgb(255, 99, 132)")
            ->fill(true);

        $orderChart->options([
            'tooltips' => [
                'intersect' => false,
                'mode' => 'nearest',
                'xPadding' => 10,
                'yPadding' => 10,
                'caretPadding' => 10
            ]
        ]);


        //income chart
        $incomeChart = new IncomeChart();
        $invoiceChartData = collect([]);

        for ($i=1; $i <= 12; $i++){
            $invoiceCount = Invoice::whereMonth('created_at',$i)
                ->whereYear('created_at',Carbon::now()->year)
                ->where('invoice_status_id',3)
                ->sum('total');
            $invoiceChartData->push($invoiceCount);
        }
        $incomeChart->labels(["Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agu","Sep","Okt","Nov","Des"]);
        $incomeChart->dataset('Income','line',$invoiceChartData)
                ->color('rgb(255, 99, 132)')
                ->backgroundcolor("rgb(255, 99, 132)")
                ->fill(false);

        $incomeChart->options([
            'tooltips' => [
                'intersect' => false,
                'mode' => 'nearest',
                'xPadding' => 10,
                'yPadding' => 10,
                'caretPadding' => 10
            ]
        ]);
        return view('admin.dashboard.index',[
            'customers' => $customers,
            'drivers' => $drivers,
            'orders' => $orders,
            'income' => $income,
            'orderChart' =>$orderChart,
            'customerChart' =>$customerChart,
            'incomeChart' => $incomeChart
        ]);
    }

}
