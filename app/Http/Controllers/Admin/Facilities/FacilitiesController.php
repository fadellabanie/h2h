<?php

namespace App\Http\Controllers\Admin\Facilities;

use App\Domains\Facility\repository\FacilityRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Gate;

class FacilitiesController extends Controller
{

	private $facilityRepo;

	public function __construct(FacilityRepository $facilityRepo)
	{
		$this->facilityRepo = $facilityRepo;
	}


	public function index()
	{

		if (!Gate::allows('facility_access')) {
            return abort(401);
        }

		if (request()->ajax()) {
    		return Datatables::eloquent($this->facilityRepo->data(['parent']))->make(true);
    	}

		return view('admin.facilities.index');
	}


	public function store()
	{

		if (!Gate::allows('facility_create')) {
            return response()->json(['errors' => [
                'name' => 'sorry you don`t have permission'
            ]],401);
        }

		$this->validate(request(),[
    		'name' => 'required|min:2|max:256',
    		'parent_id' => 'nullable|exists:facilities,id',
            'amount' => [Rule::requiredIf(function (){
                return request('parent_id') != '';
            }),'numeric'],
            'isPercentage' => 'nullable|boolean',
		]);

		$this->facilityRepo->store(request()->all());
	}

	public function update($id)
	{

		if (!Gate::allows('facility_edit')) {
            return response()->json(['errors' => [
                'name' => 'sorry you don`t have permission'
            ]],401);
        }

		$this->validate(request(),[
    		'name' => 'required|min:2|max:256',
    		'parent_id' => 'nullable|exists:facilities,id',
            'amount' => 'required|numeric',
            'isPercentage' => 'nullable|boolean',
		]);

		$this->facilityRepo->update($id,request()->all());

	}

	public function delete($id)
	{
		if (!Gate::allows('facility_delete')) {
            return abort(401);
        }

		return $this->facilityRepo->delete($id);
	}

	public function all()
	{
		return $this->facilityRepo->all();
	}
}
