<?php

namespace App\Http\Controllers\Admin\Offer;

use App\Domains\Offer\repository\OfferRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;

class OfferController extends Controller
{

	private $offers;

	public function __construct(OfferRepository $offers)
	{
		$this->offers = $offers;
	}

    
    public function index()
    {
		// return "Hello World";
    	if (request()->ajax()) {
		
			// return Datatables::eloquent($this->offers->all())->make(true);
			return Datatables::eloquent($this->offers->select(['id','offer_title','offer_image_url']))->make(true);

    	}

    	return view('admin.offers.index');
    }


    public function all()
    {
    	return $this->catRepo->all();
	}
	
	function imageEdit($image)
    {
        $imageName = date('d-m-y-h-s').$image->getClientOriginalName();
        $path = base_path().'\public\images\\';
       $image->move($path , $imageName);
        return 'images\\'.$imageName;
	}
	
    public function store()
    {
    	$this->validate(request(),[
			'title' => 'required|string|min:2|max:255',
			'image' =>'mimes:jpeg,jpg,png,gif|required|max:10000'
		]);
		
		$data['offer_title'] = request()['title'];
		$data['offer_image_url'] = $this->imageEdit(request()['image']);

    	return $this->offers->store($data);
    }


    public function show($id)
    {
    	return $this->catRepo->findCat($id);
    }

    public function update($id)
    {
    	$this->validate(request(),[
    		'name' => 'required|string|min:2|max:255|regex:/^([a-zA-Z]+)(\s[a-zA-Z]+)*$/',
    	]);

    	return $this->catRepo->update(request()->all(),$id);
    }

    public function delete($id)
    {
    	return $this->offers->delete($id);
	}
	
	
}
