<?php

namespace App\Domains\Type\repository;

use App\Domains\Service\entity\Service;
use App\Domains\Type\entity\Type;
use Illuminate\Support\Facades\DB;


class TypeRepository
{

	private $model;

	public function __construct(Type $model)
	{
		$this->model = $model;
	}


	public function all($service_id,$category_id)
	{

	   // return 'tes';
// 	    return $this->model
// 			->join('category_type','types.id','category_type.type_id')
// 			->join('service_type','types.id','service_type.type_id')
// 			->join('services','service_type.service_id','services.id')
// 			->where('service_type.service_id',$service_id)
// 			//->orWhere('services.parent_id',$service_id)
// 			->where('category_type.category_id',$category_id)
// 			->select('types.name','service_type.price','types.id')
// 			->get()
// 		;

		$q = $this->model
			//->join('category_type','types.id','category_type.type_id')
			->join('service_type','types.id','service_type.type_id')
			->join('services','service_type.service_id','services.id')
			// ->leftJoin('order_items',function($q){
			// 	$q->on('types.id','order_items.type_id')
			// 		->join('orders','order_items.order_id','orders.id')
			// 		->on('orders.status_id',1);
			// 		//->addSelect(DB::raw('count(order_items.type_id) as itemsCount'));
			// })
			//->join('orders','order_items.order_id','orders.id')
			//->where('service_type.service_id',$service_id)
			// ->whereHas('items',function($q){
			// 	$q->whereHas('order',function($q){
			// 		$q->where('orders.status_id',1)->count('order_items.type_id');
			// 	});
			// })
			->where('service_type.category_id',$category_id)
			//->where('service_type.service_id',$service_id)
			->select('types.name','service_type.price','types.id','services.id as service_id')
			->groupBy('types.name','service_type.price','types.id','services.id');

		$service = Service::findOrFail($service_id);


		if ($service->id == 4) {
			$q->where('services.parent_id',$service_id);

        }else{
		    $q->where('service_type.service_id',$service_id);
		}


		$data = $q->get();

        $data = $data->unique('id');
		$data = $data->values();
		
		$customer = auth('api')->user()->customer;
		$customerId = $customer->id;

		// dd($customer->basket());
		// dd($data);

		return $data->transform(function($type) use($customerId) {

			// dd( $type->id);

			$basketCount = DB::table('order_items')
						->join('orders','order_items.order_id','=' ,'orders.id')
						->where('orders.customer_id', '=' ,$customerId)
						->where('orders.status_id', 1)
						->where('order_items.type_id', $type->id)
						->where('order_items.service_id', $type->service_id)
						->select('order_items.count','order_items.order_id','order_items.id as item_id')->first();

			// dd($basketCount, $type);
						
			return [
				'id' => $type->id,
				'name' => $type->name,
				'price' => $type->price,
				'service_id' => $type->service_id,
				'order_id' => $basketCount ? $basketCount->order_id : 0,
				'item_id' => $basketCount ? $basketCount->item_id : 0,
				'count' => $basketCount ? $basketCount->count : 0
			];
		});

	}

	public function select(array $relations)
	{
		return $this->model
			->with($relations)
			->select('types.*');
	}

	public function getModel()
	{
		return $this->model;
	}

	public function store(array $data)
	{

		$type = $this->model->create([
			'name' => $data['name'],
			'description' => $data['description'],
		]);

		$this->typeCategoriers($type,$data['category_id']);

		return $type;
	}

	public function update(array $data,$id)
	{

		$type = $this->model->where('id',$id)->first();

		$type->update([
			'name' => $data['name'],
			'description' => $data['description'],
		]);

		$this->typeCategoriers($type,$data['category_id']);

	}

	public function delete($id)
	{
		$type = $this->model->where('id',$id)->first();

		// remove type categories
		$this->typeCategoriers($type,[]);

		//remove type service
		$this->typeServices($type,[]);
		// remove type

		$type->delete();
	}


	public function typeCategoriers($type,array $categoryIds)
	{
		$type->categories()->sync($categoryIds);
	}

	public function typeServices($type, array $serviceIds)
	{
		$type->services()->sync($serviceIds);
	}
}
