<?php

namespace App\Domains\Type\entity;

use App\Domains\Category\entity\Category;
use App\Domains\Order\entity\OrderItem;
use App\Domains\Service\entity\Service;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{

    protected $guarded = [];

    public function categories()
    {
    	return $this->belongsToMany(Category::class);
    }

    public function services()
    {
    	return $this->belongsToMany(Service::class)->withPivot('price');
    }

    public function items()
    {
    	return $this->hasMany(OrderItem::class,'type_id');
    }
}
