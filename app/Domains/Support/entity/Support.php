<?php

namespace App\Domains\Support\entity;

use App\Domains\Customer\entity\Customer;
use Illuminate\Database\Eloquent\Model;

class Support extends Model
{

    protected $guarded = [];

    public function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id');
    }
}
