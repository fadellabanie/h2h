<?php

namespace App\Domains\Fcm;

use App\Domains\Fcm\NotificationMessage;
use App\Domains\Order\entity\Order;

class PickedupMessage extends NotificationMessage
{

    private $user;
    private $order_id;


    public function __construct($user,$order_id)
    {
        $this->user = $user;
        $this->order_id = $order_id;
    }

    public function sendMessage()
    {


       //dd($this->user);
       if ($this->user->device_id != null) {
            $this->sendMessageToUserDevice($this->user->device_id,$this->getMessageData());
       }

       $model = Order::class;

       $this->persistNotification($model,$this->order_id,$this->user->id,$this->getMessageData());
    }

    private function getMessageData()
    {
        return [
          'title' => 'Picked up',
          'body' => 'You order Picked up successfully',
          'status_id' => 1
        ];
    }
}
