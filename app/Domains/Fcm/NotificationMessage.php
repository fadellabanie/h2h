<?php

namespace App\Domains\Fcm;

use App\Notification;

class NotificationMessage
{


    protected function sendMessageToUserDevice($device_id,$data)
    {
        $serverKey = $this->getServerKey();

        $data = [
            'data' => [
                'data' => $data,
                'to' => $device_id
            ],
        ];

        $client = new \GuzzleHttp\Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'key='.$serverKey,
            ]
        ]);

        $response = $client->post('https://fcm.googleapis.com/fcm/send',
            ['body' => json_encode($data['data'])]
        );
    }

    protected function persistNotification($model,$id,$reciver_id,$data)
    {

        Notification::create([
            'notifiable_type' => $model,
            'notifiable_id' => $id,
            'data' => $data,
            'reciver_id' => $reciver_id
        ]);
    }

    private function getServerKey()
    {
        return 'AAAAq5r2cQc:APA91bEYAFRC7y9Gw5NArFI4l7HYZjsQ53IbBDIQe-9qkJiMT5TdbVRBggzkpM0YpmqwQLuHA-ZBFKonzFjHaCzJrmkWiH4mERvhcCQpWYzQSaXW8gWCLY7hjQEgYV2oFTfBXe0jSH7U';
    }
}
