<?php

namespace App\Domains\City\entity;

use App\Domains\Region\entity\Region;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    
    protected $guarded = [];
    

    public function regions()
    {
    	return $this->hasMany(Region::class);
    }
}
