<?php

namespace App\Domains\City\repository;

use App\Domains\City\entity\City;


class CityRepository
{


	private $model;

	public function __construct(City $model)
	{
		$this->model = $model;
	}


	public function select(array $fields)
	{
		return $this->model->select($fields);
	}

	public function store(array $data)
	{
		return $this->model->create([
			'name' => $data['name'],
			'delivery_cost' => $data['delivery_cost'],
		]);
	}

	public function all()
	{
		return $this->model->all(['id','name']);
	}

	public function findById($id)
	{
		return $this->model->where('id',$id)->first();
	}

	public function update(array $data,$id)
	{
		return $this->model->where('id',$id)->update([
			'name' => $data['name'],
			'delivery_cost' => $data['delivery_cost']
		]);
	}

	public function delete($id)
	{
		$city = $this->model->where('id',$id)->first();

		if ($city->regions()->exists()) {
			return response()->json(['errors' => 'sorry can`t delete'],400);
		}

		$city->delete();
	}

	public function regions($id)
	{
		$city = $this->model->where('id',$id)->with('regions')->first();

		return $city->regions;
	}
}