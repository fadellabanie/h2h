<?php

namespace App\Domains\Service\entity;

use App\Domains\Category\entity\Category;
use App\Domains\Customer\entity\Customer;
use App\Domains\Facility\entity\Facility;
use App\Domains\Order\entity\Order;
use App\Domains\Order\entity\OrderItem;
use App\Domains\Type\entity\Type;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{

    protected $guarded = [];

    public function types()
    {
    	return $this->belongsToMany(Type::class)->withPivot('price')->with('categories');
    }

    public function facilities()
    {
    	return $this->belongsToMany(Facility::class)->withPivot('amount','isPercentage');
    }

    public function childs()
    {
    	return $this->hasMany(Service::class,'parent_id');
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class,'service_id');
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class,'service_id');
    }
}
