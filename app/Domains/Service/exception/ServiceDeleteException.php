<?php


namespace App\Domains\Service\exception;


use Exception;

class ServiceDeleteException extends Exception
{
    
    public function render()
    {
    	return response()->json([
    		'errors' => 'sorry can`t delete service has types or order items'
    	],500);
    }
}
