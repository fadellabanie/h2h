<?php

namespace App\Domains\Service\repository;

use App\Domains\Service\entity\Service;
use App\Domains\Service\exception\ServiceDeleteException;

class ServiceRepository
{

	private $model;

	public function __construct(Service $model)
	{
		$this->model = $model;
	}


	public function toggleActivation($id)
	{

		$service = $this->model->where('id',$id)->first();

		if ($service->isActive == 1) {

			return $service->update(['isActive' => 0]);
		}

		return $service->update(['isActive' => 1]);
	}

	public function selectChilds($service_id)
	{
		return $this->model->where('parent_id',$service_id)
				->select('services.*');
	}

	public function createChild($data,$service_id)
	{
		return $this->model->create([
			'name' => $data['name'],
			'parent_id' => $service_id,
		]);
	}

	public function all()
	{
		return $this->model->whereNull('parent_id')->paginate(10);
	}

	public function findService($id)
	{
		return $this->model->where('id',$id)->first();
// 		return $this->model->where('id',$id)->where('parent_id',null)->firstOrFail();
	}


	public function childs($service_id)
	{
		return $this->model->where('parent_id',$service_id)->get();
	}


	public function update($data,$id)
	{
		return $this->model->where('id',$id)->update($data);
	}

	public function delete($serviceId)
	{
		$service = $this->model->where('id',$serviceId)->first();

		if ($service->types()->exists() || $service->orderItems()->exists()) {
			throw new ServiceDeleteException();
		}

		$service->delete();
	}

	public function prices($serviceId)
	{
		return $this->model->where('id',$serviceId)
			->with('types.categories')->first();
	}
}
