<?php

namespace App\Domains\Facility\entity;

use App\Domains\Order\entity\Order;
use App\Domains\Service\entity\Service;
use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{

    protected $guarded = [];


    public function services()
    {
    	return $this->belongsToMany(Service::class)->withPivot('amount','isPercentage');
    }

    public function parent()
    {
    	return $this->belongsTo(Facility::class,'parent_id');
    }

    public function childrens()
    {
    	return $this->hasMany(Facility::class,'parent_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class,'facility_id');
    }
}
