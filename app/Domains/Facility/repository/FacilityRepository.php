<?php

namespace App\Domains\Facility\repository;

use App\Domains\Facility\entity\Facility;
use App\Domains\Facility\exceptions\FacilityLevelException;
use Illuminate\Support\Facades\DB;

class FacilityRepository
{

	private $model;

	public function __construct(Facility $model)
	{
		$this->model = $model;
	}

	public function data(array $relations)
	{
		return $this->model
			->with($relations)
			->select('facilities.*');
	}

	public function find($id)
	{
		return $this->model->where('id',$id)->first();
	}

	public function store(array $data)
	{
		if (isset($data['parent_id'])) {
			$this->validateFacilityLevels($data['parent_id']);
		}

		return $this->model->create([
		    'name' => $data['name'],
		    'isPercentage' => request()->has('isPercentage') ? 1 : 0 ,
            'amount' => $data['amount'],
            'parent_id' => $data['parent_id']
        ]);
	}

	public function update($id,array $data)
	{
		if (isset($data['parent_id'])) {
			$this->validateFacilityLevels($data['parent_id']);
		}
		return $this->model->where('id',$id)->update([
            'name' => $data['name'],
            'isPercentage' => request()->has('isPercentage') ? 1 : 0 ,
            'amount' => $data['amount'],
            'parent_id' => $data['parent_id']
        ]);
	}

	public function delete($id)
	{
		$facility = $this->model->where('id',$id)->first();

		if ($facility->childrens()->exists()) {
			return response()->json(['errors' => 'sorry can`t delete parent has childs'],400);
		}

		if ($facility->order()->exists()){
		    return  response()->json(['errors' => 'sorry can`t delete facility delete order first']);
        }

		$facility->services()->sync([]);

		$facility->delete();

	}


	public function childs($parent_id)
	{
		return $this->model
			->where('parent_id',$parent_id)
			->get();
	}

	public function facilitiesForService($service_id)
	{

		return DB::table('facility_service')
			->join('services','facility_service.service_id','services.id')
			->join('facilities','facility_service.facility_id','facilities.id')
			//->whereNull('facilities.parent_id')
			->where('facility_service.service_id',$service_id)
			->select('facilities.name','facilities.id','facility_service.isPercentage','facility_service.amount')
			->get();
		// return $this->model
		// 	->whereNull('facilities.parent_id')
		// 	->whereHas('childrens',function($q) use($service_id){
		// 		$q->whereHas('services',function($cq) use($service_id){
		// 			$cq->where('facility_service.service_id',$service_id);
		// 		});
		// 	})
		// 	->with('services')
		// 	->get();
	}

	public function getServiceFacilities($facility_id,$service_id)
	{
		return DB::table('facility_service')
				->where('service_id',$service_id)
				->where('facility_id',$facility_id)
				->first();
	}

	public function all()
	{
		return $this->model
		->get(['id','name','parent_id']);
	}

	public function getModel()
	{
		return $this->model;
	}

	private function validateFacilityLevels($parent_id)
	{
		$facility = $this->model->where('id',$parent_id)
			->whereNull('parent_id')
			->exists();

		if (!$facility) {
			throw new FacilityLevelException();
		}
	}

	public function calculateFacilities($facility)
	{

		if ($facility['isPercentage'] == 1) {
			return $facility['amount'] / 100;
		}

		return $facility['amount'];

		// if (empty($facilities)) {
		// 	return 0;
		// }

		// $facilitiesData = collect();

		// foreach ($facilities as $facility) {
		// 	if ($facility['isPercentage'] == 1) {
		// 	 	$facilitiesData->push($facility['amount'] / 100);
		// 	}else{
		// 		$facilitiesData->push($facility['amount']);
		// 	}

		// }

		// return $facilitiesData->sum();
	}


}
