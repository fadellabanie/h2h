<?php

namespace App\Domains\Facility\exceptions;

use Exception;

class FacilityLevelException extends Exception
{
    
    public function render()
    {
    	return response()->json(['errors' => [
    		'parent_id' => 'can`t have more than one level'
    	]
    ],500);
    }
}
