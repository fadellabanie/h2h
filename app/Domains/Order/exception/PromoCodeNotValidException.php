<?php


namespace App\Domains\Order\exception;


use Exception;

class PromoCodeNotValidException extends Exception
{
    
    public function render()
    {
    	return response()->json([
    		'errors' => 'Promo Code Not Valid'
    	],500);
    }
}
