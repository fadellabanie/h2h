<?php


namespace App\Domains\Order\exception;


use Exception;

class OrderCancelledException extends Exception
{
    
    public function render()
    {
    	return response()->json([
    		'errors' => 'Order  Cancelled Error'
    	],500);
    }
}
