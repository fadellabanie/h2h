<?php


namespace App\Domains\Order\exception;


use Exception;

class OrderPlacedException extends Exception
{
    
    public function render()
    {
    	return response()->json([
    		'errors' => 'No order to be placed'
    	],500);
    }
}
