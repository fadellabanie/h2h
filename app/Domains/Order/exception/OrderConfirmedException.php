<?php


namespace App\Domains\Order\exception;


use Exception;

class OrderConfirmedException extends Exception
{
    
    public function render()
    {
    	return response()->json([
    		'errors' => 'Order  Confirmed Error'
    	],500);
    }
}
