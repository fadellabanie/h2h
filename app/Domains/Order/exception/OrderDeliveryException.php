<?php


namespace App\Domains\Order\exception;


use Exception;

class OrderDeliveryException extends Exception
{
    
    public function render()
    {
    	return response()->json([
    		'errors' => 'No available drivers in this date please choose another date'
    	],500);
    }
}
