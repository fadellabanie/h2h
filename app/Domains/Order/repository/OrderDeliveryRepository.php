<?php

namespace App\Domains\Order\repository;

use App\Domains\Driver\entity\Driver;
use App\Domains\Driver\entity\DriverStatus;
use App\Domains\Fcm\OrderAcceptedMessage;
use App\Domains\Order\entity\OrderDelivery;
use App\Domains\Order\entity\OrderStatus;
use App\Domains\Order\exception\OrderDeliveryException;
use App\Domains\Order\service\OrderService;
use App\Domains\User\entity\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;


class OrderDeliveryRepository
{

	private $model;
	private $orderService;


	public function __construct(OrderDelivery $model,OrderService $orderService)
	{
		$this->model = $model;
		$this->orderService = $orderService;
	}

	public function acceptOrder($order)
	{

		$driver = $this->matchDriver($order);

		$now = Carbon::now()->format('Y-m-d H:i:s');

		$this->updateOrder($order);


		$order_delivery = $this->model->create([
			'order_id' => $order->id,
			'driver_id' => $driver->id,
			'accepted_at' => $now,
		]);

        $driver_id = $order_delivery->driver_id;

		$user = User::whereHas('driver',function($q) use($driver_id){
            $q->where('drivers.id',$driver_id);
        })->first();

        $message = new OrderAcceptedMessage($user,$order->id);


        $message->sendMessage();

        return $order_delivery;
	}

	public function updateOrder($order)
	{
		$order->update([
            'status_id' => OrderStatus::ACCEPTED
        ]);
	}

	public function matchDriver($order)
	{
		$orderRegion = $order->address()->first()->region_id;

		// $driver = DB::table('drivers')
		// 	->join('driver_region','drivers.id','driver_region.driver_id')
		// 	->join('users','drivers.user_id','users.id')
		// 	->where('driver_region.region_id',$orderRegion)
		// 	->where('users.isActive',1)
		// 	->first();

		$driver = Driver::whereHas('regions',function($q) use($orderRegion){
						$q->where('driver_region.region_id',$orderRegion);
					})
			->whereHas('user',function($q){
				$q->where('users.isActive',1);
			})
			->first();

		if ($driver) {
			return $driver;
		}

		$order->update([
			'status_id' => OrderStatus::PLACED,
		]);

		$order->invoice()->delete();

		throw new OrderDeliveryException();
	}

}
