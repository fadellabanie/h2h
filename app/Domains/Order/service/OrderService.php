<?php

namespace App\Domains\Order\service;

use App\Domains\Facility\entity\Facility;
use App\Domains\Facility\repository\FacilityRepository;
use App\Domains\Order\entity\Order;
use App\Domains\Order\entity\OrderItem;
use App\Domains\Order\entity\OrderStatus;
use App\Domains\Order\exception\OrderCancelledException;
use App\Domains\Order\exception\OrderConfirmedException;
use App\Domains\Order\exception\OrderPlacedException;
use App\Domains\Order\exception\PromoCodeNotValidException;
use App\Domains\PromoCode\entity\PromoCode;
use App\Domains\Type\entity\Type;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class OrderService
{
	private $model;
	private $facilityRepo;
	private $orderItem;

	public function __construct(Order $model,FacilityRepository $facilityRepo,OrderItem $orderItem)
	{
		$this->model = $model;
		$this->facilityRepo = $facilityRepo;
		$this->orderItem = $orderItem;
	}

	public function update($data,$customer)
	{

		$order = $customer->customerBasket();

		if ($data['promo_code'] != '') {
			$this->addPromoCode($data['promo_code'],$customer,$data['promo_amount']);
		}

		$order->comment = $data['comment'];
		$order->is_urgent = $data['is_urgent'];
		$order->is_hanged = $data['is_hanged'];

		$order->promo_code_id = $data['promo_code'] ? $data['promo_code'] : null;
		$order->amount = $data['amount'];
		$order->total_amount = $data['total_amount'];

		$order->save();

		return $order;
	}

	public function facilitiesTotalAmount($facilities)
	{
		$faciltyAmounts = $this->facilityRepo->calculateFacilities($facilities);

		return $faciltyAmounts;

	}


	public function addFacilities($facility_id)
	{

        $customer = auth('api')->user()->customer;

        $order = $customer->customerBasket();

        $facility = Facility::findOrFail($facility_id);


        if ($facility->isPercentage == 1){
            $faciltyAmounts = $facility->amount / 100;
            $total_amount =  $order->amount + ($order->amount * $faciltyAmounts);
        }else{
            $total_amount =  $order->amount + $facility->amount;
        }

        $order->total_amount = $total_amount;
        $order->facility_id = $facility->id;
        $order->facility_amount = $facility->amount;
        $order->isPercentage = $facility->isPercentage;

        $order->save();

        return $order;
	}

	public function removeFacility()
    {

        $customer = auth('api')->user()->customer;

        $order = $customer->customerBasket();

        if ($order->facility_id == null){
            return response()->json(['error' => 'sorry order facility already empty'],500);
        }

        $order->facility_id = null;
        $order->isPercentage = null;
        $order->facility_amount = null;
		$order->facility_amount = null;

		$order->is_urgent = 0 ;
		
		$order->promo_code_id = null;

        $this->calculateOrder($order);

        return $order;
    }

	public function addUrgentToOrder($order,$is_urgent)
	{

		$baseAmount = $order->items()->sum('amount');

		$facilitiesAmount =   $order->items->map(function($item){
			return $item->total_amount - $item->amount;
		})->sum();

		if ($is_urgent == 1) {
			$total_amount = ($baseAmount * 2) + $facilitiesAmount;
		}else{
			$total_amount = $baseAmount;
		}

		$order->update([
			'total_amount' => $total_amount,
			'is_urgent' => $is_urgent
		]);

		return $order;
	}

	public function store($data,$customer)
	{
        
        $order = $customer->customerBasket();
        
		if (!$order || $order->status_id > OrderStatus::PLACED) {
			$order = $this->createNewOrder($data,$customer);
		}


		$this->addNewItem($order,$data);

		return $order;
	}

	public function currentOrder($customer)
	{
		return $this->model->where('customer_id',$customer->id)->latest()->first();
	}

	private function addNewItem($order,$data)
	{

	    $item = OrderItem::where('category_id',$data['category_id'])
            ->where('service_id',$data['service_id'])
            ->where('type_id',$data['type_id'])
            ->whereHas('order',function ($q) use ($order){
                $q->where('orders.id',$order->id);
            })
            ->first();

        $amount = $this->getBaseAmount($data['type_id'],$data['service_id']) * $data['count'];

        if ($item){
            $item->update(['amount' => $amount,'total_amount' => $amount,'count' => $data['count']]);
        }else{
            OrderItem::create([
                'category_id' => $data['category_id'],
                'service_id' => $data['service_id'],
                'type_id' => $data['type_id'],
                'amount' => $amount,
                'total_amount' => $amount,
                'order_id' => $order->id,
                'count' => $data['count']
            ]);
        }

		return $this->calculateOrder($order);
	}

	public function increment($data, $customer)
	{

		$order = $customer->customerBasket();
        
		if (!$order || $order->status_id > OrderStatus::PLACED) {
			$order = $this->createNewOrder($data,$customer);
		}
		
		$item = OrderItem::where('category_id', $data['category_id'])
			->where('service_id', $data['service_id'])
			->where('type_id', $data['type_id'])
			->whereHas('order',function ($q) use ($order){
				$q->where('orders.id', $order->id);
			})->first();


        if ($item){

			$amount = $this->getBaseAmount($data['type_id'], $data['service_id']);
			
            $item->update([
				'amount' => $amount,
				'total_amount' => $amount * ($item->count + 1),
				'count' => $item->count + 1
			]);

        } else {
			
			$amount = $this->getBaseAmount($data['type_id'], $data['service_id']);
            OrderItem::create([
                'category_id' => $data['category_id'],
                'service_id' => $data['service_id'],
                'type_id' => $data['type_id'],
                'amount' => $amount,
                'total_amount' => $amount,
                'order_id' => $order->id,
                'count' => 1
            ]);
		}

		$order = $order->fresh();
		$order = $order->load('items');

		return $this->calculateOrder($order);
	}

	public function decrement($data, $customer)
	{

		$order = $customer->customerBasket();
        
		if ($order) {		

			$item = OrderItem::where('category_id', $data['category_id'])
							->where('service_id', $data['service_id'])
							->where('type_id', $data['type_id'])
							->whereHas('order',function ($q) use ($order){
								$q->where('orders.id', $order->id);
							})->first();

			if ($item){
				
				if ($item->count > 1) {
					
					$amount = $this->getBaseAmount($data['type_id'], $data['service_id']);
					
					$item->update([
						'amount' => $amount,
						'total_amount' => $amount * ($item->count - 1),
						'count' => $item->count - 1
					]);
						
				} else {

					$item->delete();

					$items = OrderItem::where('order_id', $order->id )->get();

					if ($items->count() == 0) {
						// order has no items
						$order->delete();
						return [];
					}
					
				}
				
			}

			$order = $order->fresh();
			$order = $order->load('items');

			return $this->calculateOrder($order);

		}// if no order
	}
	

	private function calculateOrder($order)
	{

		$total_quantity = $order->items()->sum('count');
		
		$subTotal = 0;

		foreach($order->items as $item){
            
			$itemSubTotal = $item->amount * $item->count;
			$subTotal += $itemSubTotal;
        }

		$total_amount = $order->items()->sum('total_amount');

		$order->total_quantity = $total_quantity;
		$order->amount = $subTotal;
		$order->total_amount = $total_amount;

		$order->save();

		return $order;
	}

	private function createNewOrder($data,$customer)
	{
		return $this->model->create([
			'uuId' =>  Str::orderedUuid()->toString(),
			'total_quantity' => 0,
			'total_amount' => 0,
			'amount' => 0,
			'customer_id' => $customer->id,
			'status_id' => OrderStatus::BASKET,
		]);
	}

	public function toggleFacilities($data)
	{

		$facility_id = $data['facility_id'];

		$customer = auth('api')->user()->customer;

		$order = $customer->customerBasket();

		//$items = $order->items()->where('service_id','<',4)->get();

		//dd($items);
        $facility = Facility::findOrFail($data['facility_id']);

        $faciltyAmounts = $this->facilitiesTotalAmount($facility);

        $total_amount =  $order->amount + ($order->amount * $faciltyAmounts);

        $order->update([
           'total_amount' => $total_amount,
           'facility_id' => $facility->id,
           'facility_amount' => $facility->amount,
           'isPercentage' => $facility->isPercentage
        ]);

		$order->items()->where('service_id','<',4)->get()->map(function($item) use($facility_id){

			$facility = $this->getFacilities($facility_id,$item->service_id);

			$faciltyAmounts = $this->facilitiesTotalAmount($facility);

			$total_amount =  $item->amount + ($item->amount * $faciltyAmounts);

			$item->update([
				'total_amount' => $total_amount,
				'facility_id' => $facility_id,
			]);

		});

		return $this->calculateOrder($order);
		// $item = $this->orderItem->where('id',$data['item_id'])->first();

		// $facility = $this->getFacilities($data['facility_id'],$data['service_id']);

		// $faciltyAmounts = $this->facilitiesTotalAmount($facility);

		// $total_amount =  $item->amount + ($item->amount * $faciltyAmounts);

		// $item->update([
		// 	'facility_id' => $facility['id'],
		// 	'total_amount' =>$total_amount
		// ]);

		// $order = $this->model->where('id',$item->order_id)->first();

		// return $this->calculateOrder($order);
	}

	public function getFacilities($facility_id,$service_id)
	{

		$facility = $this->facilityRepo->getServiceFacilities($facility_id,$service_id);

		if (!$facility) {
			return;
		}
		//dd($facility);
		return [
			'id' => $facility->facility_id,
			'name' => $this->facilityRepo->find($facility->facility_id)->name,
			'amount' => $facility->amount,
			'isPercentage' => $facility->isPercentage,
		];
		// ->transform(function($facility){
		// 	return [
		// 		'id' => $facility->facility_id,
		// 		'name' => $this->facilityRepo->find($facility->facility_id)->name,
		// 		'amount' => $facility->amount,
		// 		'isPercentage' => $facility->isPercentage,
		// 	];
		// });
	}

	public function getBaseAmount($type_id,$service_id)
	{
		$item = DB::table('service_type')
						->where('type_id',$type_id)
						->where('service_id',$service_id)
						->first();

		return $item->price;
	}


	public function deleteItem($order_id,$item_id)
	{
		$this->orderItem->where('id',$item_id)->delete();

		$order = $this->model->where('id',$order_id)->first();

		return $this->calculateOrder($order);
	}


	public function addPromoCode($promoCode,$customer,$amount)
	{

		$order = $customer->customerBasket();

		$now = Carbon::now()->format('Y-m-d');

		// $promoCode = PromoCode::where('code',$code)
		// ->whereDate('from','<=',$now)
		// ->whereDate('to', '>=',$now)
		// ->where('status',1)
		// ->first();

		// if (!$promoCode || $customer->promoCodes()->exists()) {
		// 	throw new PromoCodeNotValidException;
		// }

		$customer->promoCodes()->attach($promoCode,[
			'promo_amount' => $amount
		]);
		return ;
		// if ($promoCode['isPercentage'] == 1) {

		// 	$percentage_total = $order->total_amount * ($promoCode['amount'] / 100);

		// 	$total = $order->total_amount - $percentage_total;


		// 	$order->update([
		// 		'total_amount' => $total,
		// 		'promo_code_id' => $promoCode->id,
		// 	]);

		// 	return $order;
		// }

		// $total = $order->total_amount - $promoCode['amount'];

		// $order->update([
		// 	'total_amount' => $total,
		// 	'promo_code_id' => $promoCode->id,
		// ]);

		return $order;
	}

	public function placeOrder($data,$customer,$address)
	{

		$order = $customer->customerBasket();

		if (!$order) {
			throw new OrderPlacedException();
		}


		return $order->update([
			'status_id' => OrderStatus::PLACED,
			'collection_date' => $data['collection_date'],
			'pickup_time_id' => $data['pickup_time_id'],
			'address_id' => $address->id,
		]);

	}

	public function confirmOrder($data,$customer)
	{

		$order = $customer->placedOrder();

		if (!$order) {
			throw new OrderConfirmedException();
		}


		$order->update([
			'status_id' => OrderStatus::REQUESTED,
			'payment_type_id' => $data['payment_type_id'],
		]);



		return $order;
	}

	public function cancelOrder($order_id)
	{

		$order = $this->model->where('id',$order_id)->first();

		if ($order->status_id >= OrderStatus::ACCEPTED || $order->status_id == OrderStatus::CANCELLED) {
			throw new OrderCancelledException();
		}

		return $order->update([
			'status_id' => OrderStatus::CANCELLED
		]);
	}

}
