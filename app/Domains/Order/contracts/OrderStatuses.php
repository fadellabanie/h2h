<?php

namespace App\Domains\Order\contracts;

interface OrderStatuses
{
	const BASKET = 1;
	const PLACED = 2;
	const REQUESTED = 3;
	const CANCELLED = 4;
	const ACCEPTED = 5;
	const PICKEDUP = 6;
	const PROCESSING = 7;
	const OUTOFDELIVERY = 8;
	const DELIVERIED = 9;

}
