<?php

namespace App\Domains\Order\entity;

use App\Domains\Order\contracts\OrderStatuses;
use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model implements OrderStatuses
{


    protected $guarded = [];

}
