<?php

namespace App\Domains\Order\entity;

use App\Domains\Category\entity\Category;
use App\Domains\Facility\entity\Facility;
use App\Domains\Order\entity\Order;
use App\Domains\Service\entity\Service;
use App\Domains\Type\entity\Color;
use App\Domains\Type\entity\Size;
use App\Domains\Type\entity\Type;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OrderItem extends Model
{

	protected $casts = [
        'facilities' => 'array',
    ];

    protected $guarded = [];


    public function order()
    {
        return $this->belongsTo(Order::class,'order_id');
    }

    public function service()
    {
    	return $this->belongsTo(Service::class,'service_id');
    }

    public function type()
    {
    	return $this->belongsTo(Type::class,'type_id');
    }

    public function color()
    {
        return $this->belongsTo(Color::class,'color_id');
    }

    public function size()
    {
        return $this->belongsTo(Size::class,'size_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class,'category_id');
    }

    public function facility()
    {
        return $this->belongsTo(Facility::class,'facility_id');
    }

    public function facilityServicePrice($facility_id,$service_id)
    {
        return DB::table('facility_service')
                ->where('facility_id',$facility_id)
                ->where('service_id', $service_id)
                ->first();
    }
}
