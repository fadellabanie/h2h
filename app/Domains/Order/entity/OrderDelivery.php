<?php

namespace App\Domains\Order\entity;

use App\Domains\Driver\entity\Driver;
use App\Domains\Order\entity\Order;
use App\Domains\User\entity\User;
use Illuminate\Database\Eloquent\Model;

class OrderDelivery extends Model
{
    

    protected $guarded = [];


    public function driver()
    {
    	return $this->belongsTo(Driver::class,'driver_id');
    }

    public function order()
    {
    	return $this->belongsTo(Order::class,'order_id');
    }
}
