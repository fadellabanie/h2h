<?php

namespace App\Domains\Order\entity;

use App\Domains\Customer\entity\Address;
use App\Domains\Customer\entity\Customer;
use App\Domains\Driver\entity\Driver;
use App\Domains\Facility\entity\Facility;
use App\Domains\Invoice\entity\Invoice;
use App\Domains\Order\entity\OrderDelivery;
use App\Domains\Order\entity\OrderItem;
use App\Domains\Order\entity\OrderStatus;
use App\Domains\PromoCode\entity\PromoCode;
use App\Domains\Setting\entity\PickupTime;
use App\Domains\Payment\entity\PaymentType;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{

    protected $guarded = [];

    public function items()
    {
    	return $this->hasMany(OrderItem::class,'order_id');
    }

    public function delivery()
    {
    	return $this->hasOne(OrderDelivery::class,'order_id');
    }

    public function driver()
    {
        return $this->hasOneThrough(Driver::class,OrderDelivery::class,'driver_id','user_id');
    }

    public function address()
    {
    	return $this->belongsTo(Address::class,'address_id');
    }
    public function paymentType()
    {
        return $this->belongsTo(PaymentType::class);
    }
    public function invoice()
    {
        return $this->hasOne(Invoice::class,'order_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id');
    }

    public function status()
    {
        return $this->belongsTo(OrderStatus::class,'status_id');
    }

    public function rates()
    {
        return $this->belongsToMany(Customer::class,'customer_order_rate_service','order_id','customer_id');
    }

    public function pickupTime()
    {
        return $this->belongsTo(PickupTime::class,'pickup_time_id');
    }

    public function promoCode()
    {
        return $this->belongsTo(PromoCode::class,'promo_code_id');
    }

    public function getCodeAmount()
    {
        return DB::table('orders')
            ->join('customer_promo','orders.promo_code_id','customer_promo.promoCode_id')
            ->where('customer_promo.promoCode_id',$this->promo_code_id)
            ->select('customer_promo.promo_amount')
            ->first();
    }

    public function facility()
    {
        return $this->belongsTo(Facility::class,'facility_id');
    }

    public static function boot()
    {
        parent::boot();

        static::deleted(function($order) {
            if (count($order->items) > 0) {
                foreach ($order->items as $item) {
                    $item->delete();
                }
            }
        });
    }


}
