<?php

namespace App\Domains\PromoCode\repository;

use App\Domains\PromoCode\entity\PromoCode;
use Illuminate\Support\Str;

class PromoCodeRepository
{

	private $model;

	public function __construct(PromoCode $model)
	{
		$this->model = $model;
	}

	public function find($id)
	{
		return $this->model->where('id',$id)->first();
	}

	public function select(array $relations)
	{
		return $this->model->with($relations)->select('promo_codes.*');
	}

	public function store(array $data)
	{
		return $this->model->create([
			'name' => $data['name'],
			'description' => $data['description'],
			'code' => $data['code'],
			'amount' => $data['amount'],
			'from' => $data['from'],
			'to' => $data['to'],
			'isPercentage' => request()->has('isPercentage') ? 1 : 0 ,
		]);
	}

	public function update($id,$data)
	{
		return $this->model->where('id',$id)->update([
			'name' => $data['name'],
			'code' => $data['code'],
			'description' => $data['description'],
			'amount' => $data['amount'],
			'from' => $data['from'],
			'to' => $data['to'],
            'isPercentage' => request()->has('isPercentage') ? 1 : 0 ,
		]);
	}


	public function toggle($id)
	{
		$code = $this->model->where('id',$id)->first();

		if ($code->status == 0) {
			return $code->update(['status' => 1]);
		}

		return $code->update(['status' => 0]);
	}
}
