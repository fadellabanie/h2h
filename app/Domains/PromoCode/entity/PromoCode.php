<?php

namespace App\Domains\PromoCode\entity;

use App\Domains\Customer\entity\Customer;
use App\Domains\Order\entity\Order;
use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{

    protected $guarded = [];


    public function customers()
    {
        return $this->belongsToMany(Customer::class,'customer_promo','promoCode_id','customer_id');
    }

    public function orders()
    {
    	return $this->hasMany(Order::class,'promo_code_id');
    }

}
