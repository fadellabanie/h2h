<?php

namespace App\Domains\Region\repository;

use App\Domains\Region\entity\Region;


class RegionRepository
{

	private $model;

	public function __construct(Region $model)
	{
		$this->model = $model;
	}


	public function select(array $relation)
	{
		return $this->model
			->with($relation)
			->select('regions.*');
	}

	public function all()
	{
		return $this->model->all(['id','name']);
	}

	public function store(array $data)
	{
		return $this->model->create($data);	
	}

	public function update(array $data,$id)
	{
		return $this->model->where('id',$id)->update($data);
	}

	public function delete($id)
	{
		$this->model->where('id',$id)->delete();		
	}
}