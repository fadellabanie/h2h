<?php

namespace App\Domains\Region\entity;

use App\Domains\City\entity\City;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{

    protected $guarded = [];

    public function city()
    {
    	return $this->belongsTo(City::class,'city_id');
    }

}
