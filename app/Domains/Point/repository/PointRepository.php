<?php

namespace App\Domains\Point\repository;

use App\Domains\Point\entity\Point;

class PointRepository
{
	private $model;

	public function __construct(Point $model)
	{
		$this->model = $model;
	}


	public function select(array $fields)
	{
		return $this->model->select($fields);
	}

	public function all()
	{
		// return $this->model->all(['id','offer_title','offer_image_url']);
	}

	public function store(array $data)
	{
		return $this->model->create(array_filter($data));
	}


	public function findCat($id)
	{
		return $this->model->where('id',$id)->first();
	}


	public function update(array $data,$id)
	{
		return $this->model->where('id',$id)->update($data);
	}

	public function delete($id)
	{
		$cat = $this->model->where('id',$id)->first();

		

		$cat->delete();
	}
}