<?php

namespace App\Domains\Point\entity;

use App\Domains\Customer\entity\Customer;
use App\Domains\Order\entity\Order;
use Illuminate\Database\Eloquent\Model;

class Point extends Model
{

    protected $guarded = [];

    public function pointable()
    {
        return $this->morphTo();
    }

    public function customer()
    {
    	return $this->belongsTo(Customer::class,'customer_id');
    }

    public function order()
    {
    	return $this->belongsTo(Order::class,'order_id');
    }

}
