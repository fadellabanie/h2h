<?php

namespace App\Domains\Driver\contracts;

interface DriverStatuses
{
	const BUSY = 1;
	const AVAILABLE = 2;
}