<?php

namespace App\Domains\Driver\entity;

use App\Domains\Driver\contracts\DriverStatuses;
use Illuminate\Database\Eloquent\Model;

class DriverStatus extends Model implements DriverStatuses
{
    

    protected $guarded = [];
    
}
