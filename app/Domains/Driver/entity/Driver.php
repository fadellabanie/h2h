<?php


namespace App\Domains\Driver\entity;

use App\Domains\Driver\entity\DriverStatus;
use App\Domains\Order\entity\OrderDelivery;
use App\Domains\Region\entity\Region;
use App\Domains\User\entity\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    
    protected $guarded = [];


    public function user()
    {
    	return $this->belongsTo(User::class,'user_id');
    }

    public function status()
    {
    	return $this->belongsTo(DriverStatus::class,'driver_status_id');
    }

    public function regions()
    {
    	return $this->belongsToMany(Region::class);
    }

    public function delivery()
    {
        return $this->hasMany(OrderDelivery::class,'driver_id');
    }
/*    public function setDateOfBirthAttribute($value)
    {
        $this->attributes['date_of_birth'] = Carbon::parse($value)->format('y-m-d');
    }*/
}
