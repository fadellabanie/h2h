<?php

namespace App\Domains\Driver\service;

use App\Domains\Driver\repository\DriverRepository;
use App\Domains\User\repository\ManagerRepository;
use Illuminate\Support\Facades\DB;
use App\Domains\Day\repository\DayRepository;

class Driver
{

	private $driverRepo;
	private $userRepo;
	private $dayRepo;
	public function __construct(DriverRepository $driverRepo,ManagerRepository $userRepo,DayRepository $day )
	{
		$this->driverRepo = $driverRepo;
		$this->userRepo = $userRepo;
		$this->dayRepo = $day;
	}


	public function data(array $relations)
	{
		return $this->driverRepo->model()->with($relations)->select('drivers.*');
	}

	public function new(array $data)
	{
		$data['user_group_id'] = 2;
		
		DB::beginTransaction();
		try{
		    // Database operations
		    $user = $this->userRepo->store($data);
			$driver = $this->driverRepo->store($user->id,$data);
		
			//$this->dayRepo->store($driver->id,$data['day']);
		    // commit action for success
		    DB::commit();
		}catch(\Exception $e){
		    // rollback operation for failure
		    DB::rollback();
		    return response()->json(['errors' => 'server serror'],400);
		}	
	}

	public function update($id,$data)
	{
		$this->driverRepo->update($id,$data);
		//$this->dayRepo->update($id,$data['day']);
	}


	public function driverImage($id,$image)
	{

		$driver = $this->driverRepo->findDriver($id,[]);

		if ($driver->image_url != null && file_exists(public_path($driver->image_url)) ) {
			unlink(public_path($driver->image_url));
		}

		$ext = $image->getClientOriginalExtension();
		$name = time().'.'.$ext;
		$path = public_path('/uploads/drivers/avatars');
		$fullPath = '/uploads/drivers/avatars/' . $name;
		$image->move($path, $name);

		$driver->update([
			'image_url' => $fullPath
		]);
	}

	
	public function driverFile($id,$file)
	{
		
		$driver = $this->driverRepo->findDriver($id,[]);

		if ($driver->file_url != null ) {
			//unlink(public_path('uploads/'.$driver->image_url));
			unlink(storage_path('/app/'.$driver->file_url));
		}

		$driver->update([
			'file_url' => $file->store('driver/files','local') 
		]);
	}
}