<?php

namespace App\Domains\Driver\repository;

use App\Domains\Driver\entity\Driver;
use App\Domains\User\entity\User;
use Carbon\Carbon;

class DriverRepository
{

	private $model;

	public function __construct(Driver $model)
	{
		$this->model = $model;
	}


	public function model()
	{
		return $this->model;
	}

	public function store($user_id,$data)
	{

		$driver = $this->model->create([
			'user_id' => $user_id,
			'license' => $data['license'],
			'date_of_birth' => $data['date_of_birth'],
			'driver_status_id' => 2,
			'file_url' => request()->hasFile('file') ? request()->file('file')->store('driver/files','local') : null
		]);
		$this->syncRegion($driver,$data['region_id']);
		
		return $driver;
	}

	public function findDriver($id,array $relations)
	{
		return $this->model->where('id',$id)
			->with($relations)
			->first();
	}

	public function update($id,array $data)
	{
		$driver = $this->model->where('id',$id)->first();

		$driver->update([
			'license' => $data['license'],
			//'time_from' => $data['time_from'],
			//'time_to' => $data['time_to'],
			'date_of_birth' => Carbon::parse($data['date_of_birth'])->format('Y-m-d')
		]);


		$this->syncRegion($driver,$data['region_id']);

		
	}

	public function syncRegion($driver,array $regions)
	{
		if (!empty($regions)) {
			$driver->regions()->sync($regions);
		}
	}

}