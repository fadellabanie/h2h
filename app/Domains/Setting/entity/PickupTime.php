<?php

namespace App\Domains\Setting\entity;

use App\Domains\Order\entity\Order;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PickupTime extends Model
{
    

    protected $guarded = [];

    public function setFromAttribute($from)
    {
    	$this->attributes['from'] = Carbon::parse($from);
    }    

    public function setToAttribute($to)
    {
    	$this->attributes['to'] = Carbon::parse($to);
    }

    public function getFromAttribute($from)
    {
    	return Carbon::parse($from)->format('g:i A');;
    }    

    public function getToAttribute($to)
    {
    	return Carbon::parse($to)->format('g:i A');;
    }

    public function orders()
    {
        return $this->hasMany(Order::class,'pickup_time_id');
    }
}
