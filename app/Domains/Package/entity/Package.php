<?php

namespace App\Domains\Package\entity;

use App\Domains\Customer\entity\Customer;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{

    protected $guarded = [];


    public function customers()
    {
    	return $this->belongsToMany(Customer::class)->withTimestamps();
    }
}
