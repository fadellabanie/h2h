<?php

namespace App\Domains\Package\repository;

use App\Domains\Package\entity\Package;

class PackageRepository
{


	private $model;

	public function __construct(Package $model)
	{
		$this->model = $model;
	}


	public function select(array $relations)
	{
		return $this->model->with($relations)->select('packages.*');
	}

	public function delete($id)
	{
		$cat = $this->model->where('id',$id)->first();

		$cat->delete();
	}


	public function status($id , $status){
		$package = $this->model->where('id',$id)->first();
		if ($package->isActive == 0){
			$package->update(['isActive'=>1]);
		}else{
			$package->update(['isActive'=>0]);
		}
		return 'success';
	}

	public function new($requst){
		$package = $this->model->create([
			'name'=>$requst['name'],
			'cost'=>$requst['price']
		]);
		
		return $package;
	}

}