<?php

namespace App\Domains\Gift\entity;

use App\Domains\Customer\entity\Customer;
use App\Domains\Gift\entity\GiftType;
use Illuminate\Database\Eloquent\Model;

class Gift extends Model
{
    

    protected $guarded = [];
    

    public function type()
    {
    	return $this->belongsTo(GiftType::class,'gift_type_id');
    }

    public function customers()
    {
    	return $this->belongsToMany(Customer::class);
    }
}
