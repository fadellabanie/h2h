<?php

namespace App\Domains\Invoice\repository;

use App\Domains\Invoice\entity\Invoice;
use App\Domains\Invoice\entity\InvoiceStatus;
use App\Domains\Order\entity\Order;
use App\Domains\Setting\entity\Setting;
use PDF;

class InvoiceRepository
{

	private $model;

	public function __construct(Invoice $model)
	{
		$this->model = $model;
	}


	public function find($id,array $relations)
	{
		return $this->model->where('id',$id)->with($relations)->first();
	}
	public function generate($order)
	{

		$deliveryCost = $order->load('address.city')->address->city->delivery_cost;

		$sub_total = $order->total_amount;

		$total = $order->total_amount + $deliveryCost;

		$invoiceno = $this->generateInvoiceNumber($order);

		$tax = $this->getInvoiceTax();

		if ($tax != 0) {
			$tax = $tax / 100;
			$total = $total + ($total * $tax);
		}

		$invoice = $this->model->create([
			'order_id' => $order->id,
			'invoiceno' => $invoiceno,
			'invoice_status_id' => InvoiceStatus::ISSUED,
			'subtotal' => $sub_total,
			'total' => $total,
			'delivery_cost' => $deliveryCost,
			'tax' => $tax
		]);

		$invoice->load(['order.items.service','order.items.type','order.items.color','order.items.size','order.address','order.customer.user']);

        $pdf = PDF::loadView('admin.invoices.pdf',compact('invoice'));

		$pdf->save(storage_path('/app/invoices/'.$invoice->invoiceno.'.pdf','local'));

        return $invoice;
	}


	public function getInvoiceTax()
	{
		$setting = Setting::first();

		if ($setting && $setting->tax != null) {
			return $setting->tax;
		}

		return 0;

	}
	public function generateInvoiceNumber($order)
	{
		$nextInvoiceNumber = 'INV'.date('Y-m-d').'-'.$order->id;

		return str_replace('-',"",$nextInvoiceNumber);
	}
}
