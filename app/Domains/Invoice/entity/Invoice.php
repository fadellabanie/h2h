<?php

namespace App\Domains\Invoice\entity;

use App\Domains\Order\entity\Order;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{


    protected $guarded = [];

    public function order()
    {
    	return $this->belongsTo(Order::class,'order_id');
    }
}
