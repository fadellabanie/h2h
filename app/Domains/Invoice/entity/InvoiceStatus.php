<?php

namespace App\Domains\Invoice\entity;

use App\Domains\Invoice\contracts\InvoiceStatuses;
use Illuminate\Database\Eloquent\Model;

class InvoiceStatus extends Model implements InvoiceStatuses
{
    

    protected $guarded = [];
}
