<?php

namespace App\Domains\Invoice\contracts;


interface InvoiceStatuses
{
	const ISSUED = 1;
	const CANCELLED = 2;
}