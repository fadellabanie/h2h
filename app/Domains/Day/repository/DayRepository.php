<?php

namespace App\Domains\Day\repository;

use App\Domains\Day\entity\Day;


class DayRepository
{


	private $model;

	public function __construct(Day $model)
	{
		$this->model = $model;
	}


	public function select(array $fields)
	{
		return $this->model->select($fields);
	}

	public function store($driver,array $data)
	{
		$days = ['Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
		
		foreach($days as $day){
			if(in_array($day,$data)){
				$result[$day] = true;
			}
			else
			{
				$result[$day] = false;
			}
		}
		$result['driver_id'] = $driver;
		return $this->model->create($result);
	}

	public function update($driver_id,$data)
	{
		$days = ['Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
		
		foreach($days as $day){
			if(in_array($day,$data)){
				$result[$day] = true;
			}
			else
			{
				$result[$day] = false;
			}
		}

		$this->model->where('driver_id',$driver_id)->update($result);
	}
	public function driver($id)
	{
		return $this->model->where('driver_id',$id)->first();
	}
 
}