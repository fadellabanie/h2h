<?php

namespace App\Domains\User\contract;

interface ManagerRepositoryInterface
{

	public function select(array $fields);
	public function store(array $data);

}