<?php

namespace App\Domains\User\repository;

use App\Domains\User\contract\ManagerRepositoryInterface;
use App\Domains\User\entity\User;

class ManagerRepository implements ManagerRepositoryInterface
{

	private $model;

	public function __construct(User $model)
	{
		$this->model = $model;
	}


	public function select(array $fields)
	{
		return $this->model
		->where('user_group_id',1)
		->select($fields);
	}

	public function store(array $data)
	{
		return $this->model->create([
			'email' => $data['email'],
			'name' => $data['name'],
			'address' => isset($data['address']) ? $data['address'] : null,
			'phone' => isset($data['phone']) ? $data['phone'] : null,
			'user_group_id' => $data['user_group_id'],
			'password' => bcrypt($data['password']),
			'isActive' => 1
		]);
	}

	public function update($data,$id)
	{
		if ($data['password'] != '') {
			$data['password'] = bcrypt($data['password']);
		}
		return $this->model->where('id',$id)->update(array_filter($data));
	}

	public function findManager($id)
	{
		return $this->model->where('id',$id)
			->where('user_group_id',1)
			->first();
	}

	public function delete($id)
	{
		if (auth()->id() == $id) {
			return response()->json(['errors' => 'can`t delete your self'],400);
		}
		if ($id == 1) {
			return response()->json(['errors' => 'can`t delete main admin' ],400);
		}
		return $this->model->where('id',$id)->delete();
	}

	public function toggleActivation($id)
	{
		$user = $this->model->where('id',$id)->first();

		if ($user->isActive) {
			return $user->update(['isActive' => 0]);
		}

		return $user->update(['isActive' => 1]);
	}
}