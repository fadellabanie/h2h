<?php

namespace App\Domains\User\entity;

use App\Domains\User\entity\User;
use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    protected $guarded = [];


    public function users()
    {
    	return $this->hasMany(User::class);
    }

}
