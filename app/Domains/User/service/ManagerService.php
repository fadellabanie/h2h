<?php


namespace App\Domains\User\service;


use App\Domains\User\contract\ManagerRepositoryInterface;
use App\Domains\User\entity\UserGroup;


class ManagerService
{

	private $managerRepo;

	public function __construct(ManagerRepositoryInterface $managerRepo)
	{
		$this->managerRepo = $managerRepo;
	}


	public function selectManagers(array $fields)
	{
		return $this->managerRepo->select($fields);
	}

	public function create(array $data)
	{
		$this->managerRepo->store($data);
	}

	public function update($data,$id)
	{
		$this->managerRepo->update($data,$id);
	}

}