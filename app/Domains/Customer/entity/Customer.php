<?php


namespace App\Domains\Customer\entity;

// use App\Domains\Driver\entity\DriverStatus;
use App\Domains\Customer\entity\Address;
use App\Domains\Gift\entity\Gift;
use App\Domains\Item\entity\Item;
use App\Domains\Order\entity\Order;
use App\Domains\Order\entity\OrderStatus;
use App\Domains\Point\entity\Point;
use App\Domains\PromoCode\entity\PromoCode;
use App\Domains\Region\entity\Region;
use App\Domains\Service\entity\Service;
use App\Domains\User\entity\User;
use App\Exceptions\WalletException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

    protected $guarded = [];


    public function user()
    {
    	return $this->belongsTo(User::class,'user_id');
    }


    public function orders()
    {
        return $this->hasMany(Order::class,'customer_id');
    }


    public function basket()
    {   
        
        return $this->orders()
        ->where('customer_id',$this->id)
        ->whereIn('status_id',[OrderStatus::BASKET,OrderStatus::PLACED])

        //->where('status_id',OrderStatus::BASKET)
        //->orWhere('status_id',OrderStatus::PLACED)

        ->with('items.type','items.service','items.color','items.size','items.facility')
        ->get();
    }

    public function customerBasket()
    {
        return $this->orders()
            ->where('customer_id',$this->id)
            ->whereIn('status_id',[OrderStatus::BASKET,OrderStatus::PLACED])
            //->orWhere('status_id',OrderStatus::PLACED)
            ->latest()
            ->first();
    }

    public function placedOrder()
    {
        return $this->orders()
            ->where('status_id',OrderStatus::PLACED)
            ->where('customer_id',$this->id)
            ->latest()
            ->first();
    }

    public function rates()
    {
        return $this->belongsToMany(Order::class,'customer_order_rate_service','customer_id','order_id');
    }

    public function promoCodes()
    {
        return $this->belongsToMany(PromoCode::class,'customer_promo','customer_id','promoCode_id');
    }


    public function gifts()
    {
        return $this->belongsToMany(Gift::class,'customer_gift','customer_id','gift_id');
    }

    public function points()
    {
        return $this->hasMany(Point::class,'customer_id');
    }

    public function addresses()
    {
        return $this->hasMany(Address::class,'customer_id');
    }

    public function validateWallet($invoice)
    {
        if ($this->wallet < $invoice->total) {

            $invoice->order()->update([
                'status_id' => OrderStatus::PLACED,
            ]);

            $invoice->delete();

            throw new WalletException();
        }
    }
    // public function status()
    // {
    // 	return $this->belongsTo(DriverStatus::class,'driver_status_id');
    // }

    // public function regions()
    // {
    // 	return $this->belongsToMany(Region::class);
    // }

/*    public function setDateOfBirthAttribute($value)
    {
        $this->attributes['date_of_birth'] = Carbon::parse($value)->format('y-m-d');
    }*/
}
