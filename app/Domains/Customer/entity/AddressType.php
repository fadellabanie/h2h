<?php

namespace App\Domains\Customer\entity;

use App\Domains\Customer\contracts\AddressTypes;
use App\Domains\Customer\entity\Address;
use Illuminate\Database\Eloquent\Model;

class AddressType extends Model implements AddressTypes
{
    

    protected $guarded = [];
    

    public function addresses()
    {
    	return $this->hasMany(Address::class,'address_type_id');
    }
}
