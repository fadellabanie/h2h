<?php

namespace App\Domains\Customer\entity;

use App\Domains\City\entity\City;
use App\Domains\Customer\entity\AddressType;
use App\Domains\Customer\entity\Customer;
use App\Domains\Region\entity\Region;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{

	
    protected $guarded = [];


    public function city()
    {
    	return $this->belongsTo(City::class,'city_id');
    }

    public function region()
    {
    	return $this->belongsTo(Region::class,'region_id');
    }

    public function customer()
    {
    	return $this->belongsTo(Customer::class,'customer_id');
    }

    public function type()
    {
    	return $this->belongsTo(AddressType::class,'address_type_id');
    }
}
