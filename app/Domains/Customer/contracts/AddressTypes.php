<?php

namespace App\Domains\Customer\contracts;


interface AddressTypes
{

	const HOME = 1;
	const WORK = 2;
	const OTHER = 3;
}