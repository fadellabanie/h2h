<?php


namespace App\Domains\Customer\repository;

use App\Domains\Customer\entity\Address;


class AddressRepository
{

	private $model;

	public function __construct(Address $model)
	{
		$this->model = $model;
	}


	public function select($csutomer_id)
	{
		return $this->model
		->where('customer_id',$csutomer_id)
		->with('type','region','city')
		->select('addresses.*');
	}

	public function store($data,$customer_id)
	{
		return $this->model->create([
			'address_type_id' => $data['address_type_id'],
			'customer_id' => $customer_id,
			'city_id' => $data['city_id'],
			'building_no' => $data['building_no'],
			'floor_no' => $data['floor_no'],
			'flat_no' => $data['flat_no'],
			'region_id' => $data['region_id'],
			'street' => $data['street'],
			'lat' => $data['lat'],
			'lng' => $data['lng'],
		]);
	}


}
