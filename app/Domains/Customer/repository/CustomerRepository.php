<?php

namespace App\Domains\Customer\repository;

use App\Domains\Customer\entity\Customer;
use App\Domains\User\entity\User;
use Carbon\Carbon;

class CustomerRepository
{

	private $model;

	public function __construct(Customer $model)
	{
		$this->model = $model;
	}


	public function model()
	{
		return $this->model;
	}

	public function store(User $user,array $data)
	{
		return $user->customer()->create();
	}

	public function findDriver($id,array $relations)
	{
		return $this->model->where('id',$id)
			->with($relations)
			->first();
	}



	public function update($id,array $data)
	{
		$driver = $this->model->where('id',$id)->first();

		$driver->update([
			'license' => $data['license'],
			'date_of_birth' => Carbon::parse($data['date_of_birth'])->format('Y-m-d')
		]);


		$this->syncRegion($driver,$data['region_id']);
	}

	public function syncRegion($driver,array $regions)
	{
		if (!empty($regions)) {
			$driver->regions()->sync($regions);
		}
	}

	public function user($user_id)
	{
		return $this->model->where('user_id',$user_id)->first();
	}
}