<?php

namespace App\Domains\Customer\service;

use App\Domains\Customer\repository\CustomerRepository;
use App\Domains\User\repository\ManagerRepository;
use Illuminate\Support\Facades\DB;
use App\Domains\Day\repository\DayRepository;

class Customer
{

	private $customerRepo;
	private $userRepo;
	private $dayRepo;
	public function __construct(CustomerRepository $customerRepo,ManagerRepository $userRepo,DayRepository $day )
	{
		$this->customerRepo = $customerRepo;
		$this->userRepo = $userRepo;
		$this->dayRepo = $day;
	}


	public function data(array $relations)
	{
		return $this->customerRepo->model()->with($relations)->select('customers.*');
	}

	public function new(array $data)
	{
		$data['user_group_id'] = 3;

		DB::beginTransaction();
		try{
		    // Database operations
		    $user = $this->userRepo->store($data);

		    //return $user;
			$this->customerRepo->store($user,$data);
		    // commit action for success
			DB::commit();
			return $user;
		}catch(\Exception $e){
		    // rollback operation for failure
		    DB::rollback();
		    return response()->json(['errors' => 'server error'],400);
		}
	}

	public function update($id,$data)
	{
		$this->driverRepo->update($id,$data);
	}

	public function driverImage($id,$image)
	{

		$driver = $this->driverRepo->findDriver($id,[]);

		if ($driver->image_url != null ) {
			unlink(public_path('uploads/'.$driver->image_url));
		}

		$driver->update([
			'image_url' => $image->store('drivers/avatars/'.$driver->id,'uploads')
		]);

	}


	public function addPoint($order,$customer)
	{
		$fixed_points = Setting::first();

		$points = $order->total / $fixed_points;

		$customer->update(['points' => $points]);
	}

}
