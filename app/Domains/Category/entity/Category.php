<?php

namespace App\Domains\Category\entity;

use App\Domains\Type\entity\Type;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $guarded = [];

    public function types()
    {
    	return $this->belongsToMany(Type::class);
    }
}
