<?php

namespace App\Domains\Category\repository;

use App\Domains\Category\entity\Category;

class CategoryRepository
{
	private $model;

	public function __construct(Category $model)
	{
		$this->model = $model;
	}


	public function select(array $fields)
	{
		return $this->model
			->select($fields);
	}

	public function all()
	{
		return $this->model->all(['id','name']);
	}

	public function store(array $data)
	{
		return $this->model->create(array_filter($data));
	}


	public function findCat($id)
	{
		return $this->model->where('id',$id)->first();
	}


	public function update(array $data,$id)
	{
		return $this->model->where('id',$id)->update($data);
	}

	public function delete($id)
	{
		$cat = $this->model->where('id',$id)->first();

		if ($cat->types()->exists()) {
			return response()->json(['errors' => 'sorry can`t delete'],400);
		}

		$cat->delete();
	}
}