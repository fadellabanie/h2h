<?php

namespace App\Exceptions;

use Exception;

class WalletException extends Exception
{


    public function render()
    {
    	return response()->json(['errors' => 'sorry you don`t have enough money in your wallet'],500);
    }
}
