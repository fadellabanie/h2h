<?php

namespace App\Exceptions;

use Exception;

class PasswordValidException extends Exception
{

	public function render()
	{
		return response()->json(['error' => 'the password not applied'],500);
	}	
}