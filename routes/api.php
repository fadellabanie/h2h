<?php

use Illuminate\Http\Request;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::group(['namespace'=>'Apis'],function(){
//     Route::post('login','CustomerController@login');
// });
// Route::group(['middleware' => 'auth:api','namespace'=>'Apis'], function(){
//     Route::get('logout', 'CustomerController@logout')->name('api.jwt.logout');
// });



Route::group([

    'middleware' => 'api',
    'prefix' => 'auth',
    'namespace'=>'Apis'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('change_password', 'AuthController@change_password');
    Route::post('me', 'AuthController@me');
    Route::post('register', 'AuthController@register');
    Route::post('social', 'AuthController@social');
    Route::post('forgot', 'ForgotPasswordController@create');
    Route::get('find/{token}', 'ForgotPasswordController@find');
    Route::post('reset', 'ForgotPasswordController@reset');
    // Route::get('service','ServiceController@get_all');
    // Route::get('service/{id}','ServiceController@show');
    // Route::get('service/pricing/{id}','ServiceController@pricing');
    // Route::get('offers','OfferController@index');
    // Route::get('points','PointController@index');
});


Route::group(['prefix' => 'pages','namespace'=>'Apis'],function(){
    Route::get('about','PagesController@about');
    Route::get('terms','PagesController@terms');
});

Route::group(['prefix' => 'v1', 'namespace'=>'Apis'],function(){
    Route::group(['prefix' => 'services'],function(){
        Route::get('/prices','ServiceController@prices');
    });
});
//,'middleware' => 'api'
Route::group(['prefix' => 'v1', 'namespace'=>'Apis','middleware' => 'api' ],function(){


    Route::group(['prefix' => 'pickupTimes'],function(){
        Route::get('/','pickupTimesController@index');
    });


    Route::group(['prefix' => 'services'], function(){
        Route::get('/','ServiceController@index');
        // Route::get('/prices','ServiceController@prices');
        Route::post('/types','ServiceController@types');
        Route::post('/type/prices','ServiceController@typePrices');
        Route::post('/facilities','ServiceController@facilities');
        Route::get('/childs/{service_id}','ServiceController@childs');
        Route::post('/types/childs','ServiceController@typeChilds');

    });


    Route::group(['prefix' => 'facilities'],function(){
        Route::get('/','FacilitiesController@index');
        Route::post('/childs','FacilitiesController@childs');
    });


    Route::group(['prefix' => 'orders'],function(){


        Route::post('/','OrdersController@store');
        Route::get('/details/{order_id}','OrdersController@details');
        Route::get('/basket','OrdersController@basket');
        Route::post('/update','OrdersController@update');
        Route::post('/place','OrdersController@place');
        Route::post('/confirm','OrdersController@confirm');
        Route::post('/cancel','OrdersController@cancel');
        Route::post('/track','OrdersController@track');

        Route::post('/increment','OrdersController@incrementItem');
        Route::post('/decrement','OrdersController@decrementItem');


        Route::group(['prefix' => 'facilities'], function (){
            Route::post('add','OrderFacilitiesController@store');
            Route::post('delete','OrderFacilitiesController@delete');
        });

        //Route::post('/prices','OrdersController@track');

        //Route::post('urgent','OrdersController@urgent');

        Route::group(['prefix' => 'item'],function(){
            Route::get('/{item_id}','OrderItemsController@show');
            Route::post('/','OrderItemsController@index');
            Route::post('/facilities','OrderItemsController@facilities');
            Route::post('/update/{item_id}','OrderItemsController@update');
            Route::delete('/delete','OrderItemsController@delete');
        });


        Route::group(['prefix' => 'rate'],function(){
            Route::post('/','RateOrderController@store');
            Route::get('/','RateOrderController@index');
        });


    });


    Route::group(['prefix' => 'customers'],function(){

        Route::get('wallet','CustomersController@wallet');

        Route::get('points','CustomersController@points');

        Route::get('notifications','CustomersController@notifications');

        Route::group(['prefix' => 'edit'],function (){

            Route::post('name','UpdateCustomersController@name');
            Route::post('phone','UpdateCustomersController@phone');
            Route::post('email','UpdateCustomersController@email');
            Route::post('date_of_birth','UpdateCustomersController@dateOfBirth');
            Route::post('gender','UpdateCustomersController@gender');
            Route::post('password','UpdateCustomersController@password');
            Route::post('image','UpdateCustomersController@image');

        });


        Route::group(['prefix' => 'orders'],function(){
            Route::get('/','CustomerOrdersController@index');
        });

        Route::group(['prefix' => 'addresses'],function(){
            Route::get('/','AddressesController@index');
            Route::get('/delete/{address_id}','AddressesController@delete');
            Route::post('/','AddressesController@store');
        });
    });

    Route::group(['prefix' => 'categories'],function(){
        Route::get('/','CategoriesController@index');
    });

    Route::group(['prefix' => 'colors'],function(){
        Route::get('/','ColorsController@index');
    });

    Route::group(['prefix' => 'sizes'],function(){
        Route::get('/','SizesController@index');
    });

    Route::group(['prefix' => 'cities'],function(){

        Route::get('/','CitiesController@index');

        Route::group(['prefix' => 'regions'],function(){
            Route::get('/','CitiyRegionsController@index');
        });
    });

    Route::group(['prefix' => 'invoices'],function(){

        Route::post('/','InvoicesController@index');

    });

    Route::group(['prefix' => 'gifts'],function(){

        Route::get('/','GiftsController@index');
        Route::post('/redeem','GiftsController@redeem');

    });

    Route::group(['prefix' => 'support'],function(){
        Route::post('/','SupportsController@store');
    });

    Route::group(['prefix' => 'packages'],function(){
        Route::get('/','PackagesController@index');
        Route::post('/','PackagesController@store');
    });

    Route::group(['prefix' => 'promocode'],function(){
        Route::post('/','PromoCodesController@index');
    });


    Route::group(['prefix' => 'wallet'],function(){
        Route::get('/','WalletsController@index');
    });

    Route::group(['prefix' => 'driver'],function(){

        Route::get('/','DriversController@index');
        
        
       Route::post('/get-location','DriversController@getLocation');

        Route::post('/pickedup','DriversController@pickedup');

        Route::post('/dropoff','DriversController@dropoff');

        Route::post('/delivered','DriversController@delivered');
           

        Route::group(['prefix' => 'orders'],function(){
            
            Route::get('/get-drivers','DriverOrdersController@listDrivers');
            
            Route::get('/accept-order-by-driver/{order_id}','DriverOrdersController@acceptOrder');

            Route::get('/completed','DriverOrdersController@completed');
            Route::get('/pickup','DriverOrdersController@pickup');
            Route::get('/dropoff','DriverOrdersController@dropoff');
            Route::get('/today','DriverOrdersController@today');
        });


    });


});// api/v1 "auth:api"
