<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'dashboard','namespace' => 'Admin','middleware'=> 'auth'], function() {

    Route::get('/','DashboardController@index')->name('dashboard.index');

    Route::group(['prefix' => 'managers','namespace' => 'Users'], function() {
        Route::get('/','ManagersController@index')->name('managers.index');
        Route::post('/','ManagersController@store')->name('managers.store');
        Route::patch('/update/{id}','ManagersController@update')->name('managers.update');
        Route::get('/{id}','ManagersController@show')->name('managers.show');
        Route::get('/delete/{id}','ManagersController@delete')->name('managers.delete');
        Route::get('/activation/toggle/{id}','ManagersController@toggleActivation')->name('managers.toggleActivation');

        
        Route::get('/users/create','ManagersController@create')->name('users.create');
        
        Route::get('/users/edit/{user_id}','ManagersController@edit')->name('users.edit');
    });


    Route::group(['prefix' => 'categories','namespace' => 'Categories'], function() {

        Route::get('/','CategoriesController@index')->name('categories.index');
        Route::get('/all','CategoriesController@all')->name('categories.all');
        Route::post('/','CategoriesController@store')->name('categories.store');
        Route::get('/{id}','CategoriesController@show')->name('categories.show');
        Route::post('/update/{id}','CategoriesController@update')->name('categories.update');
        Route::get('/delete/{id}','CategoriesController@delete')->name('categories.delete');

        Route::get('/types/{type_id}','CategoriesController@types')->name('categories.types');

    });

    Route::group(['prefix' => 'cities','namespace' => 'Cities'], function() {
        Route::get('/','CitiesController@index')->name('cities.index');
        Route::get('/all','CitiesController@all')->name('cities.all');
        Route::post('/','CitiesController@store')->name('cities.store');
        Route::get('/{id}','CitiesController@show')->name('cities.show');
        Route::post('/update/{id}','CitiesController@update')->name('cities.update');
        Route::get('/delete/{id}','CitiesController@delete')->name('cities.delete');
        Route::get('/regions/{city_id}','CitiesController@cityRegions')->name('cities.regions');

    });


    Route::group(['prefix' => 'regions','namespace' => 'Regions'], function() {
        Route::get('/','RegionsController@index')->name('regions.index');
        Route::get('/all','RegionsController@all')->name('regions.all');
        Route::post('/','RegionsController@store')->name('regions.store');
        Route::get('/{id}','RegionsController@show')->name('regions.show');
        Route::post('/update/{id}','RegionsController@update')->name('regions.update');
        Route::get('/delete/{id}','RegionsController@delete')->name('regions.delete');
    });

    Route::group(['prefix' => 'drivers','namespace' => 'Drivers'], function() {
        Route::get('/','DriversController@index')->name('drivers.index');
        Route::get('/all','DriversController@all')->name('drivers.all');
        Route::post('/','DriversController@store')->name('drivers.store');
        Route::get('/{id}','DriversController@show')->name('drivers.overview');
        Route::get('/personal/{id}','DriversController@edit')->name('drivers.personal');
        Route::get('/account/{id}','DriversController@account')->name('drivers.account');
        Route::post('/update/{id}','DriversController@update')->name('drivers.update');
        Route::get('/delete/{id}','DriversController@delete')->name('drivers.delete');
        Route::get('/download/{id}','DriversController@download')->name('drivers.download');


    });

    Route::group(['prefix' => 'services','namespace' => 'Services'], function() {


        Route::get('/','ServicesController@index')->name('services.index');
        Route::get('/all','ServicesController@all')->name('services.all');
        Route::post('/','ServicesController@store')->name('services.store');
        Route::get('/{id}','ServicesController@show')->name('services.show');
        Route::get('edit/{id}/{serviceid}','ServicesController@edit')->name('services.edit');
        Route::post('update_type/{serviceid}','ServicesController@updateType')->name('services.updateType');


        Route::patch('/update/{id}','ServicesController@update')->name('services.update');

        Route::get('/activation/toggle/{id}','ServicesController@toggleActivation')->name('services.toggleActivation');


        Route::get('/delete/{id}','ServicesController@delete')->name('services.delete');

        Route::group(['prefix' => 'childs'],function(){
            Route::get('{service_id}','ServiceChildsController@index');
            Route::post('{service_id}','ServiceChildsController@store')->name('serviceChild.store');
            Route::post('update/{service_id}','ServiceChildsController@update')->name('serviceChild.update');

            Route::get('delete/{service_id}','ServiceChildsController@delete');
        });
    });


    Route::group(['prefix' => 'serviceTypes','namespace' => 'serviceTypes'], function() {


        Route::get('/{service_id}','ServiceTypesController@index');
        Route::get('/all','ServiceTypesController@all');
        Route::post('/{service_id}','ServiceTypesController@store')->name('serviceTypes.store');
        Route::get('/{id}','ServiceTypesController@show');
        Route::get('/find/{service_id}','ServiceTypesController@find');
        Route::post('/update/{id}','ServiceTypesController@update');
        Route::get('/delete/{service_id}/{type_id}/{cat_id}','ServiceTypesController@delete');
        Route::get('/childs/{service_id}','ServiceTypesController@childs');

    });


    Route::group(['prefix' => 'types','namespace' => 'Types'], function() {

        Route::get('/','TypesController@index')->name('types.index');
        Route::get('/all','TypesController@all')->name('types.all');
        Route::post('/','TypesController@store')->name('types.store');
        Route::get('/{id}','TypesController@show')->name('types.show');
        Route::post('/update/{id}','TypesController@update')->name('types.update');
        Route::get('/delete/{id}','TypesController@delete')->name('types.delete');

    });

    Route::group(['prefix' => 'facilities','namespace' => 'Facilities'], function() {
        Route::get('/','FacilitiesController@index')->name('facilities.index');
        Route::get('/all','FacilitiesController@all')->name('facilities.all');
        Route::post('/','FacilitiesController@store')->name('facilities.store');
        Route::get('/{id}','FacilitiesController@show')->name('facilities.show');
        Route::post('/update/{id}','FacilitiesController@update')->name('facilities.update');
        Route::get('/delete/{id}','FacilitiesController@delete')->name('facilities.delete');
    });

    Route::group(['prefix' => 'serviceFacilities','namespace' => 'serviceFacilities'], function() {
        Route::get('/{service_id}','serviceFacilitiesController@index');
        Route::get('/all','serviceFacilitiesController@all');
        Route::post('/{service_id}','serviceFacilitiesController@store')->name('serviceFacilities.store');
        Route::get('/{id}','serviceFacilitiesController@show');
        Route::get('/find/{service_id}','serviceFacilitiesController@find');
        Route::post('/update/{id}','serviceFacilitiesController@update');
        Route::get('/delete/{service_id}/{facility_id}','serviceFacilitiesController@delete');
    });


    Route::group(['prefix' => 'packages','namespace' => 'Packages'], function() {
        Route::get('/','PackagesController@index')->name('packages.index');
        Route::get('/all','PackagesController@all')->name('packages.all');
        Route::get('/{id}','PackagesController@show')->name('packages.show');
        Route::patch('/update/{id}','PackagesController@update')->name('packages.update');

        Route::get('/toggle/{id}','PackagesController@toggle')->name('packages.toggle');

        Route::get('/customers/{package_id}','PackagesController@customers')->name('packages.customers');

        Route::get('/delete/{id}','PackagesController@delete')->name('packages.delete');
        Route::post('/','PackagesController@store')->name('packages.store');

    });

    Route::group(['prefix' => 'customers','namespace' => 'Customer'], function() {

        Route::get('/','CustomerController@index')->name('customers.index');

        Route::get('/find','CustomerController@find')->name('customers.find');

        Route::get('/{customer_id}','CustomerController@show')->name('customers.show');
        Route::post('/store','CustomerController@store')->name('customers.store');
        
        Route::get('/edit/{id}','CustomerController@editCustomer')->name('customers.edit');
        Route::post('/update/{id}','CustomerController@updateCustomer')->name('customers.update');

        Route::get('orders/{customer_id}','CustomerController@orders');


        Route::group(['prefix' => 'addresses'],function(){
            Route::get('/{customer_id}','CustomerAddressesController@index')->name('customerAddress.index');
        });

        Route::group(['prefix' => 'points'],function(){
            Route::get('/{customer_id}','CustomerPointsController@index')->name('customerPoints.index');
        });

        Route::group(['prefix' => 'invoices'],function(){
            Route::get('/{customer_id}','CustomerInvoicesController@index')->name('customerInvoices.index');
        });

    });



    Route::group(['prefix'=>'offers','namespace'=>'Offer'],function(){

        Route::get('/','OfferController@index')->name('offers.index');
        Route::post('/','OfferController@store')->name('offers.store');
        Route::get('/delete/{id}','OfferController@delete')->name('offers.delete');

    });

    Route::group(['prefix'=>'gifts','namespace'=>'Gift'],function(){

        Route::get('/','GiftsController@index')->name('gifts.index');
        Route::get('/{gift_id}','GiftsController@show')->name('gifts.show');
        Route::post('/','GiftsController@store')->name('gifts.store');
        Route::patch('/{id}','GiftsController@update')->name('gifts.update');
        Route::get('/delete/{gift_id}','GiftsController@delete')->name('gifts.delete');
        Route::get('/toggle/{gift_id}','GiftsController@toggle')->name('gifts.toggle');


        Route::group(['prefix' => 'customers'],function (){
            Route::get('/{gift_id}','GiftsCustomersController@index')->name('gifts.customers');
            Route::get('/show/{gift_id}','GiftsCustomersController@show')->name('gifts.customers.show');
            Route::get('/delete/{customer_id}/{gift_id}','GiftsCustomersController@delete')->name('gifts.customers.delete');

        });

    });

    Route::group(['prefix'=>'points','namespace'=>'Point'],function(){

        Route::get('/','PointController@index')->name('points.index');
        Route::post('/','PointController@store')->name('points.store');
        Route::get('/delete/{id}','PointController@delete')->name('points.delete');

    });

    Route::group(['prefix'=>'promocodes','namespace'=>'PromoCodes'],function(){

        Route::get('/','PromoCodesController@index')->name('promoCodes.index');
        Route::get('/{id}','PromoCodesController@show')->name('promoCodes.show');
        Route::post('/','PromoCodesController@store')->name('promoCodes.store');
        Route::patch('/{id}','PromoCodesController@update')->name('promoCodes.update');
        Route::get('/delete/{id}','PromoCodesController@delete')->name('promoCodes.delete');

        Route::get('/toggle/{id}','PromoCodesController@toggle')->name('promoCodes.toggle');

    });

    Route::group(['prefix' => 'promocodesCustomers','namespace'=>'PromoCodes'],function(){

        Route::get('/{id}','PromoCodeCustomersController@index')->name('promoCodes.customers');

        Route::get('show/{id}','PromoCodeCustomersController@show')->name('promoCodes.show');

        Route::get('delete/{customer_id}/{promo_id}','PromoCodeCustomersController@delete')->name('promoCodes.delete');
    });

    Route::group(['prefix' => 'settings','namespace'=>'Settings'],function(){

        Route::get('/','SettingsController@index')->name('settings.index');
        Route::post('/','SettingsController@store')->name('settings.save');
        Route::post('/storePage','SettingsController@storePage')->name('settings.storePage');

        Route::group(['prefix' => 'pickup'],function(){
            Route::get('/','PickupsController@index');
            Route::get('delete/{pick_id}','PickupsController@delete');
            Route::post('/','PickupsController@store')->name('timepicker.store');
        });
    });


    Route::group(['prefix' => 'invoices','namespace' => 'Invoices'], function() {
        Route::get('/','InvoicesController@index')->name('invoices.index');
        Route::get('/{invoice_id}','InvoicesController@show')->name('invoices.show');

        Route::get('download/{invoice_id}','InvoicesController@download')->name('invoices.download');
    });

    Route::group(['prefix' => 'orders','namespace' => 'Orders'], function() {

        Route::get('/','OrdersController@index')->name('orders.index');
        Route::get('/charge/{order_uuid}','OrdersController@charge')->name('orders.charge');
        Route::get('/calendar','OrdersController@calendar')->name('orders.calendar');
        Route::get('/calendarData','OrdersController@calendarData')->name('orders.calendarData');
        Route::get('/{order_id}','OrdersController@show')->name('orders.show');
        Route::get('/delete/{order_id}','OrdersController@delete')->name('orders.delete');

        Route::group(['prefix' =>'delivery'],function(){

            Route::get('/{driver_id}','OrderDeliveriesController@index');

        });

    });

    Route::group(['prefix' => 'support','namespace' => 'Supports'], function() {

        Route::get('/','SupportsController@index')->name('support.index');
        Route::get('/{support_id}','SupportsController@show')->name('support.show');
        Route::get('/delete/{support_id}','SupportsController@delete')->name('support.delete');
        Route::post('/{support_id}','SupportsController@reply')->name('support.reply');

    });


    Route::group(['prefix' => 'reports','namespace' => 'Reports'],function (){
        Route::get('services','ServicesReportsController@index')->name('reports.services.index');
        Route::get('facilities','FacilitiesReportsController@index')->name('reports.facilities.index');
        Route::get('drivers','DrviersReportsController@index')->name('reports.drivers.index');
    });
});

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
  ]);

Route::get('/home', 'HomeController@index')->name('home');
