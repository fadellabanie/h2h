<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'facebook' => [
        'client_id' => '620763618713187',
        'client_secret' => '3b8860bb2eb1e3609b708b5b89b876d8',
        'redirect' => 'http://h2h.theportal.agency/',
    ],

    'google' => [
        'client_id' => '737039249671-qjtp8grfv3pv1aeult2eloqivns8e079.apps.googleusercontent.com',
        'client_secret' => 'c-ilex0mGuJmTEsjZ_B5hI21',
        'redirect' => 'http://h2h.theportal.agency/',
    ],

];
